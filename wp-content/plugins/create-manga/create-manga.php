<?php
/**
 * @package HH POPUNDER
 */
/*
Plugin Name: CREATE MANGA
Plugin URI:
Description: creates and updates manga and chapters. fetch data from kissmanga, mangafox and my anime list
Version: 0.1. beta
Author: Hortt
Author URI:
License: GPLv2 or later
Text Domain: crt_mnga
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 20015-2016 WebRangers
*/
// Make sure we don't expose any info if called directly
if ( ! function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

/**
 *
 */
define( 'CREATEMANGA__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

/**
 *
 */
define( 'CREATEMANGA_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );

register_activation_hook( __FILE__, array( 'Createmanga_Settings', 'plugin_activation' ) );
register_deactivation_hook( __FILE__, array( 'Createmanga_Settings', 'plugin_deactivation' ) );

require_once( CREATEMANGA__PLUGIN_DIR . 'inc/nokogiri.php' );
require_once( CREATEMANGA__PLUGIN_DIR . 'inc/class.options.php' );
require_once( CREATEMANGA__PLUGIN_DIR . 'inc/class.utils.php' );
require_once( CREATEMANGA__PLUGIN_DIR . 'inc/class.crons.php' );
require_once( CREATEMANGA__PLUGIN_DIR . 'inc/class.metabox.php' );
require_once( CREATEMANGA__PLUGIN_DIR . 'inc/class.createmanga.php' );

add_action( 'init', array( 'Createmanga', 'init' ) );
add_action( 'init', array( 'Createmanga_Metabox', 'init' ) );
add_action( 'init', array( 'Createmanga_Utils', 'init' ) );


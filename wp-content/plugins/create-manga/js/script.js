'use strict';


function mh_ajax_search() {
    (function( $ ) {
        /* AJAX SEARCH */
        var search_input = $( '.sa_search input#s' );

        search_input.each( function() {
            var sa_wrapper = $( this ).parents( '.sa_search' ).find( '.sa_wrapper' );
            var sa_start = true;
            var search_query = $( this ).val();
            $( this ).keyup( function( e ) {
                if ( sa_start === true && $( this ).val().length > 1 ) {
                    get_sa( this.value );
                }
                if ( !this.value ) {
                    sa_wrapper.hide().html( '' );
                }
                search_query = this.value;
            } );

            sa_wrapper.on('click', '.menu-item-title, .menu-item-checkbox', function(){
                var found_title = sa_wrapper.find( 'label.menu-item-title').text(),
                    found_id = sa_wrapper.find( '.menu-item-checkbox').val();
                sa_wrapper.parent().find('input[name="post_parent"]').val(found_id);
                search_input.val(found_title);
                sa_wrapper.parent().find('.edit_link').remove();
            });

            //$( "body > div:not('.sa_search')" ).click( function( e ) {
            //    sa_wrapper.hide();
            //} );
            function get_sa( keyword ) {
                var search_indicator = $( '.mh_search_indicator' );
                $.ajax( {
                    url: '/wp-admin/admin-ajax.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        action: 'search_ajax_short',
                        keyword: keyword
                    },
                    beforeSend: function() {
                        search_indicator.toggleClass( 'fa-search fa-spin fa-spinner' );
                        sa_start = false;
                    },
                    success: function( response ) {
                        sa_start = true;
                        if ( response != '' )
                            sa_wrapper.html( response ).slideDown( 'fast' );
                        else
                            sa_wrapper.html( '' ).slideUp( 'fast' );
                        if ( keyword !== search_query ) {
                            get_sa( search_query )
                        }
                    },
                    error: function( response ) {
                        sa_start = true;
                        search_indicator.toggleClass( 'fa-search fa-spin fa-spinner' );
                    }
                } );
            }
        } )
    })( jQuery );
}
(function ($) {
    $(document).ready(function () {
        var fetch_button = $('.fetch_images_for_chapter'),
            insert_button = $('.insert_post'),
            fetch_manga_button = $('.fetch_manga_data'),
            batch_button = $('#process_all_chapters'),
            saving_data_permission = true,
            post_ids = [],
            posts_id_object = $('button[data-completed_chapter="0"][data-origin_url]');

        mh_ajax_search();
        fetch_button.click(function () {
            var post_id = $(this).data('post_id'),
                replace = $(this).attr('data-replace'),
                origin_url = $(this).data('origin_url');
            fetch_images_for_chapter(post_id, replace);
        });

        fetch_manga_button.click(function () {
            var post_id = $(this).data('post_id'),
                source = $(this).data('source');
            fetch_manga_date_from_remote_source(post_id, source);
        });

        insert_button.click(function () {
            var origin_url = $(this).data('origin_url'),
                post_type = $(this).data('post_type'),
                post_parent = $(this).data('post_parent'),
                title = $(this).data('title');
            insert_post(title, post_parent, origin_url, post_type);
        });

        function insert_posts_one_by_one( action ) {
            var post_id = post_ids[0];
            if (typeof post_id === 'undefined') {
                alert('all chapters was inserted')
            } else {
                if (saving_data_permission == true) {
                    var fetch_button;
                    if( action == 'insert' ){
                        fetch_button= $('button.insert_post[data-origin_url="' + post_id + '"');
                    } else {
                        fetch_button = $('button.fetch_images_for_chapter[data-origin_url="' + post_id + '"');
                    }
                    fetch_button.click();
                }
            }
        }


        batch_button.click(function (e) {
            e.preventDefault();
            posts_id_object = $('button[data-origin_url][data-completed_chapter="0"]');
            if (post_ids.length == 0) {
                /* если нет - то сначала */
            } else {
                post_ids = [];
            }
            posts_id_object.each(function () {
                post_ids.push($(this).attr('data-origin_url'));
            });
            insert_posts_one_by_one( $('#process_all_chapters').data('action').toString() );

            return false;
        });

        function fetch_images_for_chapter(post_id, replace) {
            var button = $('button.fetch_images_for_chapter[data-post_id="' + post_id + '"]'),
                spinner = button.find('.spinner');

            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'fetch_images_for_chapter',
                    post_id: post_id,
                    replace: replace
                },
                beforeSend: function () {
                    spinner.show();
                    saving_data_permission = false;
                },
                success: function (response) {
                    spinner.hide();
                    button.parents('tr').find('.fetch_result').html(response.message);
                    button.parents('table').find('.img_count').html(parseInt(response.message));
                    if (response.success == true) {
                        $('.wpseo-score-icon[data-post_id="' + post_id + '"]').removeClass('bad').addClass('good')
                    } else {
                        $('.wpseo-score-icon[data-post_id="' + post_id + '"]').removeClass('good').addClass('bad')
                    }
                    saving_data_permission = true;
                    if (post_ids.length != 0) {
                        post_ids.shift();
                        insert_posts_one_by_one( $('#process_all_chapters').data('action').toString());
                    }
                },
                error: function (response) {

                }
            })
        }

        function insert_post(title, post_parent, origin_url, post_type) {
            var button = $('button.insert_post[data-origin_url="' + origin_url + '"]'),
                spinner = button.find('.spinner');
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'insert_post',
                    post_parent: post_parent,
                    post_type: post_type,
                    origin_url: origin_url,
                    title: title
                },
                beforeSend: function () {
                    spinner.show();
                },
                success: function (response) {
                    spinner.hide();
                    button.parents('tr').find('.insert_result').html(response.message);
                    if (response.success == true) {
                        button.parents('tr').find('.wpseo-score-icon').removeClass('bad').addClass('ok');
                        button.hide();
                        saving_data_permission = true;
                        if (post_ids.length != 0) {
                            post_ids.shift();
                            insert_posts_one_by_one( $('#process_all_chapters').data('action').toString());
                        }
                    } else {
                        button.parents('tr').find('.wpseo-score-icon').removeClass('ok').addClass('bad')
                    }

                },
                error: function (response) {

                }
            })
        }

        function fetch_manga_date_from_remote_source(post_id, source) {
            var button = $('button.fetch_manga_data[data-source="' + source + '"]'),
                spinner = button.find('.spinner');
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'manga_data_remote_fetch',
                    post_id: post_id,
                    source: source
                },
                beforeSend: function () {
                    spinner.show();
                },
                success: function (response) {
                    spinner.hide();
                    var year_input = $('input#release_year'),
                        altr_nms_input = $('input#altr_nms'),
                        status_input = $('input#status'),
                        rating_input = $('input#rating'),
                        genres_input = $('input#new-tag-genre'),
                        authors_input = $('input#new-tag-author'),
                        artists_input = $('input#new-tag-artist'),
                        synopsys_input = $('textarea#content');
                    year_input.val() === '' ? year_input.val(response.released) : '';
                    altr_nms_input.val() === '' ? altr_nms_input.val(response.other_names.join()) : '';
                    status_input.val() === '' ? status_input.val(response.status) : '';
                    rating_input.val() === '' ? rating_input.val(response.rating) : '';
                    genres_input.val() === '' ? genres_input.val(response.genres.join()) : '';
                    authors_input.val() === '' ? authors_input.val(response.artists) : '';
                    artists_input.val() === '' ? artists_input.val(response.author) : '';

                    if (synopsys_input.attr('aria-hidden') == 'true' && tinyMCE.activeEditor.getContent() === '') {
                        tinyMCE.activeEditor.setContent(response.summary);
                    } else {
                        synopsys_input.val() === '' ? synopsys_input.val(response.summary) : '';
                    }
                },
                error: function (response) {

                }
            })
        }
        $('#crunch').on('click', function(e){
            e.preventDefault();
            var srcs = JSON.parse($('textarea[name="images_srcs"]').val());
            var posit_id = $('input#post_ID').val();
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'crunch_images',
                    images_srcs: srcs,
                    post_id: posit_id
                },
                beforeSend: function () {

                },
                success: function (response) {
                    $('#crunch_info').html(parseInt(response.message));
                },
                error: function (response) {

                }
            });
            return false;
        })
    });
})(jQuery);

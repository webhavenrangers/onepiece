<?php

/**
 * Created by PhpStorm.
 * User: Phantom
 * Date: 01.07.2016
 * in order to set autoincrement value on the tables run next query
 * ALTER TABLE table AUTO_INCREMENT = 1;
 * for the next tables:
 * wp_images_to_download
 * wp_posts
 * wp_postmeta
 * Time: 16:35
 */
class Createmanga {
	/**
	 *
	 */
	const PAGE_TITILE = 'Create Manga Settings';
	/**
	 *
	 */
	const PLUGIN_TITILE = 'Create Manga';
	/**
	 *
	 */
	const MENU_TITILE = 'Create Manga';
	/**
	 *
	 */
	const MENU_SLUG = 'create-manga';
	/**
	 * @var array
	 */

	/**
	 *
	 */
	public static function init() {
		$createmanga = new Createmanga();
		$options     = Createmanga_Settings::$options;
		if ( (bool) $options['enable_cron_jobs']['enabled'] === true ) {
			new Createmanga_Crons();
		}
		$createmanga->create_images_to_downlooad_table();
	}


	/**
	 * Createmanga constructor.
	 */
	function __construct() {
		Createmanga_Settings::getInstance();
		$this->add_actions();
		$this->add_filters();
	}

	/**
	 *
	 */
	function create_images_to_downlooad_table() {
		global $wpdb;
		$sql1 = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}images_to_download` (
			 `id` bigint(20) NOT NULL AUTO_INCREMENT,
			 `status` varchar(255) NOT NULL,
			 `chapter_id` int(11) NOT NULL,
			 `attach_id` bigint(20) NOT NULL,
			 `num_in_chapter` int(11) NOT NULL,
			 `source_url` text NOT NULL,
			 `chapter_title` text NOT NULL,
			 `manga_id` int(11) NOT NULL,
			 `insert_date` datetime NOT NULL,
			 `downloaded_date` datetime NOT NULL,
			 PRIMARY KEY (`id`)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$wpdb->query( $sql1 );
	}

	/**
	 * @param $links
	 *
	 * @return mixed
	 */
	function plugin_settings_link( $links ) {
		$settings_link = '<a href="admin.php?page=' . self::MENU_SLUG . '">Settings</a>';
		array_unshift( $links, $settings_link );

		return $links;
	}


	/**
	 *
	 */
	function load_menu() {
		add_menu_page( self::PAGE_TITILE, self::MENU_TITILE, 'edit_others_posts', self::MENU_SLUG,
			array( $this, 'create_admin_page' ), 'dashicons-welcome-comments', 7 );
		add_submenu_page( self::MENU_SLUG, "API tests", "Test Page for api.mangahaven.org", 'edit_others_posts', self::MENU_SLUG.'test', array( $this, 'create_test_page' ) );
	}

	/**
	 *
	 */
	public function create_admin_page() { ?>
		<div class="wrap">
		<form method="post" action="options.php"><?php
			// This prints out all hidden setting fields
			settings_fields( Createmanga_Settings::$options_name . '_group' );
			do_settings_sections( self::MENU_SLUG );
//			$crons = new Createmanga_Crons();
//			$crons->add_chapters_for_ongoings();
			submit_button(); ?>
		</form>
		</div><?php
	}

	/**
	 *
	 */
	public function create_test_page() { ?>
		<div class="wrap">
		<form method="post" action="options.php">
			<h1>Test routs links:</h1>
			<br><label>Get Manga Data: <a target="_blank" href="http://api.mangahaven.org/rest/manga/aHR0cDovL2tpc3NtYW5nYS5jb20vTWFuZ2EvS3Vyb3NoaXRzdWpp">http://api.mangahaven.org/rest/manga/aHR0cDovL2tpc3NtYW5nYS5jb20vTWFuZ2EvS3Vyb3NoaXRzdWpp</a></label>
			<br><label>Get Chapterslist For Manga: <a target="_blank" href="http://api.mangahaven.org/rest/chapterlist/aHR0cDovL2tpc3NtYW5nYS5jb20vTWFuZ2EvS3Vyb3NoaXRzdWpp">http://api.mangahaven.org/rest/manga/aHR0cDovL2tpc3NtYW5nYS5jb20vTWFuZ2EvS3Vyb3NoaXRzdWpp</a></label>
			<br><label>Get Chapter Data (images): <a target="_blank" href="http://api.mangahaven.org/rest/chapter/aHR0cDovL2tpc3NtYW5nYS5jb20vTWFuZ2EvV2l0Y2gtQ3JhZnQtV29ya3MvMz9pZD0yNzAyNQ%3D%3D">aHR0cDovL2tpc3NtYW5nYS5jb20vTWFuZ2EvV2l0Y2gtQ3JhZnQtV29ya3MvMz9pZD0yNzAyNQ%3D%3D</a></label>


			<?php
			global $wpdb;
			$images = $wpdb->get_var( "SELECT COUNT(id)  FROM `wp_images_to_download` WHERE `status` NOT LIKE 'downloaded'" );
			$present_images = $wpdb->get_var( "SELECT COUNT(ID) FROM `wp_posts` WHERE `post_type` LIKE 'attachment'" );
			$published_chapters = $wpdb->get_var( "SELECT COUNT(ID) FROM `wp_posts` WHERE `post_type` LIKE 'chapter' AND `post_status` = 'publish'" );
			$published_mangas = $wpdb->get_var( "SELECT COUNT(ID) FROM `wp_posts` WHERE `post_type` LIKE 'manga' AND `post_status` = 'publish'" );
			$total_chapters = $wpdb->get_var( "SELECT COUNT(ID) FROM `wp_posts` WHERE `post_type` LIKE 'chapter'" );
			$total_mangas = $wpdb->get_var( "SELECT COUNT(ID) FROM `wp_posts` WHERE `post_type` LIKE 'manga'" );
			echo "<p>IMAGES IN DOWNLOAD QUERY: {$images}</p>";
			echo "<p>IMAGES DOWNLOADED: {$present_images}</p>";
			echo "<p>PUBLISHED CHAPTERS: {$published_chapters}</p>";
			echo "<p>PUBLISHED MANGAS: {$published_mangas}</p>";
			echo "<p>TOTAL CHAPTERS: {$total_chapters}</p>";
			echo "<p>TOTAL MANGAS: {$total_mangas}</p>";


//
//			settings_fields( Createmanga_Settings::$options_name . '_group' );
//			do_settings_sections( self::MENU_SLUG );
//			submit_button(); ?>
		</form>
		</div><?php
	}


	/**
	 *
	 */
	function enqueue_admin() {
		wp_enqueue_script( self::MENU_SLUG . 'script', plugins_url( '../js/script.js', __FILE__ ), array( 'jquery' ),
			true );

		wp_enqueue_style( self::MENU_SLUG . 'admin-style', plugin_dir_url( __FILE__ ) . '../css/admin-style.css', false,
			'1.0.0' );
	}

	/**
	 *
	 */
	function add_actions() {
		add_action( 'admin_menu', array( $this, 'load_menu' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin' ) );
		add_action( 'admin_menu', array( $this, 'broken_chapters_list_page' ) );
		add_action( 'wp_before_admin_bar_render', array( $this, 'broken_chapters_count' ) );
	}


	/**
	 *
	 */
	function add_filters() {
		add_filter( 'plugin_action_links_' . CREATEMANGA_PLUGIN_BASENAME, array( $this, 'plugin_settings_link' ), 10,
			2 );
		add_filter( 'rest_prepare_chapter', array( $this, 'mh_rest_prepare_post', 10, 3 ) );
		add_filter( 'rest_query_vars', array( $this, 'mh_allow_meta_query' ) );
		add_filter( 'rest_query_vars', array( $this, 'mh_allowed_post_status' ) );
		add_filter( 'http_request_timeout', array( $this, 'set_request_timeout' ) );
	}


	/**
	 * @return int
	 */
	function set_request_timeout() {
		return Createmanga_Settings::$options['request_timeout'];
	}


	/**
	 *
	 */
	public function page_init() {
		register_setting( Createmanga_Settings::$options_name . '_group', // Option group
			Createmanga_Settings::$options_name, // Option name
			array( $this, 'sanitize' ) // Sanitize
		);

		add_settings_section( 'setting_section_id', // ID
			self::PAGE_TITILE, // Title
			array( $this, 'print_section_info' ), // Callback
			self::MENU_SLUG // Page
		);

		add_settings_field( 'request_timeout', // ID
			'HTTP request timeout (seconds)', // Title
			array( $this, 'request_timeout_callback' ), // Callback
			self::MENU_SLUG, // Page
			'setting_section_id' // Section
		);
		add_settings_field( 'mangas_per_day', // ID
			'Create mangas per day: ', // Title
			array( $this, 'mangas_per_day_callback' ), // Callback
			self::MENU_SLUG, // Page
			'setting_section_id' // Section
		);
		add_settings_field( 'watermark_rate', // ID
			'Watermark rate: ', // Title
			array( $this, 'watermark_rate_callback' ), // Callback
			self::MENU_SLUG, // Page
			'setting_section_id' // Section
		);

		add_settings_section( 'enable_cron_jobs', // ID
			'Enable Cronjobs ', // Title
			array( $this, 'print_cronjobs_info' ), // Callback
			self::MENU_SLUG // Page
		);

		add_settings_field( 'enabled', // ID
			'Enable CRON (disables all cronjobs)', // Title
			array( $this, 'enable_cron_jobs_callback' ), // Callback
			self::MENU_SLUG, // Page
			'enable_cron_jobs' // Section
		);

		add_settings_field( 'add_chapters_for_ongoings', // ID
			'Add chapters for ongoing series :', // Title
			array( $this, 'add_chapters_for_ongoings_callback' ), // Callback
			self::MENU_SLUG, // Page
			'enable_cron_jobs' // Section
		);

		add_settings_field( 'download_images', // ID
			'Download images (no rest calls needed): ', // Title
			array( $this, 'download_images_callback' ), // Callback
			self::MENU_SLUG, // Page
			'enable_cron_jobs' // Section
		);

		add_settings_field( 'add_manga_data', // ID
			'Add manga data from MYAL and Mangafox (no rest calls needed): ', // Title
			array( $this, 'add_manga_data_callback' ), // Callback
			self::MENU_SLUG, // Page
			'enable_cron_jobs' // Section
		);

		add_settings_field( 'create_download_images_order', // ID
			'Create download images list: ', // Title
			array( $this, 'create_download_images_order_callback' ), // Callback
			self::MENU_SLUG, // Page
			'enable_cron_jobs' // Section
		);

		add_settings_field( 'create_N_mangas', // ID
			'Create new mangas: ', // Title
			array( $this, 'create_N_mangas_callback' ), // Callback
			self::MENU_SLUG, // Page
			'enable_cron_jobs' // Section
		);

		add_settings_section( 'myal_settings_id', // ID
			'My Anime List API credentials', // Title
			'', // Callback
			self::MENU_SLUG // Page
		);

		add_settings_field( 'myal_login', // ID
			'Login:', // Title
			array( $this, 'myal_login_callback' ), // Callback
			self::MENU_SLUG, // Page
			'myal_settings_id' // Section
		);

		add_settings_field( 'myal_password', // ID
			'Password:', // Title
			array( $this, 'myal_password_callback' ), // Callback
			self::MENU_SLUG, // Page
			'myal_settings_id' // Section
		);

	}

	/**
	 * @param $input
	 *
	 * @return array
	 */
	public function sanitize( $input ) {
		$new_input = array();

		if ( isset( $input['enable_cron_jobs'] ) ) {
			$new_input['enable_cron_jobs'] =
				array_merge( Createmanga_Settings::$defaults['enable_cron_jobs'], $input['enable_cron_jobs'] );
		} else {
			$new_input['enable_cron_jobs'] = Createmanga_Settings::$options;
		}

		if ( isset( $input['request_timeout'] ) ) {
			$new_input['request_timeout'] = (int) $input['request_timeout'];
		}

		if ( isset( $input['mangas_per_day'] ) ) {
			$new_input['mangas_per_day'] = (int) $input['mangas_per_day'];
		}
		if ( isset( $input['watermark_rate'] ) ) {
			$new_input['watermark_rate'] = (int) $input['watermark_rate'];
		}

		if ( isset( $input['myal_credentials'] ) ) {
			$new_input['myal_credentials'] = $input['myal_credentials'];
		}

		return $new_input;
	}

	/**
	 *
	 */
	public function print_section_info() {
		print '';
	}

	/**
	 *
	 */
	public function print_cronjobs_info() {
		print 'Enable crons and specify the jobs';
	}

	/**
	 *
	 */
	public function enable_cron_jobs_callback() {
		echo '<input class="" type="checkbox" id="enable_cron_jobs" name="' . Createmanga_Settings::$options_name .
		     '[enable_cron_jobs][enabled]" value="1"  ' .
		     checked( Createmanga_Settings::$options['enable_cron_jobs']['enabled'], 1, false ) . '/>';

	}

	/**
	 *
	 */
	public function add_chapters_for_ongoings_callback() {
		echo '<input class="" type="checkbox" id="add_chapters_for_ongoings" name="' .
		     Createmanga_Settings::$options_name . '[enable_cron_jobs][add_chapters_for_ongoings]" value="1"  ' .
		     checked( Createmanga_Settings::$options['enable_cron_jobs']['add_chapters_for_ongoings'], 1, false ) .
		     '/>';

	}

	/**
	 *
	 */
	public function download_images_callback() {
		echo '<input class="" type="checkbox" id="add_chapters_for_ongoings" name="' .
		     Createmanga_Settings::$options_name . '[enable_cron_jobs][download_images]" value="1"  ' .
		     checked( Createmanga_Settings::$options['enable_cron_jobs']['download_images'], 1, false ) . '/>';

	}

	/**
	 *
	 */
	public function add_manga_data_callback() {
		echo '<input class="" type="checkbox" id="add_manga_data" name="' . Createmanga_Settings::$options_name .
		     '[enable_cron_jobs][add_manga_data]" value="1"  ' .
		     checked( Createmanga_Settings::$options['enable_cron_jobs']['add_manga_data'], 1, false ) . '/>';
	}

	/**
	 *
	 */
	public function create_download_images_order_callback() {
		echo '<input class="" type="checkbox" id="create_download_images_order" name="' .
		     Createmanga_Settings::$options_name . '[enable_cron_jobs][create_download_images_order]" value="1"  ' .
		     checked( Createmanga_Settings::$options['enable_cron_jobs']['create_download_images_order'], 1, false ) .
		     '/>';
	}

	/**
	 *
	 */
	public function create_N_mangas_callback() {
		echo '<input class="" type="checkbox" id="create_N_mangas" name="' . Createmanga_Settings::$options_name .
		     '[enable_cron_jobs][create_N_mangas]" value="1"  ' .
		     checked( Createmanga_Settings::$options['enable_cron_jobs']['create_N_mangas'], 1, false ) . '/>';
	}

	/**
	 *
	 */
	public function request_timeout_callback() {
		echo '<input class="" type="text" id="request_timeout" name="' . Createmanga_Settings::$options_name .
		     '[request_timeout]" value="' . Createmanga_Settings::$options['request_timeout'] . '"/>';

	}

	/**
	 *
	 */
	public function mangas_per_day_callback() {
		echo '<input class="" type="text" id="mangas_per_day" name="' . Createmanga_Settings::$options_name .
		     '[mangas_per_day]" value="' . Createmanga_Settings::$options['mangas_per_day'] . '"/>';

	}

	/**
	 *
	 */
	public function watermark_rate_callback() {
		echo '<input class="" type="text" id="watermark_rate" name="' . Createmanga_Settings::$options_name .
		     '[watermark_rate]" value="' . Createmanga_Settings::$options['watermark_rate'] . '"/>%';

	}

	/**
	 *
	 */
	public function myal_login_callback() {
		echo '<input class="" type="text" id="myal_login" name="' . Createmanga_Settings::$options_name .
		     '[myal_credentials][login]" value="' . Createmanga_Settings::$options['myal_credentials']['login'] . '"/>';

	}

	/**
	 *
	 */
	public function myal_password_callback() {
		echo '<input class="" type="text" id="myal_password" name="' . Createmanga_Settings::$options_name .
		     '[myal_credentials][password]" value="' . Createmanga_Settings::$options['myal_credentials']['password'] .
		     '"/>';

	}

	/**
	 *
	 */
	function broken_chapters_list_page() {
		add_submenu_page( 'edit.php', 'Broken Chapters', 'Broken Chapters', 'delete_others_posts', 'dead_chapter',
			[ $this, 'get_posts_with_dead_chapter' ] );
	}

	/**
	 *
	 */
	function broken_chapters_count() {
		global $wp_admin_bar;
		$count = $this->get_posts_with_dead_chapter( true );


		$wp_admin_bar->add_node( array(
			'parent' => null,
			'id'     => 'broken_feeds_count',
			'title'  => __( '<font style="' . ( ( $count > 0 ) ? 'color:rgb(250,20,20);font-weight:bold;' : '' ) .
			                '">Broken chapters: ' . $count . '</font>' ),
			'href'   => ( ( is_admin() ) ? menu_page_url( "dead_chapter", false ) :
				admin_url( "admin.php?page=dead_chapter" ) ),
			'meta'   => array(
				"title" => __( 'There are ' . $count . " broken chapters found." ),
				"class" => ""
			)
		) );
	}

	/**
	 * @param bool $count_only
	 *
	 * @return mixed
	 */
	function get_posts_with_dead_chapter( $count_only = false ) {
		if ( $count_only === true ) {
			global $wpdb;
			$sql   = "SELECT COUNT(`ID`) FROM `" . $wpdb->posts .
			         "` p  INNER JOIN $wpdb->postmeta pm ON (p.ID = pm.post_id) WHERE 1=1  AND p.post_type = 'chapter' AND (p.post_status = 'publish' OR p.post_status = 'private' OR p.post_status = 'draft')  AND pm.meta_key = 'dead_chapter' AND pm.meta_value = 1 ";
			$posts = $wpdb->get_results( $sql, ARRAY_A );

			return ( $posts[0]["COUNT(`ID`)"] );
		}

		$p     = isset( $_GET['p'] ) ? intval( $_GET['p'] ) : 1;
		$args  = array(
			'post_type'      => 'chapter',
			'orderby'        => 'date',
			'meta_query'     => array(
				'relation' => 'AND', // Optional, defaults to "AND"
				array(
					'key'     => 'dead_chapter',
					'value'   => 1,
					'compare' => 'LIKE'
				)
			),
			'posts_per_page' => 30,
			'paged'          => $p,
		);
		$query = new WP_Query( $args );
		$posts = $query->get_posts(); ?>
		<div class="wrap dead_chapter"><?php
		if ( count( $posts ) > 0 ) {
			ob_start() ?>
			<h3>Broken Chapters</h3>
			<button onclick="jQuery('.fetch_images_for_chapter').click();">
				Fetch All
			</button>
			<?php
			echo $this->print_posts_table( $posts );
			echo '<br>';
			echo $this->print_pagination( $query, $p );
			$output_list = ob_get_contents();
			ob_clean();
			echo $output_list;
		} else { ?>
			<p><?php echo __( "Nothing found." ); ?></p>
			<?php
		}
		?>
		</div><?php
	}

	/**
	 * @param $posts
	 * @param bool $inline
	 *
	 * @return string
	 */
	function print_posts_table( $posts, $inline = true ) {
		ob_start(); ?>
		<style type="text/css">
			.spinner {
				background: url(<?php echo admin_url( '/images/spinner.gif' ); ?>) no-repeat;
				-webkit-background-size: 20px 20px;
				background-size: 20px 20px;
				display: none;
				float: right;
				opacity: .7;
				filter: alpha(opacity=70);
				width: 20px;
				height: 20px;
				margin: 2px 5px 0;
			}

			.purge_custom_links {
				display: block;
				color: #FF030C;
			}

			.purge_custom_links.loading {
				background: url(<?php echo get_template_directory_uri(). '/images/load.gif'; ?>) center center no-repeat;
				text-indent: -9999px;
				width: 30px;
			}

			.green-row {
				background-color: #90ee90;
			}

			.red-row {
				background-color: rgba(255, 3, 12, 0.6);
			}

			#text-info {
				display: none;
			}
		</style>
		<table class="wp-list-table widefat fixed posts">
		<thead>
		<tr>
			<th style="width: 10%;">#</th>
			<th style="width: 30%;">Post Title</th>
			<th style="width: 20%;">Actions</th>
			<th style="width: 20%;">Origin Link</th>
			<th style="width: 10%;">Status</th>
		</tr>
		</thead>
		<?php
		foreach ( $posts as $pi => $post ) {
			$edit_links =
				'<a class="" target="_blank" href="' . get_edit_post_link( $post->ID, "" ) . '">Edit the Post</a>';

//		recover_youtube_shortcode_from_revisions($post);
			$origin_url_encoded = get_post_meta( $post->ID, 'origin_url', true );
			$post_status        = get_post_meta( $post->ID, 'dead_chapter', true );
			$post_status        = ( ! empty( $post_status ) ) ?
				'<span  data-post_id="' . $post->ID . '" class="wpseo-score-icon bad"></span>' :
				'<span  data-post_id="' . $post->ID . '" class="wpseo-score-icon good"></span>'; ?>
		<tr <?php if ( $pi % 2 == 0 ) {
			echo 'class="alternate"';
		}
		echo 'id=' . $post->ID; ?>>
			<td style="width: 10%;"><?php echo( $pi + 1 ); ?>
				<span class="spinner"></span>
				<span class="post_id_mark hidden"><?php echo $post->ID; ?></span>
			</td>
			<td class="post-title" style="width: 30%;"><?php echo $post->post_title; ?></td>
			<td meta-data="dead_chapter" class="post-edit" style="width: 40%;">
				<?php echo $edit_links; ?><br>
				<button data-post_id="<?php echo $post->ID; ?>" data-replace="1"
				        data-origin_url="<?php echo $origin_url_encoded; ?>"
				        type="button" class="button fetch_images_for_chapter" value="Fetch">
						<span style="margin-left:0!important; float:left; visibility: visible; display:none;"
						      class="spinner"></span>Fetch
				</button>
			</td>
			<td style="width: 20%;">
				<a id="origin_link" target="_blank"
				   href="http://kissmanga.com<?php echo base64_decode( $origin_url_encoded ); ?>"
				   class="widefat"><?php echo $post->post_title; ?></a>
			</td>
			<td style="width: 10%;"><?php echo $post_status; ?></td>
			</tr><?php
		} ?>
		</table><?php
		$table = ob_get_clean();

		return $table;
	}

	/**
	 * @param $query
	 * @param $p
	 *
	 * @return string
	 */
	function print_pagination( $query, $p ) {
		$pages    = $query->max_num_pages;
		$total    = $query->found_posts;
		$this_url = get_admin_url() . "admin.php?page=" . $_GET['page'];
		if ( $p > 1 ) {
			$prev = $p - 1;
		} else {
			$prev = 1;
		}
		if ( $p < $pages ) {
			$next = $p + 1;
		} else {
			$next = $pages;
		}
		ob_start(); ?>
		<div class="tablenav bottom">
		<div class="tablenav-pages">
			<span class="displaying-num"><?php echo $total; ?> items</span>
			<span class="pagination-links">
				<a class="first-page" title="Go to the first page" href="<?php echo "{$this_url}&p=1"; ?>">&laquo;</a>
				<a class="prev-page" title="Go to the previous page"
				   href="<?php echo "{$this_url}&p={$prev}"; ?>">&lsaquo;</a>
				<span class="paging-input"><?php echo $p; ?> of <span
						class="total-pages"><?php echo $pages; ?></span></span>
				<a class="next-page" title="Go to the next page"
				   href="<?php echo "{$this_url}&p={$next}"; ?>">&rsaquo;</a>
				<a class="last-page" title="Go to the last page"
				   href="<?php echo "{$this_url}&p={$pages}"; ?>">&raquo;</a>
			</span>
		</div>
		</div><?php
		$pagination = ob_get_clean();

		return $pagination;
	}


	/**
	 * Add REST API give post_parent for all posts.
	 */
	function mh_rest_prepare_post( $data, $post, $request ) {
		$_data                = $data->data;
		$_data['post_parent'] = wp_get_post_parent_id( $post->ID );

		$data->data = $_data;

		return $data;
	}

	/**
	 * Add REST API support to use get_posts by metakey-value
	 */
	function mh_allow_meta_query( $valid_vars ) {

		$valid_vars = array_merge( $valid_vars, array( 'meta_key', 'meta_value' ) );

		return $valid_vars;
	}


}


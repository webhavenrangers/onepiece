<?php

/**
 * Created by PhpStorm.
 * User: hortt
 * Date: 17.08.16
 * Time: 19:38
 * this is singleton class for metaboxes
 */
class Createmanga_Metabox {
	/**
	 *
	 */
	public static function init() {
		new Createmanga_Metabox();
	}


	/**
	 * Createmanga_Metabox constructor.
	 */
	function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_metaboxes' ) );
		add_action( 'save_post', array( $this, 'mh_save_meta_box' ) );
	}

	/**
	 *
	 */
	function add_metaboxes() {
		add_meta_box( 'fetch_chapter_images', 'Fetch Chapter Images', [ $this, 'render_fetch_images_metabox' ],
			'chapter', 'normal', 'high' );

		add_meta_box( 'manually_add_images', 'Add Chapter Images', [ $this, 'manually_proceed_images_for_chapter_metabox' ],
			'chapter', 'normal', 'high' );
		// Chapters
		add_meta_box( 'mh-meta-box-chapters-list', 'Chapters (list)', [ $this, 'mh_metabox_chapters_list' ], 'manga',
			'normal', 'default' );

		add_meta_box( 'mh-meta-box-chapters-parent', 'Set Parent', [ $this, 'mh_metabox_chapters_parent' ], 'chapter',
			'side', 'default' );

		// Mangas data
		add_meta_box( 'mh-meta-box-mangas-data', 'Manga\'s data', [ $this, 'mh_render_mangas_data_meta_box' ], 'manga',
			'normal', 'default' );

		// Attachment
		add_meta_box( 'mh-meta-box-thumbnail_select', 'KISSMANGA/MAL image',
			[ $this, 'mh_render_thumbnail_select_meta_box' ], 'manga', 'normal', 'default' );

		// Manga on chapter page
//		add_meta_box( 'mh-meta-box-id-manga', 'Manga info', [ $this,'mh_meta_box_manga'], 'chapter', 'normal', 'default' );

	}

	function manually_proceed_images_for_chapter_metabox(){?>
		<legend>
			go to kissmanga and run it from console:
			var a = []; jQuery('img[onload]').each(function(){a.push(jQuery(this).attr('src'))});
			copy(JSON.stringify(a))
			take srcs and paste to textarea below:
		</legend>
		<textarea class="widefat"  name="images_srcs">
		</textarea>
		<button id="crunch" class="button">
			crunch!
		</button>
		<span id="crunch_info"></span>
		<?php
	}


	/**
	 *
	 */
	function render_fetch_images_metabox() {
		global $post;
		$args               = array(
			'post_parent' => $post->ID,
			'post_type'   => 'attachment',
			'numberposts' => - 1,
			'post_status' => 'any',
			'orderby'     => 'ID',
			'order'       => 'ASC',
		);
		$children_array     = get_children( $args );
		$count              = count( $children_array );
		$status_meta        = get_post_meta( $post->ID, 'dead_chapter', true );
		$origin_url_encoded = get_post_meta( $post->ID, 'origin_url', true );
		$data_replace       = $count != 0 ? 1 : 0;
		$status             = empty( $status_meta ) && $count !== 0 ?
			'<span  data-post_id="' . $post->ID . '" class="wpseo-score-icon good"></span>' :
			'<span data-post_id="' . $post->ID . '" class="wpseo-score-icon bad"></span>';
		wp_nonce_field( 'mh_save_data' . $post->ID, 'mh_data_meta_box_nonce' ); ?>
		<table class="widefat">
		<tr>
			<th>Status</th>
			<td><?php echo $status; ?></td>
		</tr>
		<tr>
			<th><label for="origin_url"><strong>Origin URL</strong></label></th>
			<td>
				<input type="text" name="origin_url" id="origin_url"
				       value="<?php echo base64_decode( $origin_url_encoded ); ?>" class="widefat"/>
			</td>
		</tr>
		<tr>
			<th><label for="parent_manga"><strong>Parent Manga</strong></label></th>
			<td>
				<?php echo '<a href="'.get_permalink( $post->post_parent ).'" >'.get_the_title($post->post_parent).'</a>';?>
				|| <?php echo '<a href="'.get_edit_post_link( $post->post_parent ).'" >Edit link</a>';?>
			</td>
		</tr>
		<tr>
			<th><label for="origin_url"><strong>Encoded URL</strong></label></th>
			<td>
				<?php echo urlencode( Createmanga_Utils::fix_origin_url_maybe($origin_url_encoded) ); ?>
			</td>
		</tr>
		<tr>
			<th><label><strong>Link</strong></label></th>
			<td>
				<a id="origin_link" target="_blank"
				   href="http://kissmanga.com<?php echo base64_decode( $origin_url_encoded ); ?>" class="widefat">Open
					Origin url</a>
			</td>
		</tr>
		<tr>
			<th>Images Count</th>
			<td class="img_count"><?php echo $count; ?></td>
		</tr>
		<tr>
			<th>
				<button data-post_id="<?php echo $post->ID; ?>" data-replace="<?php echo $data_replace; ?>"
				        data-origin_url="<?php echo $origin_url_encoded; ?>"
				        type="button" class="button fetch_images_for_chapter" value="Fetch">
						<span style="margin-left:0!important; float:left; visibility: visible; display:none;"
						      class="spinner"></span>Fetch
				</button>
			</th>
			<td class="fetch_result"></td>
		</tr>
		</table><?php
		foreach ( $children_array as $item ) {
			$chapter_small = wp_get_attachment_image( $item->ID, 'medium', false );
			echo '<div>ID: ' . $item->ID . '</div>';
			echo '<div>Title: ' . $item->post_title . '</div>';
			echo $chapter_small . "<br />";
		}
	}

	function mh_metabox_chapters_parent() {
		global $post;
		$id_manga  = $post->post_parent;
		$title     = $id_manga == 0 ? '' : get_the_title( $id_manga );
		$edit_link = $id_manga == 0 ? '' :
			'<a class="edit_link" href="' . get_permalink( $id_manga ) . '">Edit: ' . $title . '</a>';
		?>
		<p><strong>Manga title</strong></p>
		<div class="tabs-panel tabs-panel-active sa_search" id="tabs-panel-posttype-page-search">
			<p class="quick-search-wrap">
				<label for="quick-search-posttype-page" class="screen-reader-text">Search</label>
				<input type="search" class="widefat" placeholder="Search" value="<?php echo $title; ?>" name="" id="s"
				       autocomplete="off">
				<input type="number" value="<?php echo $id_manga; ?>" name="post_parent" autocomplete="off">
				<span class="spinner"></span>
				<input type="submit" name="submit" id="submit-quick-search-posttype-page"
				       class="button button-small quick-search-submit hide-if-js" value="Search">
			</p>
			<?php echo $edit_link; ?>
			<div class="sa_wrapper dropdown-menu"></div>
		</div>
		<?php
	}

	/**
	 *
	 */
	function mh_render_mangas_data_meta_box() {
		global $post;
		$altername_names_esc = esc_attr( get_post_meta( $post->ID, 'altr_nms', true ) );
		$count_of_chapters   = get_post_meta( $post->ID, 'cnt_children', true );
		$origin_url          = get_post_meta( $post->ID, 'origin_url', true );
		$release_year        = get_post_meta( $post->ID, 'release_year', true );

		$views_this_week = 0;
		$total_views     = 0;
		$updated_date    = get_post_meta( $post->ID, 'updt_dte', true );
		$sldr            = get_post_meta( $post->ID, 'sldr', true );
		$rating          = get_post_meta( $post->ID, 'rating', true );
		$status          = get_post_meta( $post->ID, 'status', true );
		if ( is_null( $rating ) ) {
			$rating = 'no data';
		}

		wp_nonce_field( 'mh_save_data' . $post->ID, 'mh_data_meta_box_nonce' ); ?>
		<p>
			<label for="origin_url"><strong>Origin URL(should looks like: "/Manga/Koutetsu-Jeeg" or
					"http://kissmanga.com/Manga/Denkigai-no-Honya-san")</strong></label>
			<input type="text" name="origin_url" id="origin_url" value="<?php echo base64_decode( $origin_url ); ?>"
			       class="widefat"/>
			Encoded url: <?php echo urlencode(Createmanga_Utils::fix_origin_url_maybe($origin_url)); ?>
		</p>
		<table class="widefat striped">
			<thead>
			<tr>
				<th>Source</th>
				<th>Link</th>
				<th>Action</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>My Anime List:</td>
				<td><a target="_blank" href="https://myanimelist.net/manga.php?q=<?php echo $post->post_title; ?>">https://myanimelist.net/manga.php?q=<?php echo $post->post_title; ?></a>
				</td>
				<td><!--<button data-post_id="<?php echo $post->ID; ?>" type="button" class="button fetch_manga_data" data-source="myal" value="Fetch">
					<span style="margin-left:0!important; float:left; visibility: visible; display:none;" class="spinner"></span>Fetch</button>--></td>
			</tr>
			<tr>
				<td>Mangafox:</td>
				<td><a target="_blank" href="http://mangafox.me/manga/<?php echo str_replace( '-', '_',
							sanitize_title( $post->post_title ) ) . '/'; ?>"><?php echo $post->post_title; ?></a></td>
				<td>
					<button data-post_id="<?php echo $post->ID; ?>" type="button" class="button fetch_manga_data"
					        data-source="mangafox" value="Fetch">
						<span style="margin-left:0!important; float:left; visibility: visible; display:none;"
						      class="spinner"></span>Fetch
					</button>
				</td>
			</tr>
			<tr>
				<td>Kissmanga:</td>
				<td><a id="origin_link" target="_blank"
				       href="<?php echo base64_decode( $origin_url ); ?>"
				       class="widefat"><?php echo $post->post_title ?></a></td>
				<td>
					<button data-post_id="<?php echo $post->ID; ?>" type="button" class="button fetch_manga_data"
					        data-source="kissmanga" value="Fetch">
						<span style="margin-left:0!important; float:left; visibility: visible; display:none;"
						      class="spinner"></span>Fetch
					</button>
				</td>
			</tr>
			</tbody>
		</table>


		<p>
			<label for="release_year"><strong>Release year</strong></label>
			<input type="text" name="release_year" id="release_year" value="<?php echo $release_year; ?>"
			       class="widefat"/>
		</p>
		<p>
			<label for="altr_nms"><strong>Alternate names</strong></label>
			<input type="text" name="altr_nms" id="altr_nms" value="<?php echo $altername_names_esc; ?>"
			       class="widefat"/><br/>
		</p>
		<p>
			<label for="status"><strong>Status</strong></label>
			<input type="text" name="status" id="status" value="<?php echo $status; ?>" class="widefat"/><br/>
		</p>
		<p>
			<label for="rating"><strong>Rating</strong></label>
			<input type="text" name="rating" id="rating" value="<?php echo $rating; ?>" class="widefat"/><br/>
		</p>
		<p>
			<label for="count_of_chapters"><strong>Count of chapters</strong></label>
			<?php echo $count_of_chapters; ?>
		</p>
		<p>
			<label for="views_this_week"><strong>Views this week</strong></label>
			<?php echo $views_this_week; ?>
		</p>
		<p>
			<label for="total_views"><strong>Total views</strong></label>
			<?php echo $total_views; ?>
		</p>
		<p>
			<label for="updated_date"><strong>Updated date</strong></label>
			<?php echo $updated_date; ?>
		</p>
		<p>
			<input type="hidden" name="mh_change_sldr" value="1">
			<label for="sldr"><strong>Use for home page slider</strong></label>
			<input type="checkbox" name="sldr" id="sldr" <?php echo( $sldr == '1' ? 'checked' : '' ); ?> />
			<?php echo $sldr; ?>
		</p>
		<?php
	}


	/**
	 *
	 */
	function mh_metabox_chapters_list() {
		global $post;
		$tmp           = $post;
		$args          = [
			'post_type'      => 'chapter',
			'posts_per_page' => - 1,
			'post_parent'    => $post->ID,
			'orderby'        => 'ID',
			'order'          => 'DESC',
		];
		$chapter_query = new WP_Query( $args );
		if ( $chapter_query->have_posts() ) {
			$this->render_present_chapters_table($chapter_query, $post);
		} else {
			$manga_origin_url = get_post_meta( $post->ID, 'origin_url', true );
			$remote_chapters  = Createmanga_Utils::rest_get_chapters_list( $manga_origin_url );
			if ( $remote_chapters ) {
				$this->render_remote_chapters_table( $remote_chapters, $post->ID );
			}
		}
		$post = $tmp;
	}



	// When post is saving
	/**
	 * @param $post_id
	 */
	function mh_save_meta_box( $post_id ) {

		if ( isset( $_POST['mh_data_meta_box_nonce'] ) &&
		     wp_verify_nonce( $_POST['mh_data_meta_box_nonce'], 'mh_save_data' . $post_id )
		) {
			if ( isset( $_POST['release_year'] ) ) {
				update_post_meta( $post_id, 'release_year', $_POST['release_year'] );
			}

			if ( isset( $_POST['altr_nms'] ) ) {
				update_post_meta( $post_id, 'altr_nms', $_POST['altr_nms'] );
			}

			if ( isset( $_POST['rating'] ) ) {
				update_post_meta( $post_id, 'rating', $_POST['rating'] );
			}

			if ( isset( $_POST['status'] ) ) {
				update_post_meta( $post_id, 'status', $_POST['status'] );
			}

			if ( isset( $_POST['origin_url'] ) ) {
				update_post_meta( $post_id, 'origin_url', base64_encode( $_POST['origin_url'] ) );
			}

			if ( isset( $_POST['mh_change_sldr'] ) ) {
				if ( ( $_POST['sldr'] == 'on' ) ) {
					update_post_meta( $post_id, 'sldr', 1 );
				} else {
					delete_post_meta( $post_id, 'sldr' );
				}
			}
		}

		// if thumbnail was changed

		if ( isset( $_POST['mh_thumbnail_select_meta_box'] ) &&
		     wp_verify_nonce( $_POST['mh_thumbnail_select_meta_box'], 'mh_thumbnail_select' . $post_id )
		) {
			if ( isset( $_POST['current_thumbnail'] ) && ! empty( $_POST['current_thumbnail'] ) ) {
				set_post_thumbnail( $post_id, $_POST['current_thumbnail'] );
			}
		}

	}


	/**
	 *
	 */
	function mh_render_thumbnail_select_meta_box() {
		global $post;

		wp_nonce_field( 'mh_thumbnail_select' . $post->ID, 'mh_thumbnail_select_meta_box' );

		$tmp_post               = $post;
		$post_thumbnail_id      = get_post_thumbnail_id( $post );
		$attachment_image_posts = new WP_Query( array(
			'posts_per_page' => 999,
			'post_type'      => 'attachment',
			'post_parent'    => $post->ID,
			'post_status'    => 'inherit',
			'orderby'        => 'post_title',
			'order'          => 'ASC',
		) );
		if ( ! empty( $post_thumbnail_id ) && ! $attachment_image_posts->have_posts() ) {
			wp_update_post( array(
				'ID'          => $post_thumbnail_id,
				'post_parent' => $post->ID
			) );
			$img_data = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail', false );
			$img_url  = $img_data[0];
			?>
			<div class="thumb-selector">
			<input
				id="thumb_<?php echo $post_thumbnail_id; ?>"
				type="radio"
				value="<?php echo $post_thumbnail_id; ?>"
				name="current_thumbnail" checked="checked">
			<label class="thumb-cc" for="thumb_<?php echo $post_thumbnail_id; ?>"
			       style="background-image: url('<?php echo $img_url; ?>');"></label>
			</div><?php
		} elseif ( $attachment_image_posts->have_posts() ) {
			?>
			<div class="thumb-selector">
				<?php
				while ( $attachment_image_posts->have_posts() ) {
					$attachment_image_posts->the_post();
					$img_data = wp_get_attachment_image_src( get_the_ID(), 'thumbnail', false );
					$img_url  = $img_data[0];
					$checked  = '';
					if ( $post_thumbnail_id == get_the_ID() ) {
						$is_active = 'thumbnail-active';
						$checked   = 'checked';
					}
					$thumb_id = get_the_ID();
					?>
					<input
						id="thumb_<?php echo $thumb_id; ?>"
						type="radio"
						value="<?php echo $thumb_id; ?>"
						name="current_thumbnail" <?php echo $checked; ?>
					>
					<label class="thumb-cc" for="thumb_<?php echo $thumb_id; ?>"
					       style="background-image: url('<?php echo $img_url; ?>');"></label>
					<?php
				}
				?>
			</div>
			<?php
		}
		$attachment_image_posts->reset_postdata();
		$post = $tmp_post;
	}


	function render_present_chapters_table( $chapter_query, $post ) { ?>
		<table class="widefat striped">
		<caption><b class="alignleft">Chapters Found: <?php echo $chapter_query->found_posts; ?></b><br>
			<button id="process_all_chapters" data-action="fetch" style="float: left; margin: 10px 0;" class="button button-primary">Run
				Batch
			</button>
		</caption>

		<thead>
		<tr>
			<th>Chapter's name</th>
			<th>Origin Url</th>
			<th>Action</th>
			<th>Status</th>
		</tr>
		</thead><?php
		foreach (  $chapter_query->posts as $chapter ) {
			$dead_chapter = get_post_meta($chapter->ID, 'dead_chapter', true);
			$is_fetched = $chapter->post_status == 'draft' || $dead_chapter == 1?0:1;
			$origin_url = get_post_meta( $chapter->ID, 'origin_url', true );
			$title      = mh_get_improved_chapter_name( $chapter ); ?>
			<tr>
			<td><a href="<?php echo get_edit_post_link( $chapter ); ?>" title="<?php esc_attr( $title ); ?>">
					<span class="dashicons dashicons-media-text"></span>
					<?php echo $title ?>
				</a>
			</td>
			<td><?php echo base64_decode( $origin_url ); ?></td>
			<td>
				<span class="fetch_result"></span>
				<button data-completed_chapter="<?php echo $is_fetched; ?>" data-post_type="chapter" data-post_id="<?php echo $chapter->ID; ?>"
				        data-origin_url="<?php echo $origin_url; ?>"
				        type="button" class="button fetch_images_for_chapter" value="Fetch">
					<span style="margin-left:0!important; float:left; visibility: visible; display:none;"
					      class="spinner"></span>Fetch
				</button>
			</td>
			<td>
				<?php if( $is_fetched ) {
					echo '<span data-post_id="'.$chapter->ID.'" class="wpseo-score-icon good"></span>';
				} else {
					echo '<span data-post_id="'.$chapter->ID.'" class="wpseo-score-icon bad"></span>';
				}?>
			</td>
			</tr><?php
		} ?>
		</table>
	<?php
	}

	/**
	 * @param $remote_chapters
	 * @param $parent_id
	 */
	function render_remote_chapters_table( $remote_chapters, $parent_id ) { ?>
		<table class="widefat striped">
		<caption><b class="alignleft">Chapters Found: <?php echo $remote_chapters->total_count; ?></b><br>
			<button id="process_all_chapters" data-action="insert"  style="float: left; margin: 10px 0;" class="button button-primary">Run
				Batch
			</button>
		</caption>

		<thead>
		<tr>
			<th>Chapter's name</th>
			<th>Origin Url</th>
			<th>Action</th>
			<th>Status</th>
		</tr>
		</thead><?php
		foreach ( array_reverse( $remote_chapters->chapters ) as $remote_chapter ) {
			$origin_url = $remote_chapter->origin_url;
			$title      = $remote_chapter->title; ?>
			<tr>
			<td><?php echo $title; ?></td>
			<td><?php echo base64_decode( $origin_url ); ?></td>
			<td>
				<span class="insert_result"></span>
				<button  data-completed_chapter="0" data-post_type="chapter" data-post_parent="<?php echo $parent_id; ?>"
				        data-title="<?php echo $title; ?>" data-origin_url="<?php echo $origin_url; ?>" type="button"
				        class="button insert_post" value="Insert">
					<span style="margin-left:0!important; float:left; visibility: visible; display:none;"
					      class="spinner"></span>Insert
				</button>
			</td>
			<td><span class="wpseo-score-icon bad"></span></td>
			</tr><?php
		} ?>
		</table><?php
	}

}
<?php

/**
 * Created by PhpStorm.
 * User: hortt
 * Date: 19.08.16
 * Time: 12:33
 */
class Createmanga_Crons {

	/**
	 * Createmanga_Crons constructor.
	 */
	function __construct() {

		$options = Createmanga_Settings::$options;

		$this->add_actions();
		$this->add_filters();

		if ( (bool) $options['enable_cron_jobs']['add_chapters_for_ongoings'] == true &&
		     ! wp_next_scheduled( 'add_chapters_for_ongoings' )
		) {
			wp_schedule_event( time(), '10min', 'add_chapters_for_ongoings' );
		}

		if ( (bool) $options['enable_cron_jobs']['create_download_images_order'] == true &&
		     ! wp_next_scheduled( 'create_download_images_order' )
		) {
			wp_schedule_event( time(), '30sec', 'create_download_images_order' );
		}
		if ( (bool) $options['enable_cron_jobs']['add_manga_data'] == true &&
		     ! wp_next_scheduled( 'add_manga_data' )
		) {
			wp_schedule_event( time(), '1min', 'add_manga_data' );
		}

		if ( (bool) $options['enable_cron_jobs']['download_images'] == true ) {
			if ( ! wp_next_scheduled( 'download_images' ) ) {
				wp_schedule_event( time(), '10sec', 'download_images' );
			}
			if ( ! wp_next_scheduled( 'download_images_aliquot2' ) ) {
				wp_schedule_event( time(), '10sec', 'download_images_aliquot2' );
			}
			if ( ! wp_next_scheduled( 'download_images_aliquot3' ) ) {
				wp_schedule_event( time(), '10sec', 'download_images_aliquot3' );
			}
			if ( ! wp_next_scheduled( 'download_images_aliquot5' ) ) {
				wp_schedule_event( time(), '10sec', 'download_images_aliquot5' );
			}
		}

		if ( (bool) $options['enable_cron_jobs']['create_N_mangas'] == true &&
		     ! wp_next_scheduled( 'create_N_mangas' )
		) {
			wp_schedule_event( time(), '8hour', 'create_N_mangas' );
		}
	}


	/**
	 *
	 */
	function add_actions() {
		add_action( 'create_download_images_order', array( $this, 'create_download_images_order' ) );
		add_action( 'download_images', array( $this, 'download_images' ) );
		add_action( 'download_images_aliquot2', array( $this, 'download_images_aliquot2' ) );
		add_action( 'download_images_aliquot3', array( $this, 'download_images_aliquot3' ) );
		add_action( 'download_images_aliquot5', array( $this, 'download_images_aliquot5' ) );
		add_action( 'add_manga_data', array( $this, 'add_manga_data' ) );
		add_action( 'add_chapters_for_ongoings', array( $this, 'add_chapters_for_ongoings' ) );
		add_action( 'create_N_mangas', array( $this, 'create_N_mangas' ) );
	}


	/**
	 *
	 */
	function add_filters() {
		add_filter( 'cron_schedules', array( $this, 'mh_add_cron_schedule' ) );
	}


	/**
	 * @param $schedules
	 *
	 * @return mixed
	 */
	function mh_add_cron_schedule( $schedules ) {
		$schedules['weekly'] = array(
			'interval' => 604800, // 1 week in seconds
			'display'  => __( 'Once Weekly' ),
		);
		$schedules['1min']   = array(
			'interval' => 60, // 1 min in seconds
			'display'  => __( 'Each 1 minute' ),
		);
		$schedules['30sec']  = array(
			'interval' => 30, // 30 seconds
			'display'  => __( 'Each 30 seconds' ),
		);
		$schedules['10sec']  = array(
			'interval' => 10, // 30 seconds
			'display'  => __( 'Each 10 seconds' ),
		);
		$schedules['10min']  = array(
			'interval' => 600, // 10 min in seconds
			'display'  => __( 'Each 10 minutes' ),
		);
		$schedules['8hour']  = array(
			'interval' => 28800, // 10 min in seconds
			'display'  => __( 'Each 8 hours' ),
		);
		$schedules['5min']   = array(
			'interval' => 300, // 5 min in seconds
			'display'  => __( 'Each 5 minutes' ),
		);

		return $schedules;
	}


	/**
	 *
	 */
	function create_download_images_order() {
		$args = [
			'post_type'      => 'chapter',
			'posts_per_page' => 1,
			'post_status'    => 'draft',
			'orderby'        => 'ID',
			'order'          => 'ASC',
			'meta_query'     => array(
				'relation' => 'AND',
				[
					'key'     => 'origin_url',
					'compare' => 'EXISTS'
				],
				[
					'relation' => 'OR',
					[
						'key'     => 'dead_chapter',
						'value'   => 1,
						'compare' => '!='
					],
					[
						'key'     => 'dead_chapter',
						'compare' => 'NOT EXISTS'
					]
				],
				[
					'key'     => 'total_images',
					'compare' => 'NOT EXISTS'
				]
			)
		];

		$query = new WP_Query( $args );
		if ( $query->posts ) {
			$post = $query->post;
			Createmanga_Utils::create_images_order( $post->ID, $post->post_parent );
		}
	}


	/**
	 *
	 */
	function get_images_to_download( $aliquot = 1 ) {
		global $wpdb;
		if ( $aliquot == 1 ) {
			$prepare = $wpdb->prepare( 'SELECT * FROM ' . $wpdb->prefix . 'images_to_download
			 WHERE ( status = %s OR status = %s ) AND MOD(`chapter_id`, 2) != 0 AND MOD(`chapter_id`, 5) != 0
			    AND  MOD(`chapter_id`, 3) != 0 AND attach_id = 0 ORDER BY chapter_id, id ASC LIMIT 0, 5',
				'need_download', 'fail' );
		} else {
			$prepare = $wpdb->prepare( 'SELECT * FROM ' . $wpdb->prefix . 'images_to_download
			 WHERE ( status = %s OR status = %s ) AND MOD(`chapter_id`, %d) = 0 AND attach_id = 0 ORDER BY chapter_id, id ASC LIMIT 0, 5',
				'need_download', 'fail', $aliquot );
		}
		$images_data = $wpdb->get_results( $prepare, ARRAY_A );

		return $images_data;
	}

	/**
	 * @param $images_data
	 */
	function store_images_with_cron( $images_data ) {
		foreach ( $images_data as $key => $value ) {
			$chapter_id     = $value['chapter_id'];
			$chapter        = get_post( $chapter_id );
			$img_url        = Createmanga_Utils::fix_image_url_maybe( $value['source_url'] );
			$image_title    = $value['chapter_title'] . ' - image: ' . $value['num_in_chapter'];
			$save_file_name = Createmanga_Utils::make_save_file_name( $img_url, $value['num_in_chapter'] );
			$attachment_id  = Createmanga_Utils::create_image_from_remote_host( $img_url, $chapter_id, $image_title,
				$save_file_name );
			if ( $attachment_id ) {
				global $wpdb;
				$wpdb->update( $wpdb->prefix . 'images_to_download', array(
					'status'          => 'downloaded',
					'attach_id'       => $attachment_id,
					'downloaded_date' => current_time( 'mysql', 1 )
				), array(
					'id' => $value['id'],
				) );

				$chould_be_images     = get_post_meta( $chapter_id, 'total_images', true );
				$current_images_count = Createmanga_Utils::get_children_count( $chapter_id );
				if ( $current_images_count == $chould_be_images ) {
					Createmanga_Utils::change_status_trigger( $chapter_id, $value['chapter_title'],
						$chapter->post_parent );
				}
			}
		}
	}


	/**
	 *
	 */
	function download_images() {
		$images_data = $this->get_images_to_download();
		if ( $images_data ):
			$this->store_images_with_cron( $images_data );
		endif;
	}

	/**
	 *
	 */
	function download_images_aliquot2() {
		$images_data = $this->get_images_to_download( 2 );
		if ( $images_data ):
			$this->store_images_with_cron( $images_data );
		endif;
	}

	/**
	 *
	 */
	function download_images_aliquot3() {
		$images_data = $this->get_images_to_download( 3 );
		if ( $images_data ):
			$this->store_images_with_cron( $images_data );
		endif;
	}

	/**
	 *
	 */
	function download_images_aliquot5() {
		$images_data = $this->get_images_to_download( 5 );
		if ( $images_data ):
			$this->store_images_with_cron( $images_data );
		endif;
	}

	/**
	 *
	 */
	function add_manga_data() {

		$args = [
			'post_type'      => 'manga',
			'posts_per_page' => 1,
			'meta_query'     => array(
				array(
					'key'     => 'done_parsing',
					'compare' => 'NOT EXISTS' // this should work...
				),
			)
		];

		$query = new WP_Query( $args );
		if ( $query->posts ) {
			$post = $query->post;
			Createmanga_Utils::add_mangafox_data_for_manga( sanitize_title( $post->post_title ), $post->ID );
			Createmanga_Utils::add_myal_data_for_manga( $post->ID );
			add_post_meta( $post->ID, 'done_parsing', true ) OR update_post_meta( $post->ID, 'done_parsing', true );
		}

	}

	/**
	 * @return array|bool|mixed|object
	 */
	function create_N_mangas() {
		$data = wp_remote_get( Createmanga_Settings::$startparsing_route .
		                       Createmanga_Settings::$options['mangas_per_day'] );
		if ( ! $data instanceof WP_Error && isset( $data['response'] ) && $data['response']['code'] == 200 ) {
			return json_decode( $data['body'] );
		} else {
			return false;
		}
	}


	/**
	 *
	 */
	function add_chapters_for_ongoings() {
		$today = date( "d/m/Y" );
		$args  = [
			'post_type'      => 'manga',
			// @TODO: add option for post per page maybe
			'posts_per_page' => 20,
			'meta_query'     => array(
				'relation' => 'AND',
				[
					'relation' => 'OR',
					[
						'key'     => 'checked_on',
						'value'   => $today,
						'compare' => '!='
					],
					[
						'key'     => 'checked_on',
						'compare' => 'NOT EXISTS'
					]
				],
				[
					'relation' => 'OR',
					[
						'key'     => 'status',
						'value'   => 'ongoing',
						'compare' => '=' // this should work...
					],
					[
						'key'     => 'initial',
						'value'   => 1,
						'compare' => '=' // this should work...
					],
				]
			)
		];

		$query = new WP_Query( $args );
		if ( $query->posts ) {
			foreach($query->posts as $post ) {
				$success = Createmanga_Utils::add_chapters( $post->ID );
				update_post_meta( $post->ID, 'checked_on', $today );
				if ( $success ) {
					delete_post_meta( $post->ID, 'initial' );
				} else {
					add_post_meta( $post->ID, 'dead_chapter', true );
				}
			}
		}
	}
}




























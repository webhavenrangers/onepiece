<?php

/**
 * Created by PhpStorm.
 * User: hortt
 * Date: 17.08.16
 * Time: 19:56
 */
class Createmanga_Utils {

	/**
	 *
	 */
	public static function init() {
		new Createmanga_Utils();
	}


	/**
	 * Createmanga_Utils constructor.
	 */
	function __construct() {
		$this->ajax_hooks();
		$this->add_actions();
	}


	/**
	 * Register ajax hooks
	 */
	function ajax_hooks() {
		add_action( 'wp_ajax_fetch_images_for_chapter', array( 'Createmanga_Utils', 'publish_chapter' ) );
		add_action( 'wp_ajax_nopriv_fetch_images_for_chapter', array( 'Createmanga_Utils', 'publish_chapter' ) );
		add_action( 'wp_ajax_insert_post', array( 'Createmanga_Utils', 'insert_post' ) );
		add_action( 'wp_ajax_nopriv_insert_post', array( 'Createmanga_Utils', 'insert_post' ) );
		add_action( 'wp_ajax_manga_data_remote_fetch', array( 'Createmanga_Utils', 'manga_data_remote_fetch' ) );
		add_action( 'wp_ajax_nopriv_manga_data_remote_fetch', array( 'Createmanga_Utils', 'manga_data_remote_fetch' ) );
		add_action( 'wp_ajax_search_ajax_short', array( 'Createmanga_Utils', 'search_ajax_short' ) );
		add_action( 'wp_ajax_nopriv_search_ajax_short', array( 'Createmanga_Utils', 'search_ajax_short' ) );
		add_action( 'wp_ajax_crunch_images', array( 'Createmanga_Utils', 'crunch_images' ) );
		add_action( 'wp_ajax_nopriv_crunch_images', array( 'Createmanga_Utils', 'crunch_images' ) );
	}

	/**
	 * This is needed for metabox
	 */
	static function search_ajax_short() {
		$keyword = $_POST['keyword'];
		$args    = [
			'posts_per_page' => 1,
			'post_status'    => 'any',
			's'              => $keyword,
			'post_type'      => 'manga'
		];

		$query = new WP_Query( $args );
		if ( $query->have_posts() ) {
			$post       = $query->post;
			$found_data =
				'<input type="checkbox" class="menu-item-checkbox" name="post_parent"value="' . $post->ID . '">' .
				$post->post_title;
			$edit_link  = '<br><a target="_blank" href="' . get_permalink( $post->ID ) . '">Edit</a>';
		} else {
			$found_data =
				'<input type="checkbox" class="menu-item-checkbox" name="post_parent" value="0">Nothing Found';
			$edit_link  = '';
		}
		$output =
			'<ul id="page-search-checklist" data-wp-lists="list:page" class="categorychecklist form-no-clear"><li><label class="menu-item-title">' .
			$found_data . '</label>' . $edit_link . '</li></ul>';
		echo json_encode( $output );
		die;
	}

	static function crunch_images() {
		$data = new stdClass;
		$data->images = $_POST['images_srcs'];
		$post_id = $_POST['post_id'];
		$post_parent_id = wp_get_post_parent_id( $post_id );
		$counter = 0;
		$doing_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;
		foreach ( $data->images as $key => $img_url ) {
			$image_title    = get_the_title( $post_id ) . ' - image: ' . $key;
			$save_file_name = Createmanga_Utils::make_save_file_name( $img_url, $key );
			$attachment_id  = Createmanga_Utils::create_image_from_remote_host( $img_url, $post_id, $image_title,
				$save_file_name );
			if ( $attachment_id ) {
				$counter = $counter + 1;
			} else {
//				update_post_meta( $post_id, 'dead_chapter', true );
				break;
			}
		}
		if ( $counter == ( count( $data->images ) ) ) {
			Createmanga_Utils::change_status_trigger( $post_id, $data->title, $post_parent_id );
			if ( $doing_ajax ) {
				echo json_encode( [ 'success' => true, 'message' => $counter . " images created" ] );
				die();
			}
		}


		die;
	}


	/**
	 * Registers rest api extra routes for next purposes:
	 * 1) checks if manga exists
	 * 2) make image for post from remote url
	 * 3) fixes manga image and moves it to bibgoy
	 */
	function add_actions() {
		add_action( 'rest_api_init', function () {
			register_rest_route( 'check_draft/v1', '/origin_url/(?P<id>.*+)', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'mh_check_draft_callback' ),
			) );
			register_rest_route( 'make_image/v1', '/urlid/(?P<id>.*+)', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'rest_make_image' ),
			) );
			register_rest_route( 'fix_manga_image/v1', '/image_manga/(?P<id>.*+)', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'rest_fix_manga_image' ),
			) );
		} );
	}


	/* Utils section */
	/**
	 * called from ajax only
	 * @param $post_id
	 */
	static function publish_chapter( $post_id = false, $replace = false, $origin_url = false ) {
		update_post_meta( $post_id, 'dead_chapter', true );
		$doing_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;
		$replace    = $doing_ajax ? $_POST['replace'] : $replace;
		$post_id    = $doing_ajax && ! $post_id ? $_POST['post_id'] : $post_id;
		if ( (bool) $replace === true ) {
			Createmanga_Utils::remove_images_from_chapter( $post_id );
		}
		$post_parent_id = wp_get_post_parent_id( $post_id );
		$encoded_url    = $origin_url ? $origin_url : get_post_meta( $post_id, 'origin_url', true );
		$encoded_url    = Createmanga_Utils::fix_origin_url_maybe($encoded_url);
		$data           = Createmanga_Utils::rest_get_chapter_data( $encoded_url );

		$counter = 0;
		if ( $data ) {

			foreach ( $data->images as $key => $img_url ) {
				$image_title    = get_the_title( $post_id ) . ' - image: ' . $key;
				$save_file_name = Createmanga_Utils::make_save_file_name( $img_url, $key );
				$attachment_id  = Createmanga_Utils::create_image_from_remote_host( $img_url, $post_id, $image_title,
					$save_file_name );
				if ( $attachment_id ) {
					$counter = $counter + 1;
				} else {
//					update_post_meta( $post_id, 'dead_chapter', true );
					break;
				}
			}
			if ( $counter == ( count( $data->images ) ) ) {
				Createmanga_Utils::change_status_trigger( $post_id, $data->title, $post_parent_id );
				if ( $doing_ajax ) {
					echo json_encode( [ 'success' => true, 'message' => $counter . " images created" ] );
					die();
				}
			}
		} elseif ( ! $data || $counter != count( $data->images ) ) {
			if ( $doing_ajax ) {
				echo json_encode( [
					'success' => false,
					'message' => $counter . " images created. Should've been " . count( $data->images ) . ' created'
				] );
				die();
			}
		}

	}

	/**
	 * @param $post_id
	 * @param $title
	 * @param $post_parent_id
	 */
	static function change_status_trigger( $post_id, $title, $post_parent_id ) {
		delete_post_meta( $post_id, 'dead_chapter' );
		$postData = [
			'ID'          => $post_id,
			'post_status' => 'publish',
			'post_name'   => sanitize_title( $title ),
			'post_title'  => $title
		];
		wp_update_post( $postData );
		Createmanga_Utils::update_manga_data( $post_parent_id );
		Createmanga_Utils::remove_chapter_from_download_query( $post_id );
	}


	/**
	 * fixes image file name
	 * @param $url
	 * @param $img_number
	 *
	 * @return string
	 */
	static function make_save_file_name( $url, $img_number ) {
		$url     = Createmanga_Utils::fix_image_url_maybe( $url );
		$a_parts = explode( '.', $url );
		$ext     = '';
		if ( count( $a_parts ) > 0 ) {
			$ext = $a_parts[ count( $a_parts ) - 1 ];
		}

		$save_file_name = sanitize_file_name( sprintf( '%03d', $img_number ) );
		$save_file_name .= '.' . $ext;

		return $save_file_name;
	}

	/**
	 * Called from cron
	 * @param $chapter_id
	 * @param $manga_id
	 * @param string $origin_url
	 */
	static function create_images_order( $chapter_id, $manga_id, $origin_url = '' ) {
		$encoded_url = $origin_url ? $origin_url : get_post_meta( $chapter_id, 'origin_url', true );
		$data        = Createmanga_Utils::rest_get_chapter_data( $encoded_url );
		if ( $data ) {
			update_post_meta( $chapter_id, 'total_images', count( $data->images ) );
			global $wpdb;
			foreach ( $data->images as $k => $image_url ) {
				$wpdb->insert( $wpdb->prefix . 'images_to_download', array(
					'status'         => 'need_download',
					'chapter_id'     => $chapter_id,
					'source_url'     => $image_url,
					'num_in_chapter' => $k + 1,
					'manga_id'       => $manga_id,
					'insert_date'    => current_time( 'mysql', 1 ),
					'chapter_title'  => $data->title
				), array(
					'%s',
					'%d',
					'%s',
					'%d',
					'%d',
					'%s',
					'%s'
				) );
			}
		} else {
			update_post_meta( $chapter_id, 'dead_chapter', true );
		}
	}

	/**
	 * @param $post_id
	 *
	 * @return null|string
	 */
	static function get_children_count( $post_id ) {
		global $wpdb;
		$count =
			$wpdb->get_var( $wpdb->prepare( 'SELECT count(ID) FROM `wp_posts` WHERE `post_parent` =  %d', $post_id ) );

		return $count;
	}

	/**
	 * Called from ajax only
	 * @param $title
	 * @param bool $origin_url
	 * @param bool $post_type
	 */
	static function insert_post( $title, $origin_url = false, $post_type = false, $post_parent = false ) {
		$doing_ajax  = defined( 'DOING_AJAX' ) && DOING_AJAX;
		$post_type   = $doing_ajax ? $_POST['post_type'] : $post_type;
		$title       = $doing_ajax ? $_POST['title'] : $title;
		$origin_url  = $doing_ajax ? $_POST['origin_url'] : $origin_url;
		$post_parent = $doing_ajax ? $_POST['post_parent'] : $post_parent;
		$postData    = [
			'post_title'  => $title,
			'post_type'   => $post_type,
			'post_status' => 'draft',
			'post_parent' => $post_parent
		];
		$post_id     = wp_insert_post( $postData );
		if ( (bool) $origin_url === true ) {
			add_post_meta( $post_id, 'origin_url', $origin_url );
		}

		if ( ! is_wp_error( $post_id ) && $post_type == 'chapter' ) {
			Createmanga_Utils::fetch_images_for_chapter( $post_id, false, $origin_url );
		}

	}

	/**
	 * gets remote data. ajax called only.
	 */
	static function manga_data_remote_fetch() {
		$post_id = $_POST['post_id'];
		$source  = $_POST['source'];
		$post    = get_post( $post_id );
		$title   = $source == 'mangafox' ? sanitize_title( $post->post_title ) : $post->post_title;
		$data    = Createmanga_Utils::get_manga_data_from_remote_source( $post_id, $source, $title );
		echo json_encode( $data );
		die;
	}

	/**
	 * @param $post_id
	 * @param $source
	 * @param $title
	 * @TODO: improve myal data responce, make it universal
	 * @return array|bool|mixed|null|object|SimpleXMLElement|SimpleXMLElement[]
	 */
	static function get_manga_data_from_remote_source( $post_id, $source, $title ) {
		$data = [ ];
		switch ( $source ) {
			case 'myal':
				$data = Createmanga_Utils::grab_mangadata_from_myal( $title );
				break;
			case 'mangafox':
				$data = Createmanga_Utils::grab_mangadata_from_mangafox( $title );
				break;
			case 'kissmanga':
				$encoded_url = get_post_meta( $post_id, 'origin_url', true );
				$data        = Createmanga_Utils::rest_get_manga_data( $encoded_url );
				break;
		}

		return $data;
	}

	/**
	 * @param $post_id
	 */
	static function remove_images_from_chapter( $post_id ) {
		$args           = array(
			'post_parent' => $post_id,
			'post_type'   => 'attachment',
			'numberposts' => - 1,
			'post_status' => 'any'
		);
		$children_array = get_children( $args );
		foreach ( $children_array as $item ) {
			wp_delete_attachment( $item->ID, true );
		}
	}

	/**
	 * getting year from string helper
	 * @param $my_anime_list_data
	 *
	 * @return null
	 */
	static function get_release_year_from_mal_data( $my_anime_list_data ) {
		$my_anime_list_data = ( (array) $my_anime_list_data );
		if ( isset( $my_anime_list_data['start_date'] ) ) {
			list( $year, $month, $day ) = explode( '-', $my_anime_list_data['start_date'] );

			return $year;
		}

		return null;
	}

	/**
	 * gets rating from MYAL data. will be not needed if MYAL data is universalized
	 * @param $my_anime_list_data
	 *
	 * @return null
	 */
	static function get_rating_from_mal_data( $my_anime_list_data ) {
		$my_anime_list_data = ( (array) $my_anime_list_data );
		if ( isset( $my_anime_list_data['score'] ) ) {
			return $my_anime_list_data['score'];
		} else {
			return null;
		}
	}

	/**
	 * @param $my_anime_list_data
	 *
	 * @return null
	 */
	static function get_altr_nms_from_mal_data( $my_anime_list_data ) {
		$my_anime_list_data = ( (array) $my_anime_list_data );
		if ( isset( $my_anime_list_data['synonyms'] ) ) {
			return $my_anime_list_data['synonyms'];
		} else {
			return null;
		}
	}


	/**
	 * this should update manga after chapter is published
	 *
	 * @param $post_id
	 */
	static function update_manga_data( $post_id ) {
		$current_post = get_post( $post_id );
		$args         = array(
			'post_parent'    => $current_post->ID,
			'post_type'      => 'chapter',
			'post_status'    => 'any',
			'posts_per_page' => - 1,
			'orderby'        => 'ID',
			'order'          => 'DESC',
		);
		$query        = new WP_Query( $args );
		$drafts       = $published = [ ];
		if ( $query->found_posts != 0 ) {
			foreach ( $query->posts as $post ) {
				if ( $post->post_status == 'draft' ) {
					$drafts[] = $post;
				} elseif ( $post->post_status == 'publish' ) {
					$published[] = $post;
				}
			}
		}

		if ( ! empty( $published ) ) {
			$update_date = $published[0]->post_modified;
			update_post_meta( $current_post->ID, 'last_child_id', $published[0]->ID );
			update_post_meta( $current_post->ID, 'updt_dte', $update_date );
		}

		update_post_meta( $current_post->ID, 'cnt_children', count( $published ) );
		if ( $current_post->post_status !== 'publish' && empty( $drafts ) ) {
			$postData = [
				'ID'          => $current_post->ID,
				'post_status' => 'publish',
				'post_name'   => sanitize_title( $current_post->post_title )
			];
			wp_update_post( $postData );
		}
	}

	/**
	 * this should update manga after chapter is published
	 *
	 * @param $post_id
	 */
	static function add_myal_data_for_manga( $post_id ) {
		$post = get_post( $post_id );
		$data = Createmanga_Utils::grab_mangadata_from_myal( $post->post_title );
		if ( is_null( $data ) ) {
			return;
		}
		$release_year         = Createmanga_Utils::get_release_year_from_mal_data( $data );
		$rating               = Createmanga_Utils::get_rating_from_mal_data( $data );
		$synonyms             = Createmanga_Utils::get_altr_nms_from_mal_data( $data );
		$alt_titles           = get_post_meta( $post_id, 'altr_nms', true );
		$current_release_year = get_post_meta( $post_id, 'release_year', true );
		if ( empty( $current_release_year ) && ( ! empty( $release_year ) ) ) {
			update_post_meta( $post_id, 'release_year', $release_year );
		}

		update_post_meta( $post->ID, 'rating', $rating );
		if ( ! is_null( $data ) && isset( $data->image ) ) {
			$save_file_name = Createmanga_Utils::make_save_file_name( $data->image, '' );
			Createmanga_Utils::create_image_from_remote_host( $data->image, $post->ID, $post->post_title,
				$save_file_name );
		}
//		if ( strlen( $post->post_content ) < strlen( $data->synopsis ) ) {
//			wp_update_post( [ 'ID' => $post_id, 'post_content' => $data->synopsis ] );
//		}
		if ( empty( $alt_titles ) && ! empty( $synonyms ) ) {
			update_post_meta( $post_id, 'altr_nms', $synonyms );
		}
	}

	/**
	 * @param $post_name
	 *
	 * @return array
	 */
	static function grab_mangadata_from_mangafox( $post_name ) {
		$mangafox_url = 'http://mangafox.me/manga/' . str_replace( '-', '_', $post_name ) . '/';
		$html         = file_get_contents( $mangafox_url );
		$dom          = new nokogiri( $html );
		$artists      = $dom->get( 'table tr:nth-child(2) td:nth-child(3) a' )->toText();
		$genres       = explode( ',', $dom->get( 'table tr:nth-child(2) td:nth-child(4) a' )->toText( ',' ) );
		$released     = $dom->get( 'table tr:nth-child(2) td:nth-child(1) a' )->toText();
		$authors      = $dom->get( 'table tr:nth-child(2) td:nth-child(2) a' )->toText();
		$imgelement   = $dom->get( '.cover img' )->toArray();
		$other_names  = explode( ';', $dom->get( '#title h3' )->toText() );
		$title        = $imgelement[0]['alt'];
		$summary      = $dom->get( 'p.summary' )->toText();
		$img_src      = $imgelement[0]['src'];
		if ( strpos( $img_src, '?' ) !== false ) {
			$img_src = substr( $img_src, 0, strpos( $img_src, '?' ) );
		}

		return [
			'artists'     => $artists,
			'author'      => $authors,
			'genres'      => $genres,
			'other_names' => array_unique( $other_names ),
			'released'    => $released,
			'summary'     => $summary,
			'thumbnail'   => $img_src,
			'title'       => $title,
		];
	}

	/**
	 * @param $post_name - should be the slug for post
	 */
	static function add_mangafox_data_for_manga( $post_name, $post_id ) {
		$data                 = Createmanga_Utils::grab_mangadata_from_mangafox( $post_name );
		$artists              = $data['artists'];
		$genres               = $data['genres'];
		$released             = $data['released'];
		$img_src              = $data['thumbnail'];
		$current_release_year = get_post_meta( $post_id, 'release_year', true );
		wp_set_object_terms( $post_id, $artists, 'artist' );

		if ( ! empty( $genres ) ) {
			wp_set_object_terms( $post_id, $genres, 'genre', true );
		}
		if ( empty( $current_release_year ) && ( ! empty( $released ) ) ) {
			update_post_meta( $post_id, 'release_year', $released );
		}
		if ( ! empty( $img_src ) ) {
			Createmanga_Utils::create_image_from_remote_host( $img_src, $post_id );
		}
	}

	/**
	 * Triggered from CRONjob, calls api.mh rout
	 * @param $post_name - should be the slug for post
	 */
	static function add_chapters( $post_id ) {
		$encoded_url = get_post_meta( $post_id, 'origin_url', true );
		$encoded_url = Createmanga_Utils::fix_origin_url_maybe( $encoded_url );
		$data        = wp_remote_get( Createmanga_Settings::$addchapters_route . urlencode( $encoded_url ) );
		if ( ! $data instanceof WP_Error && isset( $data['response'] ) && $data['response']['code'] == 200 ) {
			return json_decode( $data['body'] );
		} else {
			return false;
		}

	}

	/**
	 * @param $title
	 *
	 * @return null|SimpleXMLElement|SimpleXMLElement[]
	 * entry looks like:
	 *  <id>86972</id>
	 * <title>title</title>
	 * <english/>
	 * <synonyms/>
	 * <chapters>0</chapters>
	 * <volumes>0</volumes>
	 * <score>7.54</score>
	 * <type>Manga</type>
	 * <status>Publishing</status>
	 * <start_date>2014-12-05</start_date>
	 * <end_date>0000-00-00</end_date>
	 * <synopsis>
	 * synopsys
	 * </synopsis>
	 * <image>
	 * https://myanimelist.cdn-dena.com/images/manga/2/165199.jpg
	 * </image>
	 *
	 *
	 * Might return wrong manga data because of wrong api responce
	 */
	static function grab_mangadata_from_myal( $title ) {
		$auth     = base64_encode( Createmanga_Settings::$options['myal_credentials']['login'] . ':' .
		                           Createmanga_Settings::$options['myal_credentials']['password'] );
		$args     = [
			'headers'    => [
				'Authorization' => "Basic $auth"
			],
			'user-agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4'
		];
		$response = wp_remote_get( 'http://myanimelist.net/api/manga/search.xml?q=' . urlencode( $title ), $args );
		if ( ! $response instanceof WP_Error && isset( $response['response'] ) &&
		     $response['response']['code'] == 200
		) {
			$content = simplexml_load_string( $response['body'] );
			if ( $content->count() == 1 ) {
				return $content->entry;
			} elseif ( $content->count() > 1 ) {
				return $content->entry[0];
			}

		}

		return null;

	}


	/**
	 * Calls api.mangahaven to get manga data from kissmanga
	 * @param $encoded_url
	 *
	 * @return array|bool|mixed|object
	 */
	static function rest_get_manga_data( $encoded_url ) {
		$encoded_url = Createmanga_Utils::fix_origin_url_maybe( $encoded_url );
		$data        = wp_remote_get( Createmanga_Settings::$manga_route . urlencode( $encoded_url ) );
		if ( ! $data instanceof WP_Error && isset( $data['response'] ) && $data['response']['code'] == 200 ) {
			return json_decode( $data['body'] );
		} else {
			return false;
		}

	}


	/**
	 * @param $encoded_url
	 *
	 * @return array|bool|mixed|object
	 */
	static function rest_get_chapter_data( $encoded_url ) {
		$encoded_url = Createmanga_Utils::fix_origin_url_maybe( $encoded_url );
		$data        = wp_remote_get( Createmanga_Settings::$chapter_route . urlencode( $encoded_url ) );

		if ( ! $data instanceof WP_Error && isset( $data['response'] ) && $data['response']['code'] == 200 ) {
			return json_decode( $data['body'] );
		} else {
			return false;
		}
	}

	/**
	 * @param $encoded_url
	 *
	 * @return array|bool|mixed|object
	 */
	static function rest_get_chapters_list( $encoded_url ) {
		$encoded_url = Createmanga_Utils::fix_origin_url_maybe( $encoded_url );
		$data        = wp_remote_get( Createmanga_Settings::$chapterlist_route . urlencode( $encoded_url ) );
		if ( ! $data instanceof WP_Error && isset( $data['response'] ) && $data['response']['code'] == 200 ) {
			return json_decode( $data['body'] );
		} else {
			return false;
		}
	}

	/**
	 * @param $origin_url
	 *
	 * @return string
	 */
	static function fix_origin_url_maybe( $origin_url ) {
		$human_url = base64_decode( $origin_url );
		if ( strpos( $human_url, 'kissmanga.com' ) === false ) {
			$human_url = 'http://kissmanga.com' . $human_url;
		}

		return base64_encode( $human_url );
	}

	/**
	 * @param $post
	 *
	 * @return array
	 */
	static function get_create_uploads_dir( $post ) {
		$upload_dir = wp_upload_dir();
		if ( $post->post_type == 'chapter' ) {

			$post_chapter  = $post;
			$post_manga_id = $post_chapter->post_parent;
//			$instead_post_id = $post_manga_id !== 0 ? implode( '/', str_split( $post_manga_id, 3 ) ) : '0';
			$instead_post_id   = implode( '/', str_split( $post_manga_id, 3 ) );
			$dir_after_uploads = 'chp/' . $instead_post_id . '/ch_' . $post_chapter->ID . '_' .
			                     sanitize_file_name( $post_chapter->post_title );
			$full_dir_path     = $upload_dir['path'] . '/mhimages/' . $dir_after_uploads;

			if ( ! file_exists( $full_dir_path ) ) {
				mkdir( $full_dir_path, 0755, true );
			}

			$upload_dir['path']    = $full_dir_path;
			$upload_dir['basedir'] = $full_dir_path;
			$upload_dir['url']     = $upload_dir['url'] . '/' . $dir_after_uploads;
			$upload_dir['baseurl'] = $upload_dir['url'];
		} elseif ( $post->post_type == 'manga' ) {

			$instead_post_id = implode( '/', str_split( $post->ID, 3 ) );

			$dir_after_uploads = 'mng/' . $instead_post_id;
			$full_dir_path     = $upload_dir['path'] . '/mhimages/' . $dir_after_uploads;

			if ( ! file_exists( $full_dir_path ) ) {
				mkdir( $full_dir_path, 0755, true );
			}

			$upload_dir['path']    = $full_dir_path;
			$upload_dir['basedir'] = $full_dir_path;
			$upload_dir['url']     = $upload_dir['url'] . '/' . $dir_after_uploads;
			$upload_dir['baseurl'] = $upload_dir['url'];
		}

		return $upload_dir;
	}

	/**
	 * @param $url
	 *
	 * @return string new url
	 */
	static function fix_image_url_maybe( $url ) {
		if ( strpos( $url, 'https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?' ) !== false ) {
			$parts  = explode( '&url=', $url );
			$result = explode( '&', $parts[1] );
			$result = $result[0];
			$result = urldecode( $result );

			return $result;
		} else {
			return $url;
		}
	}


	/**
	 * removes all records from wp_images_to_daownload table with  chapter id == $post_id
	 *
	 * @param $post_id
	 */
	static function remove_chapter_from_download_query( $post_id ) {
		global $wpdb;
		$wpdb->delete( $wpdb->prefix . 'images_to_download', [ 'chapter_id' => $post_id ] );
	}

	/**
	 * @param $image_file_name - image file name
	 * @param $watermark_file_name - stamp file name
	 */
	static function mh_watermark( $image_file_name, $watermark_file_name, $result_image_file_name = '' ) {
		if ( mt_rand( 0, 100 ) <= Createmanga_Settings::$options['watermark_rate'] ) {
			if ( $result_image_file_name == '' ) {
				$result_image_file_name = $image_file_name;
			}

			$source = imagecreatefromstring( $source_str = file_get_contents( $image_file_name ) );
			$stamp  = imagecreatefromstring( $stamp_str = file_get_contents( $watermark_file_name ) );

			$source_info = getimagesizefromstring( $source_str );

			// Set the margins for the stamp and get the height/width of the stamp image
			$marge_right  = 20;
			$marge_bottom = 20;
			$sx           = imagesx( $stamp );
			$sy           = imagesy( $stamp );


			// Copy the stamp image onto our photo using the margin offsets and the photo
			// width to calculate positioning of the stamp.
//    imagecopy($source, $stamp, imagesx($source) - $sx - $marge_right, imagesy($source) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

			imagecolortransparent( $stamp, imagecolorat( $stamp, 1, 1 ) );

			imagecopymerge( $source, $stamp, imagesx( $source ) - $sx - $marge_right,
				imagesy( $source ) - $sy - $marge_bottom, 0, 0, imagesx( $stamp ), imagesy( $stamp ), 70 );

			// Save the image
			switch ( $source_info['mime'] ) {
				case 'image/png':
					imagepng( $source, $result_image_file_name );
					break;

				case 'image/jpeg':
					imagejpeg( $source, $result_image_file_name );
					break;

				default:
					imagejpeg( $source, $result_image_file_name );

			}
			imagedestroy( $source );
		}
	}

	/**
	 * @param $file_path
	 * @param $post_id
	 * @param string $post_title
	 * @param string $img_file_name
	 *
	 * @return int
	 */
	static function create_image_from_remote_host( $file_path, $post_id, $post_title = '', $img_file_name = '' ) {
		$file_path = Createmanga_Utils::fix_image_url_maybe( $file_path );
		$image     = sanitize_text_field( $file_path );
		//Get the remote image and save to uploads directory
		if ( $img_file_name == '' ) {
			$img_file_name = $post_id . "_" . basename( $image );
		}
		$current_post = get_post( $post_id );
		$upload_dir   = Createmanga_Utils::get_create_uploads_dir( $current_post );


		$img = wp_remote_get( $image );


		if ( ! is_wp_error( $img ) && is_array( $img ) &&
		     strpos( $img['headers']['content-type'], 'image' ) !== false
		) {
			$img = $img['body']; // use the content
		} else {
			return false;
		}
		if ( Createmanga_Utils::compare_image_with_placeholder( $img, $current_post ) ) {
			return false;
		}
		if ( ! file_exists( $upload_dir['path'] . '/' . $img_file_name ) ) {
			$fp = fopen( $upload_dir['path'] . '/' . $img_file_name, 'w' );
			fwrite( $fp, $img );
			fclose( $fp );
		}

		$wp_filetype = wp_check_filetype( $image, null );

		if ( $post_title == '' ) {
			$post_title = preg_replace( '/\.[^.]+$/', '', $img_file_name );
		}

		if ( $wp_filetype['type'] == '' ) {
			$wp_filetype['type'] = 'image/jpeg';
		}

		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title'     => $post_title,
			'post_content'   => '',
			'post_status'    => 'inherit'
		);

		if ( $current_post->post_type == 'chapter' ) {
			Createmanga_Utils::mh_watermark( $upload_dir['path'] . '/' . $img_file_name,
				dirname( dirname( __FILE__ ) ) . '/img/op-logo.png', $upload_dir['path'] . '/' . $img_file_name );
		}

		//require for wp_generate_attachment_metadata which generates image related meta-data also creates thumbs
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		$attach_id = wp_insert_attachment( $attachment, $upload_dir['path'] . '/' . $img_file_name, $post_id );
		//Generate post thumbnail of different sizes.
		$attach_data = wp_generate_attachment_metadata( $attach_id, $upload_dir['path'] . '/' . $img_file_name );
		wp_update_attachment_metadata( $attach_id, $attach_data );

		if ( $current_post->post_type === 'manga' ) {
			set_post_thumbnail( $current_post->ID, $attach_id );
		}

		return $attach_id;
	}

	/**
	 * @param $img
	 *
	 * @return bool
	 */
	static function compare_image_with_placeholder( $img, $post = '' ) {
		$triangle_placeholder_md5 = '4cfee0557ac616986ee3cc0c5b6dde17';
		$clircle_placeholder_md5  = 'd3c7a4cf13c71cc43aa6b65f390b658c';
		$current_md5              = md5( $img );
		if ( $triangle_placeholder_md5 == $current_md5 || $clircle_placeholder_md5 == $current_md5 ) {
			if ( ! empty( $post ) && defined( 'DOING_CRON' ) ) {
				Createmanga_Utils::remove_chapter_from_download_query( $post->ID );
				delete_post_meta( $post->ID, 'total_images' );
				update_post_meta( $post->ID, 'dead_chapter', 1 );
//				Createmanga_Utils::create_images_order( $post->ID, $post->post_parent );
			}

			return true;
		}

		return false;
	}


	/**
	 * @param $data
	 *
	 * @return bool|null|string
	 */
	function mh_check_draft_callback( $data ) {
		global $wpdb;
		$url    = urldecode( $data['id'] );
		$result =
			$wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE  `meta_key` = 'origin_url' AND `meta_value` = %s",
				$url ) );

		if ( empty( $result ) ) {
			return false;
		} else {
			return $result;
		}
	}


	/**
	 * @param $data - encoded string like remote_image_url||parent_post_id
	 * Currently is not userd
	 * @return int|string
	 */
	function rest_make_image( $data ) {
		$encoded_url = $data['id'];
		$urlandid    = base64_decode( $encoded_url );
		$array       = explode( '||', $urlandid );
		$url         = $array[0];
		$id          = $array[1];
		if ( count( $array ) < 2 ) {
			return 'Cheating, ugh?';
		}

		return $this->create_image_from_remote_host( $url, $id );
	}

	/**
	 * @param $data
	 *
	 * @return int|string
	 */
	function rest_fix_manga_image( $data ) {
		$image_manga_strings = $data['id'];
		$explode             = explode( '&', $image_manga_strings );
		$attachment_id       = $explode[0];
		$manga_id            = $explode[1];
		$attachment_data     = wp_get_attachment_image_src( $attachment_id, 'large' );
		$image_src           = $attachment_data[0];
		$new_image_id        = $this->create_image_from_remote_host( $image_src, $manga_id );
		if ( $new_image_id ) {
			wp_delete_attachment( $attachment_id, true );

			return [ 'status' => 'success. new img_id: ' . $new_image_id ];
		} else {
			return [ 'status' => 'fail' ];
		}

	}


	/**
	 * @param $post_id
	 *
	 * @return int - count of views this week
	 */
	static function get_post_views_this_week( $post_id ) {
		$meta_name       = "vtw_" . date( 'YW' );
		$views_this_week = get_post_meta( $post_id, $meta_name, true );
		if ( empty( $views_this_week ) ) {
			$views_this_week = 0;
		}

		return $views_this_week;
	}


}
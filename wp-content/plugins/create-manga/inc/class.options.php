<?php

/**
 * Created by PhpStorm.
 * User: hortt
 * Date: 18.08.16
 * Time: 17:44
 */
class Createmanga_Settings {

	/**
	 * @var null
	 */
	private static $_instance = null;

	/**
	 * @var array
	 */
	static $options = array();
	/**
	 * @var string
	 */
	static $options_name = "create-manga";
	/**
	 * @var array
	 */
	static $defaults = array(
		'enable_cron_jobs' => [
			'enabled'                      => 0,
			'add_chapters_for_ongoings'    => 0,
			'download_images'              => 0,
			'add_manga_data'               => 0,
			'create_download_images_order' => 0,
			'create_N_mangas'              => 0
		],
		'watermark_rate'   => 10,
		'request_timeout'  => 10,
		'mangas_per_day'   => 50,
		'myal_credentials' => [
			'login'    => 'elance',
			'password' => 'tester777'
		]
	);

	/**
	 * @var string
	 */
	static $manga_route = 'http://api.mangahaven.org/rest/manga/';
	/**
	 * @var string
	 */
	static $chapter_route = 'http://api.mangahaven.org/rest/chapter/';
	/**
	 * @var string
	 */
	static $chapterlist_route = 'http://api.mangahaven.org/rest/chapterlist/';

	/**
	 * @var string
	 */
	static $addchapters_route = 'http://api.mangahaven.org/rest/addchapters/';
	/**
	 * @var string
	 */
	static $startparsing_route = 'http://api.mangahaven.org/startparsing/';

	/**
	 * @param $name
	 * @param $options
	 */
	static function store_options( $name, $options ) {
		add_option( $name, $options ) OR update_option( $name, $options );
	}

	/**
	 *
	 */
	function get_options() {
		self::$options = get_option( Createmanga_Settings::$options_name );
	}

	/**
	 *
	 */
	public static function plugin_activation() {
		Createmanga_Settings::store_options( self::$options_name, self::$defaults );
	}

	/**
	 *
	 */
	public static function plugin_deactivation() {
		delete_option( Createmanga_Settings::$options_name );
	}


	/**
	 * Createmanga_Settings constructor.
	 */
	private function __construct() {
		$this->get_options();
// приватный конструктор ограничивает реализацию getInstance ()
	}

	/**
	 *
	 */
	protected function __clone() {
// ограничивает клонирование объекта
	}

	/**
	 * @return Createmanga_Settings|null
	 */
	static public function getInstance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 *
	 */
	public function import() {
// ...
	}

	/**
	 *
	 */
	public function get() {
// ...
	}
}
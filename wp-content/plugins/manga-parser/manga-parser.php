<?php
/*
Plugin Name: Manga Parser
Description: Plugin for parsing mangas and comics
*/
if( !defined('WP_CRON_LOCK_TIMEOUT'))
	define('WP_CRON_LOCK_TIMEOUT ', 60);
require_once( 'inc/class-kissmanga.php' );
require_once( 'inc/class-readcomiconline.php' );
require_once( 'inc/class-downloader.php' );
require_once( 'inc/easy_html_dom_parser.php' );

//define( 'MH_THREADS_FOR_ISSUES_IMAGES', 3 );
define( 'MH_THREADS_FOR_CHAPTERS_IMAGES', 3 );

//
//// start cron jobs for downloading issues images
//for ( $n = 0; $n < MH_THREADS_FOR_ISSUES_IMAGES; $n ++ ) {
//	$args = array( 'num' => $n );
//	if ( ! wp_next_scheduled( 'mh_cron_multi_issues_' . $n, $args ) ) {
//		wp_schedule_event( time(), '1min', 'mh_cron_multi_issues_' . $n, $args );
//	}
//	add_action( 'mh_cron_multi_issues_' . $n, 'mh_download_issues_images', 10, 2 );
//}
//
//// start cron jobs for downloading chapters images
//for ( $n = 0; $n < MH_THREADS_FOR_CHAPTERS_IMAGES; $n ++ ) {
//	$args = array( 'num' => $n );
//	if ( ! wp_next_scheduled( 'mh_cron_multi_chapters_' . $n, $args ) ) {
//		wp_schedule_event( time(), '1min', 'mh_cron_multi_chapters_' . $n, $args );
//	}
//	add_action( 'mh_cron_multi_chapters_' . $n, 'mh_download_chapters_images', 10, 2 );
//}

// Execute 10 cron jobs (if they are in query)
//for ( $q = 0; $q < 10; $q ++ ) {
//	wp_cron();
//}

/**
 * Adding styles for this plugin (for admin zone)
 */
function mh_load_admin_style() {
	wp_register_style( 'mh_admin_css', plugin_dir_url( __FILE__ ) . 'mh-admin-style.css', false, '1.0.0' );
	wp_enqueue_style( 'mh_admin_css' );
}

add_action( 'admin_enqueue_scripts', 'mh_load_admin_style' );

register_activation_hook( __FILE__, 'mh_manga_parser_activate' );

/**
 * When this plugin is activated, trying to create tables (only if they are not exist)
 */
function mh_manga_parser_activate() {
	global $wpdb;
	$sql = "
CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "mh_downloader_chapters (
  `id` bigint(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `added_in_queue` varchar(10) NOT NULL,
  `updated_in_queue` varchar(10) NOT NULL,
  `id_manga_in_queue` bigint(20) NOT NULL,
  `id_post_type_manga` bigint(20) NOT NULL,
  `id_post_type_chapter` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

	$sql2 = "
CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "mh_downloader_chapter_images (
  `id` bigint(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  `id_chapter_in_queue` bigint(20) NOT NULL,
  `id_post_type_chapter` int(11) NOT NULL,
  `attach_id` bigint(20) NOT NULL,
  `num_in_chapter` int(11) NOT NULL,
  `source_url` text NOT NULL,
  `last_try` varchar(19) NOT NULL,
  `id_post_type_manga` bigint(20) NOT NULL,
  `insert_date` datetime NOT NULL,
  `downloaded_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

	$sql3 = "
CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "mh_downloader_mangas (
  `id` bigint(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_post_type_manga` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `latest_chapter` varchar(255) NOT NULL,
  `latest_chapter_url` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `added_in_queue` varchar(10) NOT NULL,
  `updated_in_queue` varchar(10) NOT NULL,
  `thumb_mal_status` varchar(255) NOT NULL,
  `thumb_mal_last_try` datetime NOT NULL,
  `thumb_km_status` varchar(255) NOT NULL,
  `thumb_km_last_try` datetime NOT NULL,
  `artists_status` varchar(255) NOT NULL,
  `artists_last_try` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";



	$wpdb->query( $sql );
	$wpdb->query( $sql2 );
	$wpdb->query( $sql3 );

}

// Hook for adding admin menus
add_action( 'admin_menu', 'mh_manga_parser_menu' );

// action function for above hook
function mh_manga_parser_menu() {
	// Add a new top-level menu (ill-advised):
	add_menu_page( 'Manga Parser', 'Manga Parser', 'edit_posts', __FILE__, 'mh_downloader_status' );

	// Testing page for parsing mangas.
//	add_submenu_page( __FILE__, 'Delete parsed data', 'Delete parsed data', 8, 'delete_parsed_data', 'mh_tmp_delete_parsed_data' );
	add_submenu_page( __FILE__, 'Test code', 'Test code', 'edit_posts', 'test_code', 'mh_test_code' );
}

function mh_downloader_status() {
	?>
	<h2>Downloader status</h2>
	<h4>Was mangas list downloaded today?</h4>
	<?php
	if ( MH_Downloader::was_downloaded_mangas_list_today() ) {
		echo "Yes";
	} else {
		echo "No";
	}
	?>

	<h4>When was mangas list downloaded?</h4>
	<?php
	$date = MH_Downloader::mangas_list_downloaded_date();
	if ( $date !== false ) {
		echo $date;
	} else {
		echo "Never";
	}
	?>

	<h4>What downloader should to do?</h4>
	<?php
	echo MH_Downloader::what_to_do()['task_name'];
	?>


	<h2>Downloading from kissmanga (Human Emulator on Windows server)</h2>
	<h4>Mangas pages:</h4>
	<p>
		Downloaded:
		<?php echo MH_Downloader::count_downloaded_pages_from_kissmanga(); ?>
		<br/>

		In Queue:
		<?php echo MH_Downloader::count_need_download_pages_from_kissmanga(); ?>
	</p>

	<h4>Chapters pages:</h4>
	<p>
		Downloaded:
		<?php echo MH_Downloader::count_downloaded_chapters_pages_from_kissmanga(); ?>
		<br/>

		In Queue:
		<?php echo MH_Downloader::count_need_download_chapters_pages_from_kissmanga(); ?>
	</p>

	<h2>Downloading from readcomiconline (Human Emulator on Windows server)</h2>
	<h4>Comics pages</h4>
	<p>
		Downloaded:
		<?php echo MH_Downloader::count_comics_in_queue_with_status( 'downloaded' ); ?>
		<br/>
		In Queue:
		<?php echo MH_Downloader::count_comics_in_queue_with_status( 'need_download' ); ?>
	</p>
	<h4>Issues pages</h4>
	<p>
		Downloaded:
		<?php echo MH_Downloader::count_issues_in_queue_with_status( 'downloaded' ); ?>
		<br/>
		In Queue:
		<?php echo MH_Downloader::count_issues_in_queue_with_status( 'need_download' ); ?>
	</p>

	<h2>Downloading by cron</h2>
	<h3>Thumbnails</h3>
	<h4>MyAnimeList</h4>
	<p>
		Downloaded:
		<?php echo MH_Downloader::count_mangas_where_thumbnail_status_is( 'downloaded' ); ?><br/>

		Fail:
		<?php echo MH_Downloader::count_mangas_where_thumbnail_status_is( 'fail' ); ?><br/>

		In Queue:
		<?php echo MH_Downloader::count_mangas_where_thumbnail_in_queue(); ?><br/>

		[without_image] Without images (my anime list):
		<?php echo MH_Downloader::count_mangas_where_thumbnail_status_is( 'without_image' ); ?>
	</p>

	<h4>Kissmanga</h4>
	<p>
		Downloaded:
		<?php echo MH_Downloader::count_mangas_where_thumb_km_status_is( 'downloaded' ); ?><br/>

		Fail:
		<?php echo MH_Downloader::count_mangas_where_thumb_km_status_is( 'fail' ); ?><br/>

		In Queue:
		<?php echo MH_Downloader::count_mangas_where_thumb_km_status_is( '' ); ?><br/>

		[without_image] Without images (my anime list):
		<?php echo MH_Downloader::count_mangas_where_thumb_km_status_is( 'without_image' ); ?>
	</p>


	<h4>Comics thumbnails</h4>
	<p>
		Downloaded:
		<?php echo MH_Downloader::count_comics_where_thumb_rco_status_is( 'downloaded' ); ?>
		<br/>
		In Queue:
		<?php echo MH_Downloader::count_comics_where_thumb_rco_status_is( '' ); ?>
	</p>

	<h3>Images for chapters and issues</h3>

	<h4>Chapters images</h4>
	<p>
		Downloaded:
		<?php echo MH_Downloader::count_chapters_images_in_queue( 'downloaded' ); ?><br/>

		Fail:
		<?php echo MH_Downloader::count_chapters_images_in_queue( 'fail' ); ?><br/>

		In Queue:
		<?php echo MH_Downloader::count_chapters_images_in_queue( 'need_download' ); ?><br/>
	</p>

	<h4>Issues images</h4>
	<p>
		Downloaded:
		<?php echo MH_Downloader::count_issues_images_in_queue( 'downloaded' ); ?><br/>

		Fail:
		<?php echo MH_Downloader::count_issues_images_in_queue( 'fail' ); ?><br/>

		In Queue:
		<?php echo MH_Downloader::count_issues_images_in_queue( 'need_download' ); ?><br/>
	</p>
	<?php
}

/**
 * What downloader should to do
 */
function mh_downloader_ajax_what_to_do() {
	// script will die if ip is not permissible
	MH_Downloader::die_if_ip_is_not_permissible();

	$answer             = [ ];
	$answer['your_ip']  = $_SERVER['REMOTE_ADDR'];
	$answer['datetime'] = date( 'Y-m-d H:i:s' );

	$todo   = MH_Downloader::what_to_do();
	$answer = array_merge( $answer, $todo );

	echo json_encode( $answer );
	die();
}

// What to do
add_action( 'wp_ajax_mh_what_to_do', 'mh_downloader_ajax_what_to_do' );
add_action( 'wp_ajax_nopriv_mh_what_to_do', 'mh_downloader_ajax_what_to_do' );

// Save new mangas list
add_action( 'wp_ajax_mh_save_mangas_list', 'mh_downloader_ajax_save_mangas_list' );
add_action( 'wp_ajax_nopriv_mh_save_mangas_list', 'mh_downloader_ajax_save_mangas_list' );

//mh_save_manga
add_action( 'wp_ajax_mh_save_manga', 'mh_downloader_ajax_save_manga' );
add_action( 'wp_ajax_nopriv_mh_save_manga', 'mh_downloader_ajax_save_manga' );

add_action( 'wp_ajax_mh_save_chapter', 'mh_downloader_ajax_save_chapter' );
add_action( 'wp_ajax_nopriv_mh_save_chapter', 'mh_downloader_ajax_save_chapter' );

///mh_save_manga_image
add_action( 'wp_ajax_mh_save_manga_image_from_kissmanga', 'mh_downloader_ajax_save_manga_image_from_kissmanga' );
add_action( 'wp_ajax_nopriv_mh_save_manga_image_from_kissmanga', 'mh_downloader_ajax_save_manga_image_from_kissmanga' );

// Save new mangas list
add_action( 'wp_ajax_mh_save_comics_list', 'mh_downloader_ajax_save_comics_list' );
add_action( 'wp_ajax_nopriv_mh_save_comics_list', 'mh_downloader_ajax_save_comics_list' );

//mh_save_comic
add_action( 'wp_ajax_mh_save_comic', 'mh_downloader_ajax_save_comic' );
add_action( 'wp_ajax_nopriv_mh_save_comic', 'mh_downloader_ajax_save_comic' );

add_action( 'wp_ajax_mh_save_issue', 'mh_downloader_ajax_save_issue' );
add_action( 'wp_ajax_nopriv_mh_save_issue', 'mh_downloader_ajax_save_issue' );

function mh_downloader_ajax_save_manga_image_from_kissmanga() {
	global $wpdb;
	// script will die if ip is not permissible
	MH_Downloader::die_if_ip_is_not_permissible();

//	mh_pre_print_r( $_POST );

	$manga_id = $_POST['image_data']['image_data']['id_post_type_manga'];
	$id       = $_POST['image_data']['image_data']['id'];
	echo "manga_id: $manga_id";
	echo "<br />";
	echo "id: $id";
	$manga_post = get_post( $manga_id );

	// save file to tmp
	$upload_dir = wp_upload_dir();

	$file_path = $upload_dir['path'] . '/tempfile.jpg';

	file_put_contents( $file_path, stripslashes( $_POST['image_data']['file_data'] ) );

	$ext            = 'jpg';
	$save_file_name = sanitize_file_name( $manga_post->post_title . '-v2' ) . '.' . $ext;

	$v['id'] = $id;

	$attach_id = create_image_from_remote_host( $file_path, $manga_post->ID, $manga_post->post_title, $save_file_name );
	if ( $attach_id ) {
		echo "[downloaded] " . $file_path;
		set_post_thumbnail( $manga_post->ID, $attach_id );
		$wpdb->update(
			$wpdb->prefix . 'mh_downloader_mangas',
			[ 'thumb_km_status' => 'downloaded', 'thumb_km_last_try' => current_time( 'mysql', 1 ) ],
			[ 'id' => $v['id'] ],
			[ '%s', '%s' ]
		);
	} else {
		echo "[fail] " . $file_path;
		$wpdb->update(
			$wpdb->prefix . 'mh_downloader_mangas',
			[ 'thumb_km_status' => 'fail', 'thumb_km_last_try' => current_time( 'mysql', 1 ) ],
			[ 'id' => $v['id'] ],
			[ '%s', '%s' ]
		);
	}
	die();
}

function mh_downloader_ajax_save_chapter() {
	// script will die if ip is not permissible
	MH_Downloader::die_if_ip_is_not_permissible();

	global $wpdb;
	echo date( "Y-m-d H:i:s" ) . ", action:: " . $_POST['action'] . "\n";
	$html = stripslashes( $_POST['chapters_data']['html'] );
	echo "HTML len: " . strlen( $html );

	$images_urls = MH_Kissmanga::get_image_list_from_chapter_page( $html );
	print_r($images_urls);
	echo " Images in chapter: " . count( $images_urls );
	unset( $_POST['chapters_data']['html'] );
	echo "\n" . "id_chapter_in_queue: " . $_POST['chapters_data']['chapter_data']['id'];
	echo "\n" . "id_post_type_chapter: " . $_POST['chapters_data']['chapter_data']['id_post_type_chapter'];

	foreach ( $images_urls as $k => $image_url ) {

		$arr_insert = 			array(
			'status'               => 'need_download',
			'id_chapter_in_queue'  => $_POST['chapters_data']['chapter_data']['id'],
			'id_post_type_chapter' => $_POST['chapters_data']['chapter_data']['id_post_type_chapter'],
			'source_url'           => $image_url,
			'num_in_chapter'       => $k + 1,
			'id_post_type_manga'   => $_POST['chapters_data']['chapter_data']['id_post_type_manga'],
			'insert_date'          => current_time( 'mysql', 1 )
		);

		echo "<br />:arr_insert:<br /> ";
		print_r($arr_insert);

		$result = $wpdb->insert(
			$wpdb->prefix . 'mh_downloader_chapter_images',
$arr_insert,
			array(
				'%s',
				'%d',
				'%d',
				'%s',
				'%d',
				'%d',
				'%s'
			)
		);
		echo "<br />Insert result: ";
		print_r($result);
	}

	$wpdb->update(
		$wpdb->prefix . 'mh_downloader_chapters',
		array(
			'status' => 'downloaded'
		),
		array(
			'id' => $_POST['chapters_data']['chapter_data']['id']
		),
		array(
			'%s'
		)
	);
	die();
}


function mh_downloader_ajax_save_manga() {
	global $wpdb;

	// script will die if ip is not permissible
	MH_Downloader::die_if_ip_is_not_permissible();

	MH_Downloader::debug_with_time();
	//
	echo '<b>' . date( "Y-m-d H:i:s" ) . "</b>, action: " . $_POST['action'] . "\n";
	echo 'HTML len: ' . strlen( $_POST['mangas_data']['html'] ) . "\n";
	$html = stripslashes( $_POST['mangas_data']['html'] );

	echo "_POST['mangas_data']['manga_data']['id']: " . $_POST['mangas_data']['manga_data']['id'];

	$kissmanga  = new MH_Kissmanga();
	$manga_data = $kissmanga->getMangasData( $html );
	echo "\nName: " . $manga_data['name'] . "\n";

	$my_post = array(
		'post_title'        => wp_strip_all_tags( $manga_data['name'] ),
		'post_content'      => wp_strip_all_tags( $manga_data['summary'] ),
		'post_status'       => 'draft',
		'post_type'         => 'manga',
		'comment_status'    => 'open'
	);

	// if this manga is exists
	//	$manga = get_page_by_title( wp_strip_all_tags( $aMangaData['name'] ), ARRAY_A, 'manga' );
	if ( $_POST['mangas_data']['manga_data']['id_post_type_manga'] == 0 ) {
		echo "[- - -]";
		// Insert the manga into the database
		$post_manga_id = wp_insert_post( $my_post );
		echo "[2 2 2]";
	} else {
		echo "[+ + +]";
		$post_manga_id = $_POST['mangas_data']['manga_data']['id_post_type_manga'];
	}
	echo "post_manga_id: " . $post_manga_id;
	echo "\n";

	// Genres
	$genres              = [ ];
	$genres_descriptions = [ ];

	foreach ( $manga_data['genres'] as $k_genre => $v_genre ) {
		// if term is exist, add term id, else create term with description
		$genres[] = $v_genre['name'];
	}
	$a_result = wp_set_object_terms( $post_manga_id, $genres, 'genre' );

	foreach ( $a_result as $k => $term_id ) {
		wp_update_term( $term_id, 'genre', array( 'description' => $manga_data['genres'][ $k ]['description'] ) );
	}

//	foreach ( $a_result as $k => $v ) {
//		if ( ! isset( $taxonomies_flags[ $v ] ) ) {
//			$sql = $wpdb->prepare(
//				'UPDATE wp_term_taxonomy SET description = %s WHERE term_id = %d and taxonomy = %s',
//				$aMangaData['genres'][ $k ]['description'], $v, 'genre'
//			);
//			$wpdb->query( $sql );
//			$taxonomies_flags[ $v ] = true;
//		}
//	}

	// Authors
	$authors = [ ];
	foreach ( $manga_data['authors'] as $k_author => $v_author ) {
		$authors[] = $v_author['name'];
	}
	wp_set_object_terms( $post_manga_id, $authors, 'author' );

	echo "<ul>";
	// I use "for", because we need to add chapters in sort 9 8 7 .. 1
	for ( $k = count( $manga_data['chapters'] ) - 1; $k >= 0; $k -- ) {
		echo "<!-- [[2:$k]] -->";
		$chapter = $manga_data['chapters'][ $k ];

		$ch_data = get_page_by_title( wp_strip_all_tags( $chapter['name'] ), ARRAY_A, 'chapter' );
		if ( ! is_null( $ch_data ) ) {
			echo "<li>[::](" . $ch_data['ID'] . ") " . $chapter['name'] . " [exists]</li>";
			continue;
		}

		// add chapter to database
		$my_post_chapter = array(
			'post_title'        => wp_strip_all_tags( $chapter['name'] ),
			'post_status'       => 'draft',
			'post_type'         => 'chapter',
			'post_parent'       => $post_manga_id,
			'comment_status'    => 'open'
		);

		// Insert the chapter into the database
		$post_chapter_id = wp_insert_post( $my_post_chapter );
		// insert to chapters download queue
		$wpdb->insert(
			$wpdb->prefix . 'mh_downloader_chapters',
			array(
				'status'               => 'need_download',
				'name'                 => wp_strip_all_tags( $chapter['name'] ),
				'url'                  => $chapter['url'],
				'id_manga_in_queue'    => $_POST['mangas_data']['manga_data']['id'],
				'id_post_type_manga'   => $post_manga_id,
				'id_post_type_chapter' => $post_chapter_id,
				'added_in_queue'       => date( "Y-m-d" ),
			),
			array(
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%d',
				'%s',
			)
		);

		// Day when chapter was added on kissmanga
		// Month/Day/Year -> Year-Month-Day
		list( $month, $day, $year ) = explode( "/", $chapter['date'] );
		$day_add = sprintf( "%04d-%02d-%02d", $year, $month, $day );
		update_post_meta( $post_chapter_id, 'day_add', $day_add );

		// set $manga_update_date when last chapter was added
		// and last chapter id
		if ( $k == 0 ) {
			$manga_update_date     = $day_add;
			$manga_last_chapter_id = $post_chapter_id;
		}
		echo "<li>[222]($post_chapter_id) " . $chapter['name'] . " [new]</li>";
	}
	echo "</ul>";
	// Alternate names
	if ( ! empty( $manga_data['alternate_names'] ) ) {
		update_post_meta( $post_manga_id, 'altr_nms', $manga_data['alternate_names'] );
	}

	echo "[cnt_children: " . count( $manga_data['chapters'] ) . "]<br />\n";
	// Count of chapters
	update_post_meta( $post_manga_id, 'cnt_children', count( $manga_data['chapters'] ) );

	// Updated date. This is date when last chapter was updated
	update_post_meta( $post_manga_id, 'updt_dte', $manga_update_date );

	// Last chapter id of current manga
	update_post_meta( $post_manga_id, 'last_child_id', $manga_last_chapter_id );

	$total_views = get_post_meta( $post_manga_id, 'ttl_vws', true );
	if ( empty( $total_views ) ) {
		update_post_meta( $post_manga_id, 'ttl_vws', 0 );
	}


	// set status 'downloaded' in queue
//    $_POST['mangas_data']['manga_data']['id']
	$wpdb->update(
		$wpdb->prefix . 'mh_downloader_mangas',
		array(
			'status'             => 'downloaded',
			'id_post_type_manga' => $post_manga_id,
			'image_url'          => $manga_data['image'],
		),
		array(
			'id' => $_POST['mangas_data']['manga_data']['id']
		)
	);

	MH_Downloader::debug_with_time();

	echo "<hr />";
	die();
}

function mh_downloader_ajax_save_comic() {
	global $wpdb;

	// script will die if ip is not permissible
	MH_Downloader::die_if_ip_is_not_permissible();


	MH_Downloader::debug_with_time();
	//
	echo '<b>' . date( "Y-m-d H:i:s" ) . "</b>, action: " . $_POST['action'] . "\n";
	echo 'HTML len: ' . strlen( $_POST['comics_data']['html'] ) . "\n";
	$html = stripslashes( $_POST['comics_data']['html'] );

	echo "_POST['comics_data']['comic_data']['id']: " . $_POST['comics_data']['comic_data']['id'];

	$readcomiconline = new MH_Readcomiconline();
	$comic_data      = $readcomiconline->get_comic_data( $html );
	echo "\nName: " . $comic_data['name'] . "\n";

	$my_post = array(
		'post_title'        => wp_strip_all_tags( $comic_data['name'] ),
		'post_content'      => wp_strip_all_tags( $comic_data['summary'] ),
		'post_status'       => 'draft',
		'post_type'         => 'comic',
		'comment_status'    => 'open'
	);

	// if this comic is exists
	//	$comic = get_page_by_title( wp_strip_all_tags( $comic_data['name'] ), ARRAY_A, 'comic' );

	if ( $_POST['comics_data']['comic_data']['id_post_type_comic'] == 0 ) {
		echo "[- - -]";
		// Insert the comic into the database
		$post_comic_id = wp_insert_post( $my_post );
	} else {
		echo "[+ + +]";
		$post_comic_id = $_POST['comics_data']['comic_data']['id_post_type_comic'];
	}
	echo "post_comic_id: " . $post_comic_id;
	echo "\n";

	// Genres for comic
	$comic_genres = array();
	foreach ( $comic_data['genres'] as $genre ) {
		$comic_genres[] = $genre['name'];
	}
	wp_set_object_terms( $post_comic_id, $comic_genres, 'genre' );

	// Publisher
	update_post_meta( $post_comic_id, 'publisher', $comic_data['publisher'] );

	// Writers
	$writers = array();
	foreach ( $comic_data['writers'] as $writer ) {
		$writers[] = $writer['name'];
	}
	wp_set_object_terms( $post_comic_id, $writers, 'writer' );

	// Artists
	$artists = array();
	foreach ( $comic_data['artists'] as $artist ) {
		$artists[] = $artist['name'];
	}
	wp_set_object_terms( $post_comic_id, $artists, 'artist' );

	// Publication date
	// start publication date, finish publication date, start publication year
	if ( isset( $comic_data['release_year'] ) ) {
		update_post_meta( $post_comic_id, 'release_year', $comic_data['release_year'] );
	}

	// Extra data
	update_post_meta( $post_comic_id, 'extra_data', $comic_data['extra_data'] );

	// Status
	if ( ! empty( $comic_data['status'] ) ) {
		update_post_meta( $post_comic_id, 'status', $comic_data['status'] );
	}

	echo "<ul>";
	echo "<!-- [[1]] -->";
	// I use "for", because we need to add issues in sort 9 8 7 .. 1
	for ( $k = count( $comic_data['issues'] ) - 1; $k >= 0; $k -- ) {
		echo "<!-- [[2:$k]] -->";
		$issue      = $comic_data['issues'][ $k ];
		$issue_data = get_page_by_title( wp_strip_all_tags( $issue['name'] ), ARRAY_A, 'issue' );
		if ( ! is_null( $issue_data ) ) {
			echo "<li>[::](" . $issue_data['ID'] . ") " . $issue['name'] . " [exists]</li>";
			continue;
		}

		// add issue to database
		$my_post_issue = array(
			'post_title'        => wp_strip_all_tags( $issue['name'] ),
			'post_status'       => 'draft',
			'post_type'         => 'issue',
			'post_parent'       => $post_comic_id,
			'comment_status'    => 'open'
		);

		// Insert the issue into the database
		$post_issue_id = wp_insert_post( $my_post_issue );
		// insert to issues download queue
		$wpdb->insert(
			$wpdb->prefix . 'mh_downloader_issues',
			array(
				'status'             => 'need_download',
				'name'               => wp_strip_all_tags( $issue['name'] ),
				'url'                => $issue['url'],
				'id_comic_in_queue'  => $_POST['comics_data']['comic_data']['id'],
				'id_post_type_comic' => $post_comic_id,
				'id_post_type_issue' => $post_issue_id,
				'added_in_queue'     => date( "Y-m-d" ),
			),
			array(
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%d',
				'%s',
			)
		);

		// Day when issue was added on readcomiconline
		// Month/Day/Year -> Year-Month-Day
		list( $month, $day, $year ) = explode( "/", $issue['date'] );
		$day_add = sprintf( "%04d-%02d-%02d", $year, $month, $day );
		update_post_meta( $post_issue_id, 'day_add', $day_add );

		// set $comic_update_date when last chapter was added
		// and last chapter id
		if ( $k == 0 ) {
			$comic_update_date   = $day_add;
			$comic_last_issue_id = $post_issue_id;
		}

		echo "<li>[222]($post_issue_id) " . $issue['name'] . " [new]</li>";
	}
	echo "</ul>";

	// Alternate names
	if ( ! empty( $comic_data['alternate_names'] ) ) {
		update_post_meta( $post_comic_id, 'altr_nms', $comic_data['alternate_names'] );
	}

	echo "[cnt_children: " . count( $comic_data['issues'] ) . "]<br />\n";

	// Count of issues
	update_post_meta( $post_comic_id, 'cnt_children', count( $comic_data['issues'] ) );

	// Updated date. This is date when last chapter was updated
	update_post_meta( $post_comic_id, 'updt_dte', $comic_update_date );

	// Last issue id of current comic
	update_post_meta( $post_comic_id, 'last_child_id', $comic_last_issue_id );

	$total_views = get_post_meta( $post_comic_id, 'ttl_vws', true );
	if ( empty( $total_views ) ) {
		update_post_meta( $post_comic_id, 'ttl_vws', 0 );
	}

	// set status 'downloaded' in queue
	$wpdb->update(
		$wpdb->prefix . 'mh_downloader_comic',
		array(
			'status'             => 'downloaded',
			'id_post_type_comic' => $post_comic_id,
			'image_url'          => $comic_data['image'],
		),
		array(
			'id' => $_POST['comics_data']['comic_data']['id']
		)
	);
	MH_Downloader::debug_with_time();
	echo "<hr />";
	die();
}


/**
 * Receive mangas list and save it
 */
function mh_downloader_ajax_save_mangas_list() {
	global $wpdb;
	// script will die if ip is not permissible
	MH_Downloader::die_if_ip_is_not_permissible();

	echo '[ok-1]';
	if ( isset( $_POST['mangas_data'] ) ) {
		echo "Mangas data len: " . strlen( $_POST['mangas_data'] );
	}
	$mangas_data = $_POST['mangas_data'];
	echo "\nmangas_data: ";
	print_r( $mangas_data );
	$decode_mangas_data = json_decode( stripslashes( $mangas_data ) );
	echo "\ndecode_mangas_data: ";
	print_r( $decode_mangas_data );

	foreach ( $decode_mangas_data as $k => $v ) {
		$v = ( (array) $v );
		// try to get data about manga in database
		$row = $wpdb->get_row(
			$wpdb->prepare( 'SELECT * FROM ' . $wpdb->prefix . 'mh_downloader_mangas WHERE name = %s', $v['text'] ),
			ARRAY_A );
		if ( is_null( $row ) ) {
			$wpdb->insert(
				$wpdb->prefix . 'mh_downloader_mangas',
				array(
					'name'               => $v['text'],
					'url'                => $v['url'],
					'latest_chapter'     => $v['updated_chapter'],
					'latest_chapter_url' => $v['updated_chapter_url'],
					'status'             => 'need_download',
					'added_in_queue'     => date( "Y-m-d" ),
				),
				array(
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
				)
			);
		} else {
			echo "[in database]";
			if ( $row['latest_chapter'] != $v['updated_chapter'] ) {
				echo "[will update]";
				$wpdb->update(
					$wpdb->prefix . 'mh_downloader_mangas',
					array(
						'name'               => $v['text'],
						'url'                => $v['url'],
						'latest_chapter'     => $v['updated_chapter'],
						'latest_chapter_url' => $v['updated_chapter_url'],
						'status'             => 'need_update',
						'updated_in_queue'   => date( "Y-m-d" ),
					),
					array(
						'id' => $row['id']
					),
					array(
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
					)
				);
			} else {
				echo "[will not update]";
			}
		}
	}
	MH_Downloader::set_today_last_download_day();
	die();
}


function mh_download_chapters_images() {
	global $wpdb;

//	$thread = func_get_arg( 0 );
//	if ( $thread == '' ) {
//		$thread = 0;
//	}
	$thread = 0;
	mh_thread_is_break( $thread, __FUNCTION__ );

	// select chapters images to download
	/**
	 * [97] => stdClass Object
	 * (
	 * [id] => 125
	 * [status] => need_download
	 * [id_chapter_in_queue] => 208
	 * [id_post_type_chapter] => 276677
	 * [source_url] => http://2.bp.blogspot.com/-fMdnqF-ZRQo/TlG9rgpCZdI/AAAAAAAABLg/QtZcW5nwU80/s16000/025.png
	 * )
	 */
	$images_data = $wpdb->get_results(
		$wpdb->prepare(
			'SELECT * FROM ' . $wpdb->prefix . 'mh_downloader_chapter_images
			 WHERE  status = %s AND id_post_type_manga <> 0 ORDER BY id_post_type_manga, id_post_type_chapter, id LIMIT 0, 1000',

			'need_download'
		),
		ARRAY_A
	);

	$images_data_fail = $wpdb->get_results(
		$wpdb->prepare(
			'SELECT * FROM ' . $wpdb->prefix . 'mh_downloader_chapter_images WHERE status = %s ORDER BY last_try LIMIT 0, 10',
			'fail'
		),
		ARRAY_A
	);

	if ( count( $images_data_fail ) > 0 ) {
		$images_data[] = $images_data_fail[0];
	}

	foreach ( $images_data as $k => $v ) {
		mh_thread_is_break( $thread, __FUNCTION__ );

		$chapter_id        = $v['id_post_type_chapter'];
		$chapter_post_data = get_post( $chapter_id, ARRAY_A );

		$v['source_url'] = mh_fix_download_images_url( $v['source_url'] );
		$a_parts         = explode( '.', $v['source_url'] );
		$ext             = '';
		if ( count( $a_parts ) > 0 ) {
			$ext = $a_parts[ count( $a_parts ) - 1 ];
		}

		$save_file_name = sanitize_file_name( sprintf( '%03d', $v['num_in_chapter'] ) );
		$save_file_name .= '.' . $ext;
		echo "<br /><b>$save_file_name</b><br />";

		$attach_id = create_image_from_remote_host(
			$v['source_url'],
			$chapter_id,
			$chapter_post_data['post_title'] . ' - image: ' . sprintf( '%03d', $v['num_in_chapter'] ), // post title
			$save_file_name
		);
		if ( $attach_id !== false ) {
			echo "\t<span style='color: green;'>[downloaded][attach_id:$attach_id]</span>";
			// update with status 'downloaded'
			$wpdb->update(
				$wpdb->prefix . 'mh_downloader_chapter_images',
				array(
					'status'          => 'downloaded',
					'attach_id'       => $attach_id,
					'downloaded_date' => current_time( 'mysql', 1 )
				),
				array(
					'id' => $v['id'],
				)
			);
		} else {
			echo "<span style='color: red;'>[fail]</span>";
			// update with status 'fail'
			$wpdb->update(
				$wpdb->prefix . 'mh_downloader_chapter_images',
				array(
					'status'   => 'fail',
					'last_try' => date( "Y-m-d H:i:s" ),
				),
				array(
					'id' => $v['id'],
				)
			);
		}


		// if all chapter's images are downloaded set the chapter's status like 'publish'.
		// If manga's status is 'draft' set it to 'publish'
		$not_downloaded = $wpdb->get_var(
			$wpdb->prepare(
				'SELECT count(*) as cnt FROM ' . $wpdb->prefix . 'mh_downloader_chapter_images WHERE id_post_type_chapter = %d AND status <> %s',
				$v['id_post_type_chapter'], 'downloaded' )
		);

		if ( $not_downloaded == 0 ) {
			// set chapter like 'publish'
			$current_chapter              = get_post( $v['id_post_type_chapter'] );
			$current_chapter->post_status = 'publish';
			wp_update_post( $current_chapter );

			// get manga's post, and set it to 'publish'
			$manga = get_post( $current_chapter->post_parent );
			if ( $manga->post_status == 'draft' ) {
				$manga->post_status = 'publish';
				wp_update_post( $manga );
			}
			update_post_meta( $manga->ID, 'updt_dte', date( "Y-m-d H:i:s" ) );
			// Last chapter id of current manga
			update_post_meta( $manga->ID, 'last_child_id', $current_chapter->ID );
		}
	}
}

function mh_download_issues_images() {
	global $wpdb;

	$thread = func_get_arg( 0 );
	if ( $thread == '' ) {
		$thread = 0;
	}

	mh_thread_is_break( $thread, __FUNCTION__ );

	$images_data = $wpdb->get_results(
		$wpdb->prepare(
			'SELECT * FROM ' . $wpdb->prefix . 'mh_downloader_issue_images WHERE mod(id_post_type_comic, %d) = %d AND status = %s ORDER BY id_post_type_comic, id_issue_in_queue, id LIMIT 0, 1000',
			MH_THREADS_FOR_ISSUES_IMAGES,
			$thread,
			'need_download'
		),
		ARRAY_A
	);

	$images_data_fail = $wpdb->get_results(
		$wpdb->prepare(
			'SELECT * FROM ' . $wpdb->prefix . 'mh_downloader_issue_images WHERE status = %s ORDER BY last_try LIMIT 0, 1',
			'fail'
		),
		ARRAY_A
	);

	if ( count( $images_data_fail ) > 0 ) {
		$images_data[] = $images_data_fail[0];
	}

	foreach ( $images_data as $k => $v ) {
		mh_thread_is_break( $thread, __FUNCTION__ );

		$issue_id        = $v['id_post_type_issue'];
		$issue_post_data = get_post( $issue_id, ARRAY_A );

		$v['source_url'] = mh_fix_download_images_url( $v['source_url'] );

		$a_parts = explode( '.', $v['source_url'] );
		$ext     = '';

		if ( count( $a_parts ) > 0 ) {
			$ext = $a_parts[ count( $a_parts ) - 1 ];
		}

		if ( strlen( $ext ) > 5 ) {
			$ext = 'jpg';
		}

		$save_file_name = sanitize_file_name( sprintf( '%03d', $v['num_in_issue'] ) );
		$save_file_name .= '.' . sanitize_file_name( $ext );
		echo "<br /><b>$save_file_name</b><br />";

		$attach_id = create_image_from_remote_host(
			$v['source_url'],
			$issue_id,
			$issue_post_data['post_title'] . ' - image: ' . sprintf( '%03d', $v['num_in_issue'] ), // post title
			$save_file_name
		);

		if ( $attach_id !== false ) {
			echo "\t<span style='color: green;'>[downloaded][attach_id:$attach_id]</span>";
			// update with status 'downloaded'
			$wpdb->update(
				$wpdb->prefix . 'mh_downloader_issue_images',
				array(
					'status'          => 'downloaded',
					'attach_id'       => $attach_id,
					'downloaded_date' => current_time( 'mysql', 1 )
				),
				array(
					'id' => $v['id'],
				)
			);
		} else {
			echo "<span style='color: red;'>[fail]</span>";
			// update with status 'fail'
			$wpdb->update(
				$wpdb->prefix . 'mh_downloader_issue_images',
				array(
					'status'   => 'fail',
					'last_try' => date( "Y-m-d H:i:s" ),
				),
				array(
					'id' => $v['id'],
				)
			);
		}

		// if all issue's images are downloaded set the issue's status like 'publish'.
		// If comic's status is 'draft' set it to 'publish'
		$not_downloaded = $wpdb->get_var(
			$wpdb->prepare(
				'SELECT count(*) as cnt FROM ' . $wpdb->prefix . 'mh_downloader_issue_images WHERE id_post_type_issue = %d AND status <> %s',
				$v['id_post_type_issue'], 'downloaded' )
		);

		echo "<br>not_downloaded: " . $not_downloaded;

		flush();

		if ( $not_downloaded == 0 ) {
			// set issue like 'publish'
			$current_issue              = get_post( $v['id_post_type_issue'] );
			$current_issue->post_status = 'publish';
			wp_update_post( $current_issue );

			// get manga's post, and set it to 'publish'
			$comic = get_post( $current_issue->post_parent );
			if ( $comic->post_status == 'draft' ) {
				$comic->post_status = 'publish';
				wp_update_post( $comic );
			}
			update_post_meta( $comic->ID, 'updt_dte', date( "Y-m-d H:i:s" ) );
			// Last issue id of current comic
			update_post_meta( $comic->ID, 'last_child_id', $current_issue->ID );
		}

	}


}

/**
 * @param $image_file_name - image file name
 * @param $watermark_file_name - stamp file name
 */
function mh_watermark( $image_file_name, $watermark_file_name, $result_image_file_name = '' ) {
	if ( $result_image_file_name == '' ) {
		$result_image_file_name = $image_file_name;
	}

	$source = imagecreatefromstring( $source_str = file_get_contents( $image_file_name ) );
	$stamp  = imagecreatefromstring( $stamp_str = file_get_contents( $watermark_file_name ) );

	$source_info = getimagesizefromstring( $source_str );

	// Set the margins for the stamp and get the height/width of the stamp image
	$marge_right  = 20;
	$marge_bottom = 20;
	$sx           = imagesx( $stamp );
	$sy           = imagesy( $stamp );


	// Copy the stamp image onto our photo using the margin offsets and the photo
	// width to calculate positioning of the stamp.
//    imagecopy($source, $stamp, imagesx($source) - $sx - $marge_right, imagesy($source) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

	imagecolortransparent( $stamp, imagecolorat( $stamp, 1, 1 ) );

	imagecopymerge(
		$source,
		$stamp,
		imagesx( $source ) - $sx - $marge_right,
		imagesy( $source ) - $sy - $marge_bottom,
		0,
		0,
		imagesx( $stamp ),
		imagesy( $stamp ),
		70
	);

	// Save the image
	switch ( $source_info['mime'] ) {
		case 'image/png':
			imagepng( $source, $result_image_file_name );
			break;

		case 'image/jpeg':
			imagejpeg( $source, $result_image_file_name );
			break;

		default:
			imagejpeg( $source, $result_image_file_name );

	}
	imagedestroy( $source );
}


function mh_clear_old_views_this_week() {
	global $wpdb;
	$meta_name = "vtw_" . date( 'YW' );
	$sql       = $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key LIKE %s AND meta_key <> %s", 'vtw_%', $meta_name );
	$wpdb->query( $sql );
}


function mh_log( $str, $filename = 'debug_logs.txt' ) {
	$filename = dirname( __FILE__ ) . '/' . $filename;
	$str      = date( "[Y-m-d H:i:s]: " ) . $str . "\r\n";
	$handle   = fopen( $filename, 'a+' );
	fputs( $handle, $str );
	fclose( $handle );
}

function mh_microtime_float() {
	list( $usec, $sec ) = explode( " ", microtime() );

	return ( (float) $usec + (float) $sec );
}

function mh_debug_with_time( $msg = '' ) {
	static $n = 0;
	static $prev_time = 0;
	$n ++;
	$current_time = mh_microtime_float();
	$diff         = $current_time - $prev_time;
	echo "[$n][" . date( "Y-m-d H:i:s" ) . "][$diff][" . $msg . "]\n";
	$prev_time = $current_time;
}


function mh_get_artists_for_mangas() {
	global $wpdb;
	$sql    = $wpdb->prepare(
		'SELECT id, id_post_type_manga, artists_status, artists_last_try, status
		 FROM `' . $wpdb->prefix . 'mh_downloader_mangas`
		 WHERE `id_post_type_manga` <> 0 AND (`artists_status` = %s OR artists_status = %s)
		 ORDER BY artists_status, artists_last_try LIMIT 0, 100',
		'',
		'draft'
	);
	$mangas = $wpdb->get_results( $sql, ARRAY_A );
	foreach ( $mangas as $manga ) {
		$post_manga   = get_post( $manga['id_post_type_manga'] );
		$mangafox_url = 'http://mangafox.me/manga/' . str_replace( '-', '_', $post_manga->post_name ) . '/';
		if ( $post_manga->post_status == 'draft' ) {
			$wpdb->update( $wpdb->prefix . 'mh_downloader_mangas',
				array( 'artists_status' => 'draft', 'artists_last_try' => current_time( 'mysql', 1 ) ),
				array( 'id' => $manga['id'] )
			);
			continue;
		}
		$html  = file_get_contents( $mangafox_url );
		$dom   = str_get_html( $html );
		$table = $dom->find( 'table', 0 );
		if ( $table ) {
			$artists_tr  = $table->find( 'tr', 1 );
			$artists_td  = $artists_tr->find( 'td', 2 );
			$artist_name = trim( strip_tags( $artists_td->innertext ) );

			$released_td          = $artists_tr->find( 'td', 0 );
			$released             = trim( strip_tags( $released_td->innertext ) );
			$current_release_year = get_post_meta( $post_manga->ID, 'release_year', true );
			if ( empty( $current_release_year ) && ( ! empty( $released ) ) ) {
				update_post_meta( $post_manga->ID, 'release_year', $released );
			}
		}
		if ( $table && ! empty( $artist_name ) ) {
			$artists   = array();
			$artists[] = $artist_name;
			wp_set_object_terms( $post_manga->ID, $artists, 'artist' );

			$wpdb->update( $wpdb->prefix . 'mh_downloader_mangas',
				array( 'artists_status' => 'ok', 'artists_last_try' => current_time( 'mysql', 1 ) ),
				array( 'id_post_type_manga' => $post_manga->ID )
			);
		} else {
			$wpdb->update( $wpdb->prefix . 'mh_downloader_mangas',
				array( 'artists_status' => 'fail', 'artists_last_try' => current_time( 'mysql', 1 ) ),
				array( 'id_post_type_manga' => $post_manga->ID )
			);
		}
	}
}

function mh_test_code() {

	mh_download_chapters_images();
die();


	$result = mh_ajax_get_bookmarks();
	mh_pre_print_r(json_decode($result));
	die();

	$current_user_id = get_current_user_id();
	$bookmarks       = get_user_meta( $current_user_id, 'bookmarks', true );
	unset( $bookmarks[ 0] );
	update_user_meta( $current_user_id, 'bookmarks', $bookmarks );

	$current_user_id = get_current_user_id();
	$bookmarks       = get_user_meta( $current_user_id, 'bookmarks', true );
//	unset($bookmarks[0]);
//	mh_ajax_bookmarks_delete(0);

	mh_pre_print_r( $bookmarks );
	die();

	$issues_query = mh_get_json_chapter_issue_list(429426);
	mh_pre_print_r(json_decode($issues_query));


	$issues_query = mh_get_json_chapter_issue_list(429426);
	mh_pre_print_r(json_decode($issues_query));


	$issues_query = mh_get_json_chapter_issue_list(429426);
	mh_pre_print_r(json_decode($issues_query));


	die();
	$genres_slugs = array( 'unknown', '888' );
	$result       = get_terms( array( 'slug' => $genres_slugs ) );
	mh_pre_print_r( $result );
	die();
	echo date( "Y-m-d H:i:s" );
//	mh_download_issues_images( 2 );
	die();
	$handle    = @fopen( "/var/www/wait.wtf/http/wp-content/plugins/manga-parser/debug_logs.txt", "r" );
	$result    = array();
	$need_date = date( "Y-m-d" );
	if ( $handle ) {
		while ( ( $buffer = fgets( $handle, 4096 ) ) !== false ) {
			$date_info = substr( $buffer, 1, strpos( $buffer, ']' ) - 1 );
			list( $date, $time ) = explode( ' ', $date_info );

			if ( $date == $need_date ) {
				list( $hour, $minute, $second ) = explode( ':', $time );
				$result[ $need_date ][ $hour ][ $minute ] = true;
			}
		}

		$exists = 0;
		for ( $h = 0; $h <= 23; $h ++ ) {
			if ( $h < 10 ) {
				$h = '0' . $h;
			}
			for ( $m = 0; $m < 60; $m ++ ) {
				if ( $m < 10 ) {
					$m = '0' . $m;
				}
				$bg = 'gray';
				if ( isset( $result[ $need_date ][ $h ][ $m ] ) ) {
					$exists ++;
					$bg = 'green';
				}
				echo "<span style='background-color: {$bg};'> [$h:$m] </span>";

			}
		}
		$all = 60 * 24;
		echo '<br />Exists: ' . $exists . ' of ' . $all;
		echo ' ' . ( $exists / $all * 100 ) . '%';
		if ( ! feof( $handle ) ) {
			echo "Error: unexpected fgets() fail\n";
		}
		fclose( $handle );
	} else {
		echo "handle error";
	}
	die();
	$current_user_id = get_current_user_id();
	$bookmarks       = get_user_meta( $current_user_id, 'bookmarks', true );

	mh_pre_print_r( $bookmarks );
	die();
//	mh_ajax_bookmarks_mark_as_read(343836);
//	mh_ajax_bookmarks_delete(1);
//	mh_ajax_bookmarks_delete(1);
	$result = mh_ajax_get_bookmarks();
	$result = json_decode( $result );
	mh_pre_print_r( $result );

	die();
	mh_debug_with_time();
	echo "<h1>latest_first</h1>";
	mh_ajax_get_bookmarks( 'latest_first' );
	mh_debug_with_time();

	echo "<h1>popular_first</h1>";
	mh_ajax_get_bookmarks( 'popular_first' );
	mh_debug_with_time();

	echo "<h1>abc</h1>";
	mh_ajax_get_bookmarks( 'abc' );
	mh_debug_with_time();

	die();


	mh_ajax_bookmarks_save( 335482 );
	mh_ajax_bookmarks_save( 333288 );
	mh_ajax_bookmarks_mark_as_read( 333354 );
	mh_ajax_bookmarks_mark_as_read( 333353 );
	mh_ajax_bookmarks_mark_as_read( 333352 );

	mh_ajax_bookmarks_save( 331524 );
	mh_ajax_bookmarks_mark_as_read( 331525 );
	mh_ajax_bookmarks_mark_as_read( 331526 );
	mh_ajax_bookmarks_mark_as_read( 331527 );

	mh_ajax_bookmarks_save( 343846 );
	mh_ajax_bookmarks_mark_as_read( 343847 );
	mh_ajax_bookmarks_mark_as_read( 343848 );
	mh_ajax_bookmarks_mark_as_read( 343849 );
	mh_ajax_bookmarks_mark_as_read( 343850 );
	mh_ajax_bookmarks_mark_as_read( 343851 );
	mh_ajax_bookmarks_mark_as_read( 343852 );

	mh_ajax_bookmarks_save( 343828 );
	mh_ajax_bookmarks_mark_as_read( 343829 );
	mh_ajax_bookmarks_mark_as_read( 343830 );
	mh_ajax_bookmarks_mark_as_read( 343831 );
	mh_ajax_bookmarks_mark_as_read( 343840 );
	mh_ajax_bookmarks_mark_as_read( 343841 );

	mh_ajax_get_bookmarks();

	die();

}

/**
 * @param $my_anime_list_data - SimpleXMLElement Object manga's data from myanimelist site
 *
 * @return string score or null when this data doesn't exists
 * Example: 6.75
 */
function get_rating_from_mal_data( $my_anime_list_data ) {
	$my_anime_list_data = ( (array) $my_anime_list_data );
	if ( isset( $my_anime_list_data['score'] ) ) {
		return $my_anime_list_data['score'];
	} else {
		return null;
	}
}

/**
 * Get Release Year for manga.
 *
 * @param $my_anime_list_data - manga's data from myanimelist site
 *
 * @return int release year or null
 */
function get_release_year_from_mal_data( $my_anime_list_data ) {
	$my_anime_list_data = ( (array) $my_anime_list_data );
	if ( isset( $my_anime_list_data['start_date'] ) ) {
		list( $year, $month, $day ) = explode( '-', $my_anime_list_data['start_date'] );

		return $year;
	}

	return null;
}

/**
 * @param $title - manga's name
 * if myanimelist found more than one manga, compare all found titles
 *
 * @return SimpleXMLElement|null  with manga's data if manga was found. Otherwise null. Example:
 * SimpleXMLElement Object
 * (
 * [id] => 1623
 * [title] => Cherry
 * [english] => SimpleXMLElement Object
 * (
 * )
 *
 * [synonyms] => SimpleXMLElement Object
 * (
 * )
 *
 * [chapters] => 41
 * [volumes] => 4
 * [score] => 7.22
 * [type] => Manga
 * [status] => Finished
 * [start_date] => 2006-00-00
 * [end_date] => 2007-00-00
 * [synopsis] => In a faded little rural town, Kaworu lazily lives out his days. Until the day his destiny strikes, and he meets an angel. Just moments later, they quickly take off... Together, away from this reality!
 *
 * (Source: MU)
 * [image] => http://cdn.myanimelist.net/images/manga/2/66407.jpg
 * )
 *
 */
function mh_fetch_manga_data_from_my_anime_list( $title ) {
	$ch   = curl_init();
	$url  = 'http://myanimelist.net/api/manga/search.xml?q=' . urlencode( $title );
	$page = dom_parser_curl_get_file( $url, $ch );
	curl_close( $ch );
	if ( $page === false ) {
		return null;
	}
	$content = simplexml_load_string( $page );

	if ( $content->count() == 1 ) {
		if ( $content->entry->title == $title ) {
			return $content->entry;
		} else {
			return null;
		}
	} elseif ( $content->count() > 1 ) {
		// try to found our manga
		foreach ( $content->entry as $k => $v ) {
			if ( $v->title == $title ) // this is manga we find
			{
				return $v;
			}
		}

		return null;
	}
}

function mh_recursive_delete_folder( $folder_name, $lev = 1 ) {
	$files = scandir( $folder_name );

	foreach ( $files as $file ) {
		if ( in_array( $file, array( '.', '..' ) ) ) {
			continue;
		}
		$full_file_name = $folder_name . '/' . $file;

		echo "<br />($lev)" . $full_file_name;

		if ( is_file( $full_file_name ) ) {
			echo " [file]";
		} else {
			echo " [FOLDER]";
			mh_recursive_delete_folder( $full_file_name, $lev + 1 );
		}
		unlink( $full_file_name );
	}
	if ( $lev > 1 ) {
		rmdir( $folder_name );
	}
//	mh_pre_print_r($files);
}

// tmp function for deleting parsed data
function mh_tmp_delete_parsed_data() {
	global $wpdb;

	echo "<h2>Truncating tables</h2>";

	// truncate tables
	$sql1 = 'TRUNCATE TABLE wp_mh_downloader_chapters';
	$sql2 = 'TRUNCATE TABLE wp_mh_downloader_chapter_images';
	$sql3 = 'TRUNCATE TABLE wp_mh_downloader_comic';
	$sql4 = 'TRUNCATE TABLE wp_mh_downloader_issues';
	$sql5 = 'TRUNCATE TABLE wp_mh_downloader_issue_images';
	$sql6 = 'TRUNCATE TABLE wp_mh_downloader_mangas';

	$wpdb->query( $sql1 );
	$wpdb->query( $sql2 );
	$wpdb->query( $sql3 );
	$wpdb->query( $sql4 );
	$wpdb->query( $sql5 );
	$wpdb->query( $sql6 );

	// delete all chapters
	// delete all mangas
	// delete all comics
	// delete all issues
	echo "<h2>Deleting images from uploads dir</h2>";
	mh_recursive_delete_folder( '/var/www/wait.wtf/http/wp-content/uploads/mng' );
	mh_recursive_delete_folder( '/var/www/wait.wtf/http/wp-content/uploads/chp' );
	mh_recursive_delete_folder( '/var/www/wait.wtf/http/wp-content/uploads/com' );
	mh_recursive_delete_folder( '/var/www/wait.wtf/http/wp-content/uploads/iss' );

	$args      = array(
		'post_type'      => array( 'manga', 'chapter', 'comic', 'issue', 'attachment' ),
		'posts_per_page' => 10000
	);
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ) {
		echo '<ul>';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$post_id = get_the_ID();
			echo '<li>(' . $post_id . ') ' . get_the_title() . '</li>';
			wp_delete_post( get_the_ID() );
		}
		echo '</ul>';
	}

	echo "<h2>Delete taxonomies</h2>";

	/** Delete All the Taxonomies */
	foreach ( array( 'publisher', 'comic_genre', ) as $taxonomy ) {
		// Prepare & excecute SQL
		$terms = $wpdb->get_results( $wpdb->prepare( "SELECT t.*, tt.* FROM $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('%s') ORDER BY t.name ASC", $taxonomy ) );

		// Delete Terms
		if ( $terms ) {
			foreach ( $terms as $term ) {
				$wpdb->delete( $wpdb->term_taxonomy, array( 'term_taxonomy_id' => $term->term_taxonomy_id ) );
				$wpdb->delete( $wpdb->terms, array( 'term_id' => $term->term_id ) );
				delete_option( 'prefix_' . $taxonomy->slug . '_option_name' );
			}
		}

		// Delete Taxonomy
		$wpdb->delete( $wpdb->term_taxonomy, array( 'taxonomy' => $taxonomy ), array( '%s' ) );
	}


	update_option( 'mh_last_downloaded_mangas_list', '' );

	update_option( 'mh_last_dowloaded_comic_list', '' );

	die();


	//wp_mh_downloader_chapters
	//mh_downloader_chapter_images
	//mh_downloader_comic
	//mh_downloader_issues
	//mh_downloader_issue_images
	//mh_downloader_mangas

	// clear uploads dir 'mng', 'chp', 'com', 'issue'

//	wp_reset_query()


//	$taxonomies_flags = [ ];

	// Delete previous information
//	$wpdb->query( "DELETE FROM  `wp_postmeta` WHERE meta_key =  'release_year'" );
//	$wpdb->query( "DELETE FROM  `wp_postmeta` WHERE meta_key =  'id_mng'" );
//	$wpdb->query( "DELETE FROM  `wp_postmeta` WHERE meta_key =  'day_add'" );
//	$wpdb->query( "DELETE FROM  `wp_postmeta` WHERE meta_key =  'cnt_children'" );
//	$wpdb->query( "DELETE FROM  `wp_postmeta` WHERE meta_key =  'updt_dte'" );
//	$wpdb->query( "DELETE FROM  `wp_postmeta` WHERE meta_key =  'last_child_id'" );
//	$wpdb->query( "DELETE FROM  `wp_postmeta` WHERE post_id >= 43" );
//	$wpdb->query( "DELETE FROM  `wp_postmeta` WHERE meta_key =  'altr_nms'" );
//	$wpdb->query( "DELETE FROM  `wp_posts` WHERE ID >= 43" );
//	$wpdb->query( "UPDATE  " . $wpdb->prefix . "mh_downloader_mangas SET STATUS =  'need_download'" );
//	$wpdb->query( "UPDATE  " . $wpdb->prefix . "mh_downloader_chapters SET STATUS =  'need_download'" );
}
/**
 * Show data between tags <pre> ... </pre>
 *
 * @param $mixData
 */
function mh_pre_print_r( $mixData ) {
	echo "<pre>";
	print_r( $mixData );
	echo "</pre>";
}
// Denis code
function create_image_from_remote_host( $file_path, $post_id, $post_title = '', $img_file_name = '' ) {
	$image      = sanitize_text_field( $file_path );
	$upload_dir = wp_upload_dir();

	//Get the remote image and save to uploads directory

	if ( $img_file_name == '' ) {
		$img_file_name = $post_id . "_" . basename( $image );
	}

	$current_post = get_post( $post_id );
	if ( $current_post->post_type == 'manga' ) {
		$instead_post_id = implode( '/', str_split( $post_id, 3 ) );
		$dir_after_uploads = 'mng/' . $instead_post_id;
		$full_dir_path     = $upload_dir['path'] . '/' . $dir_after_uploads;
		if ( ! file_exists( $full_dir_path ) ) {
//			echo "<br>Will mkdir <b>$full_dir_path</b><br />";
			mkdir( $full_dir_path, 0755, true );
		}
		$upload_dir['path']    = $full_dir_path;
		$upload_dir['basedir'] = $full_dir_path;
		$upload_dir['url']     = $upload_dir['url'] . '/' . $dir_after_uploads;
		$upload_dir['baseurl'] = $upload_dir['url'];
	} elseif ( $current_post->post_type == 'chapter' ) {
		$post_chapter = get_post( $post_id );
		$post_manga   = get_post( $post_chapter->post_parent );
		$instead_post_id = implode( '/', str_split( $post_manga->ID, 3 ) );
		$dir_after_uploads = 'chp/' . $instead_post_id . '/ch_' . $post_chapter->ID . '_' . sanitize_file_name( $post_chapter->post_title );
		$full_dir_path     = $upload_dir['path'] . '/' . $dir_after_uploads;
		if ( ! file_exists( $full_dir_path ) ) {
//			echo "<br>Will mkdir <b>$full_dir_path</b><br />";
			mkdir( $full_dir_path, 0755, true );
		}
		$upload_dir['path']    = $full_dir_path;
		$upload_dir['basedir'] = $full_dir_path;
		$upload_dir['url']     = $upload_dir['url'] . '/' . $dir_after_uploads;
		$upload_dir['baseurl'] = $upload_dir['url'];
	}

	$i = 0;
	do {
		$i ++;
		$img     = wp_remote_get( $image );
		$img     = wp_remote_retrieve_body( $img );
		$img_len = strlen( $img );

	} while ( ( $img_len == 0 ) && ( $i < 3 ) );

	if ( strpos( $file_path, 'http:' ) === false ) {
		$img = file_get_contents( $file_path );
//		echo "[[[[$file_path]]]]";
	}

	$img_len = strlen( $img );
	if ( $img_len == 0 ) {

		return false;
	}
	if ( ! file_exists( $upload_dir['path'] . '/' . $img_file_name ) ) {
		$fp = fopen( $upload_dir['path'] . '/' . $img_file_name, 'w' );
		fwrite( $fp, $img );
		fclose( $fp );
	}

	$wp_filetype = wp_check_filetype( $image, null );

	if ( $post_title == '' ) {
		$post_title = preg_replace( '/\.[^.]+$/', '', $img_file_name );
	}

	if ( $wp_filetype['type'] == '' ) {
		$wp_filetype['type'] = 'image/jpeg';
	}

	$attachment = array(
		'post_mime_type' => $wp_filetype['type'],
		'post_title'     => $post_title,
		'post_content'   => '',
		'post_status'    => 'inherit'
	);

	//require for wp_generate_attachment_metadata which generates image related meta-data also creates thumbs
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	$attach_id = wp_insert_attachment( $attachment, $upload_dir['path'] . '/' . $img_file_name, $post_id );
	if( defined("DOING_AJAX") )
		add_post_meta($attach_id, 'origin_image', $file_path, true );
	//Generate post thumbnail of different sizes.
	$attach_data = wp_generate_attachment_metadata( $attach_id, $upload_dir['path'] . '/' . $img_file_name );
	wp_update_attachment_metadata( $attach_id, $attach_data );
	return $attach_id;
}


add_filter( 'cron_schedules', 'mh_add_cron_schedule' );
function mh_add_cron_schedule( $schedules ) {
	$schedules['weekly'] = array(
		'interval' => 604800, // 1 week in seconds
		'display'  => __( 'Once Weekly' ),
	);
	$schedules['1min']   = array(
		'interval' => 60, // 1 min in seconds
		'display'  => __( 'Each 1 minute' ),
	);
	$schedules['10min']  = array(
		'interval' => 600, // 10 min in seconds
		'display'  => __( 'Each 10 minute' ),
	);

	return $schedules;
}

/**
 * If this thread number was active with different pid fewer than 30 seconds then die
 */
function mh_thread_is_break( $thread_number, $prefix = '' ) {
	$dir       = plugin_dir_path( __FILE__ );
	$file_name = $dir .= 'thread_' . $prefix . $thread_number . '.txt';
	$content   = file_get_contents( $file_name );
	list( $prev_time, $prev_pid ) = explode( '|', $content );
	$current_time = time();
	$difference   = $current_time - $prev_time;
	$current_pid  = getmypid();
	if ( ( $difference < 10 ) && ( $prev_pid != $current_pid ) ) {
		echo " [stop] ";
		die();
	}
	file_put_contents( $file_name, $current_time . '|' . $current_pid );
}

add_action( 'mh_cron_thumbnails', 'mh_set_thumbnail_to_next_manga', 10 );

if ( ! wp_next_scheduled( 'mh_cron_thumbnails' ) ) {
	wp_schedule_event( time(), '1min', 'mh_cron_thumbnails' );
}

//add_action( 'mh_cron_thumbnails_for_comic', 'mh_set_thumbnail_to_next_comic', 10 );
//if ( ! wp_next_scheduled( 'mh_cron_thumbnails_for_comic' ) ) {
//	wp_schedule_event( time(), '10min', 'mh_cron_thumbnails_for_comic' );
//}

//add_action( 'mh_cron_artists_for_mangas', 'mh_get_artists_for_mangas', 10 );
//if ( ! wp_next_scheduled( 'mh_cron_artists_for_mangas' ) ) {
//	wp_schedule_event( time(), '10min', 'mh_cron_artists_for_mangas' );
//}


//mh_clear_old_views_this_week
//add_action( 'mh_cron_clear_old_views_this_week', 'mh_clear_old_views_this_week', 10 );
//if ( ! wp_next_scheduled( 'mh_cron_clear_old_views_this_week' ) ) {
//	wp_schedule_event( time(), 'weekly', 'mh_cron_clear_old_views_this_week' );
//}

// mh_download_chapters_images
//add_action( 'mh_cron_chapter_images', 'mh_download_chapters_images', 10 );

//if ( ! wp_next_scheduled( 'mh_cron_chapter_images' ) ) {
//	wp_schedule_event( time(), '1min', 'mh_cron_chapter_images' );
//}

// mh_download_issues_images
//add_action( 'mh_cron_issue_images', 'mh_download_issues_images', 10 );

//if ( ! wp_next_scheduled( 'mh_cron_issue_images' ) ) {
//	wp_schedule_event( time(), '1min', 'mh_cron_issue_images' );
//}


/**
 * Update release year, rating, and thumbnail image!
 */
function mh_set_thumbnail_to_next_manga() {
	set_time_limit( 59 );

	global $wpdb;

	// Get mangas without thumbnails
	$mangas_without_thumbnails = $wpdb->get_results(
		$wpdb->prepare( 'SELECT * FROM ' . $wpdb->prefix . 'mh_downloader_mangas WHERE (thumb_mal_status = %s OR thumb_mal_status = %s)  AND status = %s LIMIT 0, 50', '', 'fail', 'downloaded' ),
		ARRAY_A
	);
	foreach ( $mangas_without_thumbnails as $k => $v ) {
//		echo '<h4>name: ' . $v['name'] . "</h4>\n";
		flush();
		$row_manga = $wpdb->get_row( $wpdb->prepare( 'SELECT * FROM wp_posts WHERE post_title = %s', $v['name'] ), ARRAY_A );

		$id_manga   = $row_manga['ID'];
		$manga_post = get_post( $id_manga );

		$mal_data = mh_fetch_manga_data_from_my_anime_list( $manga_post->post_title );

		// getting rating and release_year from MyAnimeList
		$release_year = get_release_year_from_mal_data( $mal_data );
		$rating       = get_rating_from_mal_data( $mal_data );
		update_post_meta( $id_manga, 'release_year', $release_year );
		update_post_meta( $id_manga, 'rating', $rating );

		$mal_data = ( (array) $mal_data );
		if ( isset( $mal_data['image'] ) ) {

			$a_parts = explode( '.', $mal_data['image'] );
			$ext     = '';
			if ( count( $a_parts ) > 0 ) {
				$ext = $a_parts[ count( $a_parts ) - 1 ];
			}

			$save_file_name = sanitize_file_name( $manga_post->post_title ) . '.' . $ext;

			$attach_id = create_image_from_remote_host( $mal_data['image'], $manga_post->ID, $manga_post->post_title, $save_file_name );



			if ( $attach_id ) {
//				echo "[downloaded] " . $mal_data['image'];
				set_post_thumbnail( $manga_post->ID, $attach_id );
				$wpdb->update(
					$wpdb->prefix . 'mh_downloader_mangas',
					[ 'thumb_mal_status' => 'downloaded', 'thumb_mal_last_try' => current_time( 'mysql', 1 ) ],
					[ 'id' => $v['id'] ],
					[ '%s', '%s' ]
				);
			} else {
//				echo "[fail] " . $mal_data['image'];
				$wpdb->update(
					$wpdb->prefix . 'mh_downloader_mangas',
					[ 'thumb_mal_status' => 'fail', 'thumb_mal_last_try' => current_time( 'mysql', 1 ) ],
					[ 'id' => $v['id'] ],
					[ '%s', '%s' ]
				);
			}
		} else {
//			echo "[without_image]";
			$wpdb->update(
				$wpdb->prefix . 'mh_downloader_mangas',
				[ 'thumb_mal_status' => 'without_image', 'thumb_mal_last_try' => current_time( 'mysql', 1 ) ],
				[ 'id' => $v['id'] ],
				[ '%s', '%s' ]
			);
		}


	}
	die();
}

function mh_set_thumbnail_to_next_comic() {
	set_time_limit( 59 );

	global $wpdb;

	// Get comics without thumbnails
	$comics_without_thumbnails = $wpdb->get_results(
		$wpdb->prepare(
			'SELECT * FROM ' . $wpdb->prefix . 'mh_downloader_comic
			WHERE (thumb_rco_status = %s OR thumb_rco_status = %s) AND status = %s LIMIT 0, 50',
			'',
			'fail',
			'downloaded' ),
		ARRAY_A
	);

	foreach ( $comics_without_thumbnails as $k => $v ) {
		echo '<h4>name: ' . $v['name'] . "</h4>\n";
		echo $v['image_url'];
		flush();

		$id_post_type_comic = $v['id_post_type_comic'];
		echo "<br />" . $id_post_type_comic;
		$comic_post = get_post( $id_post_type_comic );

		$a_parts = explode( '.', $v['image_url'] );
		$ext     = '';
		if ( count( $a_parts ) > 0 ) {
			$ext = $a_parts[ count( $a_parts ) - 1 ];
		}

		$save_file_name = sanitize_file_name( $comic_post->post_title ) . '.' . $ext;

		$attach_id = create_image_from_remote_host( $v['image_url'], $comic_post->ID, $comic_post->post_title, $save_file_name );
		if ( $attach_id ) {
			echo "[downloaded] " . $v['image_url'];
			set_post_thumbnail( $comic_post->ID, $attach_id );
			$wpdb->update(
				$wpdb->prefix . 'mh_downloader_comic',
				[ 'thumb_rco_status' => 'downloaded', 'thumb_rco_last_try' => current_time( 'mysql', 1 ) ],
				[ 'id' => $v['id'] ],
				[ '%s', '%s' ]
			);
		} else {
			echo "[fail] " . $v['image_url'];
			$wpdb->update(
				$wpdb->prefix . 'mh_downloader_comic',
				[ 'thumb_rco_status' => 'fail', 'thumb_rco_last_try' => current_time( 'mysql', 1 ) ],
				[ 'id' => $v['id'] ],
				[ '%s', '%s' ]
			);
		}
	}
	die();
}


//register_activation_hook(__FILE__, function () {
//    // Mangas sizes
//    add_image_size('manga-small', 137, 218);
//    add_image_size('manga-normal', 170, 270);
//    add_image_size('manga-big', 183, 292);
////    add_image_size('manga-normal', 170, 271);
////    add_image_size('manga-normal-172x274', 172, 274);
//
//    // add_image_size('manga-normal-172x274', 291, 464);
//});

/**
 * @param $url
 *
 * @return string new url
 *
 * Example.
 * before
 * https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&gadget=a&no_expand=1&resize_h=0&rewriteMime=image%2F*&url=http%3a%2f%2fi.imgur.com%2fMwyGMSv.jpg&imgmax=30000
 * after
 * http://i.imgur.com/MwyGMSv.jpg
 */
function mh_fix_download_images_url( $url ) {
	if ( strpos( $url, 'https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?' ) !== false ) {
		$parts  = explode( '&url=', $url );
		$result = explode( '&', $parts[1] );
		$result = $result[0];
		$result = urldecode( $result );

		return $result;
	} else {
		return $url;
	}
}
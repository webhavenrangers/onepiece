<?php

class MH_Readcomiconline {
	/**
	 * @param $html - HTML code from comic page
	 *
	 * @return array of comic's data
	 */
	public static function get_comic_data( $html ) {
		$comic_data = array();

		$dom                = str_get_html( $html );
		$comic_link         = $dom->find( 'a.bigChar', 0 );
		$comic_data['name'] = $comic_link->innertext;
		$comic_data['url']  = $comic_link->href;

		$tags_p = $dom->find( 'div.bigBarContainer div.barContent div p' );
		foreach ( $tags_p as $k => $tag_p ) {
			echo $k . ' ';
			$span_info = $tag_p->find( 'span.info', 0 );
			if ( $span_info ) {
				echo "[" . $span_info->innertext . "]";
				switch ( $span_info->innertext ) {
					case 'Other name:':
						$other_name_link          = $tag_p->find( 'a', 0 );
						$comic_data['alternate_names'] = $other_name_link->innertext;;
						break;
					case 'Genres:':
						$genres_links = $tag_p->find( 'a' );
						$genres       = array();
						if ( $genres_links ) {
							foreach ( $genres_links as $link ) {
								$genre         = array();
								$genre['url']  = $link->href;
								$genre['name'] = $link->innertext;
								$genres[]      = $genre;
							}
						}
						$comic_data['genres'] = $genres;
						break;
					case 'Publisher:':
						$other_name_link         = $tag_p->find( 'a', 0 );
						$comic_data['publisher'] = $other_name_link->innertext;;
						break;
					case 'Writer:':
						$writers_links = $tag_p->find( 'a' );
						$writers       = array();
						if ( $writers_links ) {
							foreach ( $writers_links as $link ) {
								$writer         = array();
								$writer['url']  = $link->href;
								$writer['name'] = $link->innertext;
								$writers[]      = $writer;
							}
						}
						$comic_data['writers'] = $writers;
						break;
					case 'Artist:':
						$artists_links = $tag_p->find( 'a' );
						$artists       = array();
						if ( $artists_links ) {
							foreach ( $artists_links as $link ) {
								$artist         = array();
								$artist['url']  = $link->href;
								$artist['name'] = $link->innertext;
								$artists[]      = $artist;
							}
						}
						$comic_data['artists'] = $artists;
						break;
					case 'Publication date:':
						// Publication date
						$publication_date_tag_p = $tag_p;
						list( , $publication_date_html_part ) = explode( '</span>', $publication_date_tag_p->innertext );
						$publication_date_html_part = str_replace( '&nbsp;', '', $publication_date_html_part );
						list( $start_date, $finish_date ) = explode( ' - ', $publication_date_html_part );
						$start_date                = trim( $start_date );
						$finish_date               = trim( $finish_date );
						$comic_extra_data['pub_str_dte'] = $start_date;
						$comic_extra_data['pub_fin_dte'] = $finish_date;
						$comic_data['extra_data'] = $comic_extra_data;

						if ( preg_match( '/[0-9]{4}/', $start_date, $matches ) ) {
							$comic_data['release_year'] = $matches[0];
						}
						break;
					case 'Status:':
						// Status & Views
						$part_of_html = $tag_p;
						$part_of_html = str_replace( '&nbsp;', '', $part_of_html );
						$parts        = explode( '</span>', $part_of_html );
						list( $status, ) = explode( ' ', $parts[1] );
						list( $views, ) = explode( ' ', $parts[2] );
						$views                = str_replace( ',', '', $views );
						$comic_data['status'] = $status;
						$comic_data['views']  = $views;
						break;
					case 'Summary:':
						$summary = '';
						for ( $i = $k + 1; $i <= 20; $i ++ ) {
							$current_p = $dom->find( 'div.bigBarContainer div.barContent div p', $i )->outertext;
							if ( $current_p == '' ) {
								break;
							}
							$summary .= $current_p . "\n";
						}
						$comic_data['summary'] = trim( $summary );
						break;
				}
			} else {
			}
			echo "<br />";
		}

		// Image (cover)
		$comic_data['image'] = $dom->find( 'link[rel=image_src]', 0 )->href;

		// Issues
		$trs = $dom->find( 'table.listing tr' );
		if ( $trs ) {
			foreach ( $trs as $tr ) {
				$issue        = array();
				$issue['url'] = $tr->find( 'td a', 0 )->href;
				if ( $issue['url'] ) {
					$issue['name']          = htmlspecialchars_decode( trim( $tr->find( 'td a', 0 )->innertext ), ENT_QUOTES );
					$issue['date']          = trim( $tr->find( 'td', 1 )->innertext );
					$comic_data['issues'][] = $issue;
				}
			}
		}

		return $comic_data;

	}


	public static function get_image_list_from_issue_page( $html ) {
		preg_match_all(
			'|lstImages.push\(\"(.*?)\"\);|si',
			$html,
			$matches );

		return $matches[1];
	}

}
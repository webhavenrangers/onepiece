<?php

class MH_Kissmanga {

	/**
	 * @param $sHtml - html from manga's page
	 */
	public static function getMangasData( $html ) {
		$manga_data = array();

		$dom                = str_get_html( $html );
		$a                  = $dom->find( 'a.bigChar', 0 );
		$manga_data['name'] = $a->innertext;
		$manga_data['url']  = $a->href;

		// Other names (Alternate names)
		preg_match_all( '|<span class="info">Other name:</span>&nbsp;(.*?)</p>|si', $html, $aMatches );
		if ( isset( $aMatches[1] ) ) {
			$html_part = $aMatches[1][0];
			preg_match_all( '|<a href="(.*?)" title="(.*?)">(.*?)</a>|si', $html_part, $aMatches );
			if ( isset( $aMatches[3] ) ) {
				$manga_data['alternate_names'] = implode( ';', $aMatches[3] );
			}
		}

		// Genres
		preg_match_all( '|<span class="info">Genres:</span>&nbsp;(.*?)</p>|si', $html, $aMatches );
		$sHtmlGenres = $aMatches[1][0];
		preg_match_all( '|<a href="(.*?)" class="dotUnder" title="(.*?)">(.*?)</a>|si', $sHtmlGenres, $aMatches );
		$aGenres = [ ];
		foreach ( $aMatches[1] as $kG => $vG ) {
			$aGenres[] = [
				'url'         => $aMatches[1][ $kG ],
				'description' => trim( $aMatches[2][ $kG ] ),
				'name'        => $aMatches[3][ $kG ],
			];
		}
		$manga_data['genres'] = $aGenres;

		// Authors (sometimes more than one author)
		preg_match_all( '|<span class="info">Author:</span>&nbsp;(.*?)</p>|si', $html, $aMatches );
		$sAuthorsHtml = $aMatches[1][0];
		preg_match_all( '|<a href="(.*?)" class="dotUnder" title="">(.*?)</a>|si', $sAuthorsHtml, $aMatches );
		$aAuthors = [ ];
		foreach ( $aMatches[1] as $kA => $vA ) {
			$aAuthors[] = [
				'name' => $aMatches[2][ $kA ],
				'url'  => $aMatches[1][ $kA ],
			];
		}

		$manga_data['authors'] = $aAuthors;

		// Summary
		preg_match_all( '|<span class="info">Summary:</span>(.*?)</div|si', $html, $aMatches );
		$summary               = $aMatches[1][0];
		$manga_data['summary'] = trim( $summary );

		// Image (cover)
		$manga_data['image'] = $dom->find( 'link[rel=image_src]', 0 )->href;

		// Status
		preg_match_all( '|Status:</span>(.*?)<span|si', $html, $aMatches );
		$status               = $aMatches[1][0];
		$status               = str_replace( '', ' ', $status );
		$status               = str_replace( '&nbsp;', ' ', $status );
		$status               = trim( str_replace( '  ', ' ', $status ) );
		$manga_data['status'] = $status;

		// Views
		preg_match_all( '|Views:</span>(.*?)<span|si', $html, $aMatches );
		$views               = $aMatches[1][0];
		$views               = str_replace( ',', '', $views );
		$views               = str_replace( '&nbsp;', ' ', $views );
		$views               = trim( str_replace( '  ', ' ', $views ) );
		$manga_data['views'] = $views;

		// array of chapters
		$trs = $dom->find( 'table.listing tr' );
		if ( $trs ) {
			foreach ( $trs as $tr ) {
				$chapter        = array();
				$chapter['url'] = $tr->find( 'td a', 0 )->href;
				if ( $chapter['url'] ) {
					$chapter['name'] = htmlspecialchars_decode (trim( $tr->find( 'td a', 0 )->innertext), ENT_QUOTES);
					$chapter['date'] = trim( $tr->find( 'td', 1 )->innertext );

					$manga_data['chapters'][] = $chapter;
				}
			}
		}
		return $manga_data;
	}

	/**
	 * @return array of chapter's images
	 */
	public static function get_image_list_from_chapter_page( $html ) {
		preg_match_all(
			'|lstImages.push\(\"(.*?)\"\);|si',
			$html,
			$matches );
		return $matches[1];
	}
}
<?php

/**
 * Class MH_Downloader for working with Human Emulator Downloader
 */
class MH_Downloader
{

    /**
     * Stop script execution if ip is not permissible.
     */
    public static function die_if_ip_is_not_permissible()
    {
        if (!self::check_ip()) {
            return true;
            $response = array(
                'error' => 1,
                'error_message' => 'Your ip ' . $_SERVER['REMOTE_ADDR'] . ' is not permissible',
            );
            echo json_encode($response);
            die();
        }
    }

    /**
     * @return bool true if ip is in available list, otherwise return false
     */
    public static function check_ip()
    {
        return true;
        if (in_array($_SERVER['REMOTE_ADDR'], [
            '162.158.115.162',
            '46.175.254.13'
        ])) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @return bool true - if comic list was downloaded today, otherwise false
     */
    public static function was_downloaded_comic_list_today()
    {
        $day = self::comic_list_downloaded_date();
        // If list was downloaded not today
        if ($day == date("Y-m-d")) {
            return true;
        }

        return false;
    }

    public static function comic_list_downloaded_date()
    {
        return get_option('mh_last_dowloaded_comic_list');
    }

    public static function was_downloaded_mangas_list_today()
    {
        $day = self::mangas_list_downloaded_date();
        // If list was downloaded not today
        if ($day == date("Y-m-d")) {
            return true;
        }

        return false;
    }

    public static function mangas_list_downloaded_date()
    {
        return get_option('mh_last_downloaded_mangas_list');
    }

    /**
     * Return string with task name for downloader.
     * @return string
     */
    public static function what_to_do()
    {
        /**
         * If needs to download comic list
         */
//		if ( ! self::was_downloaded_comic_list_today() ) {
//			global $wpdb;
//			$comic = $wpdb->get_results( 'SELECT * FROM ' . $wpdb->prefix . 'mh_downloader_comic' );
//
//			return [
//				'task_name' => 'download_comic_list',
//				'comic'     => $comic,
//				'your_ip'   => $_SERVER['REMOTE_ADDR'],
//			];
//		}
//
//		if ( self::is_need_to_download_issues() ) {
//			return [
//				'task_name' => 'download_issues',
//				'issues'    => self::get_issues_list_to_download(),
//				'your_ip'   => $_SERVER['REMOTE_ADDR'],
//			];
//		}
//
//
//		if ( self::is_need_to_download_comics() ) {
//			return [
//				'task_name'   => 'download_comics',
//				'comics_list' => self::get_comics_list_to_download(),
//				'your_ip'     => $_SERVER['REMOTE_ADDR'],
//			];
//		}


        /**
         * If needs to download mangas list
         */
        if (!self::was_downloaded_mangas_list_today()) {
            global $wpdb;
            $mangas = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'mh_downloader_mangas');

            return [
                'task_name' => 'download_mangas_list',
                'mangas' => $mangas,
                'your_ip' => $_SERVER['REMOTE_ADDR'],
            ];
        }
//
//		if ( self::is_need_to_download_mangas_images_from_kissmanga() ) {
//			return [
//				'task_name'   => 'download_images_from_kissmanga',
//				'images_list' => self::get_images_list_to_download_from_kissmanga(),
//				'your_ip'     => $_SERVER['REMOTE_ADDR'],
//			];
//		}

        if (self::is_need_to_download_chapters()) {
            return [
                'task_name' => 'download_chapters',
                'chapters' => self::get_chapters_list_to_download(),
                'your_ip' => $_SERVER['REMOTE_ADDR'],
            ];
        }

        if (self::is_need_to_download_mangas()) {
            return [
                'task_name' => 'download_mangas',
                'mangas_list' => self::get_mangas_list_to_download(),
                'your_ip' => $_SERVER['REMOTE_ADDR'],
            ];
        }

        if (self::is_need_to_download_mangas()) {
            return [
                'task_name' => 'download_mangas',
                'mangas_list' => self::get_mangas_list_to_download(),
                'your_ip' => $_SERVER['REMOTE_ADDR'],
            ];
        }

        return false;

    }

    public static function set_today_last_download_day()
    {
        update_option('mh_last_downloaded_mangas_list', date("Y-m-d"));
    }

    public static function set_today_last_download_day_comic()
    {
        update_option('mh_last_dowloaded_comic_list', date("Y-m-d"));
    }


    /**
     * Check if need to download mangas. If we have mangas with status 'need_download', return true, otherwise false
     */
    public static function is_need_to_download_mangas()
    {
        global $wpdb;
        $count_mangas =
            $wpdb->get_var(
                $wpdb->prepare(
                    'SELECT COUNT( * ) AS cnt FROM ' . $wpdb->prefix . 'mh_downloader_mangas WHERE url=%s OR url=%s OR url=%s OR url=%s
                    OR url=%s OR url=%s
                    AND (STATUS = %s OR STATUS = %s)',
                    '/Manga/One-Piece',
                    '/Manga/One-Piece-Digital-Colored-Comics',
                    '/Manga/One-Piece-Omake',
                    '/Manga/One-Piece-Party',
                    '/Manga/Wanted-One-Piece',
                    'need_download',
                    'need_update'
                )
            );

        if ($count_mangas == 0) {
            return false;
        } else {
            return $count_mangas;
        }
    }

    /**
     * Check if need to download comics. If we have comics with status 'need_download' or 'need_update', return true, otherwise false
     */
    public static function is_need_to_download_comics()
    {
        global $wpdb;
        $count_comics =
            $wpdb->get_var(
                $wpdb->prepare(
                    'SELECT COUNT( * ) AS cnt FROM ' . $wpdb->prefix . 'mh_downloader_comic WHERE id <= 1010 AND (STATUS = %s OR STATUS = %s)',
                    'need_download',
                    'need_update'
                )
            );
        if ($count_comics == 0) {
            return false;
        } else {
            return $count_comics;
        }
    }

    /**
     * @return $count_chapters - how many chapters we need to download.
     */
    public static function is_need_to_download_chapters()
    {
        global $wpdb;
        $count_chapters =
            $wpdb->get_var(
                $wpdb->prepare(
                    'SELECT count(*) as cnt FROM ' . $wpdb->prefix . 'mh_downloader_chapters WHERE status = %s',
                    'need_download'
                )
            );
        if ($count_chapters == 0) {
            return false;
        } else {
            return $count_chapters;
        }

    }

    /**
     * @return $count_issuess - how many issues we need to download.
     */
    public static function is_need_to_download_issues()
    {
        global $wpdb;
        $count_issues =
            $wpdb->get_var(
                $wpdb->prepare(
                    'SELECT count(*) as cnt FROM ' . $wpdb->prefix . 'mh_downloader_issues WHERE status = %s',
                    'need_download'
                )
            );
        if ($count_issues == 0) {
            return false;
        } else {
            return $count_issues;
        }

    }

    public static function is_need_to_download_mangas_images_from_kissmanga()
    {
        global $wpdb;
        $count_images =
            $wpdb->get_var(
                $wpdb->prepare(
                    'SELECT count(*) as cnt FROM ' . $wpdb->prefix . 'mh_downloader_mangas
					WHERE
					id_post_type_manga <> 0 AND (thumb_km_status = %s OR thumb_km_status = %s OR thumb_km_status = %s)',
                    'without_image',
                    'fail',
                    ''
                )
            );
        if ($count_images == 0) {
            return false;
        } else {
            return $count_images;
        }
    }

    public function is_need_to_download_comics_images_from_rco()
    {
        global $wpdb;
        $count_images =
            $wpdb->get_var(
                $wpdb->prepare(
                    'SELECT count(*) as cnt FROM ' . $wpdb->prefix . 'mh_downloader_comic
					WHERE
					id_post_type_comic <> 0 AND (thumb_rco_status = %s OR thumb_rco_status = %s OR thumb_rco_status = %s)',
                    'without_image',
                    'fail',
                    ''
                )
            );
        if ($count_images == 0) {
            return false;
        } else {
            return $count_images;
        }
    }

    public static function get_images_list_to_download_from_kissmanga()
    {
        global $wpdb;
        $images = $wpdb->get_results(
            $sql = $wpdb->prepare(
                'SELECT
					id, id_post_type_manga, image_url, thumb_mal_status
				FROM
					' . $wpdb->prefix . 'mh_downloader_mangas
				WHERE
				id_post_type_manga <> 0 AND ( thumb_km_status = %s OR thumb_km_status = %s OR thumb_km_status = %s )
					',
                'without_image',
                'fail',
                ''
            )
            , ARRAY_A);

        return $images;
    }

    public static function get_images_list_to_download_from_rco()
    {
        global $wpdb;
        $images = $wpdb->get_results(
            $sql = $wpdb->prepare(
                'SELECT
					id, id_post_type_comic, image_url, thumb_rco_status
				FROM
					' . $wpdb->prefix . 'mh_downloader_comic
				WHERE
				id_post_type_comic <> 0 AND ( thumb_rco_status = %s OR thumb_rco_status = %s OR thumb_rco_status = %s )
					',
                'without_image',
                'fail',
                ''
            )
            , ARRAY_A);

        return $images;
    }

    /**
     * Get list of mangas needs to download. It's mangas with status 'need_download' and 'need_update'
     * @return array - list of mangas like:
     *
     * Array
     * (
     *   [0] => Array
     *   (
     *       [id] => 1
     *       [status] => need_download
     *       [name] => Higashi no Kurume to Tonari no Meguru
     *       [url] => /Manga/Higashi-no-Kurume-to-Tonari-no-Meguru
     *   )
     * ...
     * )
     */
    public static function get_mangas_list_to_download()
    {
        global $wpdb;
        $mangas = $wpdb->get_results(
            $wpdb->prepare(
                'SELECT id, status, name, url, id_post_type_manga FROM ' . $wpdb->prefix . 'mh_downloader_mangas WHERE url=%s OR url=%s OR url=%s AND (status = %s OR status = %s) ORDER BY id',
//                '/Manga/One-Piece',
//                '/Manga/One-Piece-Digital-Colored-Comics',
                '/Manga/One-Piece-Omake',
                '/Manga/One-Piece-Party',
                '/Manga/Wanted-One-Piece',
                'need_download',
                'need_update'
            )
            , ARRAY_A);

        return $mangas;
    }

    public static function get_comics_list_to_download()
    {
        global $wpdb;
        $comics = $wpdb->get_results(
            $wpdb->prepare(
                'SELECT id, status, name, url, id_post_type_comic FROM ' . $wpdb->prefix . 'mh_downloader_comic WHERE (status = %s OR status = %s) ORDER BY id',
                'need_download',
                'need_update'
            )
            , ARRAY_A);

        return $comics;
    }

    public static function get_chapters_list_to_download()
    {
        global $wpdb;
        $chapters = $wpdb->get_results(
            $wpdb->prepare(
                'SELECT id, status, name, url, id_post_type_chapter, id_post_type_manga FROM ' . $wpdb->prefix . 'mh_downloader_chapters WHERE status = %s ORDER BY id_post_type_manga, id_post_type_chapter LIMIT 0, 10000',
                'need_download'
            )
            , ARRAY_A);

        return $chapters;
    }

    public static function get_issues_list_to_download()
    {
        global $wpdb;
        $sql = $wpdb->prepare(
            'SELECT id, status, name, url, id_post_type_issue, id_post_type_comic FROM ' . $wpdb->prefix . 'mh_downloader_issues WHERE status = %s ORDER BY id_post_type_comic, id_post_type_issue LIMIT 0, 10000',
            'need_download'
        );


        $issues = $wpdb->get_results(
            $sql
            , ARRAY_A);

        return $issues;
    }

    public static function count_comics_in_queue_with_status($status)
    {
        global $wpdb;
        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_comic WHERE STATUS = %s', $status)
        );
    }

    public static function count_issues_in_queue_with_status($status)
    {
        global $wpdb;
        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_issues WHERE STATUS = %s', $status)
        );
    }

    public static function count_downloaded_pages_from_kissmanga()
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_mangas WHERE STATUS = %s', 'downloaded')
        );
    }

    public static function count_downloaded_chapters_pages_from_kissmanga()
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_chapters WHERE STATUS = %s', 'downloaded')
        );
    }


    public static function count_need_download_pages_from_kissmanga()
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_mangas WHERE STATUS = %s', 'need_download')
        );
    }


    public static function count_need_download_chapters_pages_from_kissmanga()
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_chapters WHERE STATUS = %s', 'need_download')
        );
    }

    public static function count_downloaded_images_for_thumbnails()
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_mangas WHERE thumb_mal_status = %s', 'downloaded')
        );
    }

    public static function count_query_images_for_thumbnails()
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_mangas WHERE thumb_mal_status = %s', '')
        );
    }

    public static function count_query_without_images_for_thumbnails()
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_mangas WHERE thumb_mal_status = %s', 'without_image')
        );
    }

    public static function count_mangas_where_thumbnail_status_is($status)
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_mangas WHERE thumb_mal_status = %s', $status)
        );
    }


    public static function count_mangas_where_thumb_km_status_is($status)
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_mangas WHERE thumb_km_status = %s', $status)
        );
    }


    public static function count_comics_where_thumb_rco_status_is($status)
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_comic
			WHERE thumb_rco_status = %s AND status=%s', $status, 'downloaded')
        );
    }


    public static function count_mangas_where_thumbnail_in_queue()
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_mangas WHERE thumb_mal_status = %s AND image_url <> %s', '', '')
        );
    }

    public static function count_chapters_images_in_queue($status)
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_chapter_images WHERE status = %s', $status)
        );
    }

    public static function count_issues_images_in_queue($status)
    {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare('SELECT COUNT( * ) AS cnt FROM  ' . $wpdb->prefix . 'mh_downloader_issue_images WHERE status = %s', $status)
        );
    }

    public static function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());

        return ((float)$usec + (float)$sec);
    }

    public static function debug_with_time($msg = '')
    {
        static $n = 0;
        static $prev_time = 0;
        $n++;
        $current_time = self::microtime_float();
        $diff = $current_time - $prev_time;
        echo "[$n][" . date("Y-m-d H:i:s") . "][$diff][" . $msg . "]\n";
        $prev_time = $current_time;
    }
}
<!-- BEGIN INCLUDE PAGINATION -->
<?php if ( is_single() || is_page() || is_404() ) { ?>
<?php } else { ?>
	<?php get_template_part('includes/pagination'); ?>
<?php } ?>
<!-- END INCLUDE PAGINATION -->


	</div>
	<!-- END body -->

</div>
<!-- END #sitewrap -->

<!-- BEGIN FOOTER -->
<div id="footer">

	<!-- BEGIN WIDGETS -->
	<div class="footer-inner">
		<!-- BEGIN FULL WIDTH WIDGETS --><div class="footer-widgets-1-column"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widgets (full width)') ) : ?><?php endif; ?></div><!-- END FULL WIDTH WIDGETS -->
		<!-- BEGIN 2-COLUMN WIDGETS --><div class="footer-widgets-2-columns clear"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widgets (2 columns)') ) : ?><?php endif; ?></div><!-- END 2-COLUMN WIDGETS -->
		<!-- BEGIN 3-COLUMN WIDGETS --><div class="footer-widgets-3-columns clear"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widgets (3 columns)') ) : ?><?php endif; ?></div><!-- END 3-COLUMN WIDGETS -->
		<!-- BEGIN 4-COLUMN WIDGETS --><div class="footer-widgets-4-columns clear"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widgets (4 columns)') ) : ?><?php endif; ?></div><!-- END 4-COLUMN WIDGETS -->

		<!-- BEGIN TOP OF PAGE BUTTON -->
		<div class="top-of-page">
			<?php _e( 'TOP OF PAGE', 'bonfire' ); ?>
		</div>
		<!-- END TOP OF PAGE BUTTON -->
		
	</div>
	<!-- END WIDGETS -->

</div>
<!-- END FOOTER -->

<!-- BEGIN INSERT YOUTUBE AS BACKGROUND VIDEO -->
<script>
jQuery().ready(function() {
	jQuery('#videobackground').tubular({videoId: '<?php $videobackground = get_post_meta($post->ID, 'VideoBackground', true); ?><?php echo esc_attr($videobackground); ?>'});
});
</script>
<!-- END INSERT YOUTUBE AS BACKGROUND VIDEO -->

<!-- BEGIN FULL-SCREEN PROFILE -->
<div class="profile-wrapper">
	<div class="closeprofile">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
		<path d="M9.016,40.837c0.195,0.195,0.451,0.292,0.707,0.292c0.256,0,0.512-0.098,0.708-0.293l14.292-14.309
			l14.292,14.309c0.195,0.196,0.451,0.293,0.708,0.293c0.256,0,0.512-0.098,0.707-0.292c0.391-0.39,0.391-1.023,0.001-1.414
			L26.153,25.129L40.43,10.836c0.39-0.391,0.39-1.024-0.001-1.414c-0.392-0.391-1.024-0.391-1.414,0.001L24.722,23.732L10.43,9.423
			c-0.391-0.391-1.024-0.391-1.414-0.001c-0.391,0.39-0.391,1.023-0.001,1.414l14.276,14.293L9.015,39.423
			C8.625,39.813,8.625,40.447,9.016,40.837z"/>
		</svg>
	</div>
	<!-- BEGIN PAGE CONTENT -->
	<div class="profile-inner">
		<?php $my_query = new WP_Query('pagename=sparrow-profile'); ?>
		<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END FULL-SCREEN PROFILE -->

<!-- wp_footer -->
<?php wp_footer(); ?>
</body>
</html>
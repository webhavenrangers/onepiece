<?php
/*
Template Name: Cover
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="viewport" content="initial-scale=1.0" />
<meta charset="<?php bloginfo( 'charset' ); ?>">

<title><?php wp_title(); ?></title>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php echo bloginfo('rss2_url'); ?>">

<!-- wp_header -->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- wp_after_body -->
<?php wp_after_body(); ?>

<div id="cover-wrapper">

<!-- BEGIN HEADER (SEARCH + LOGO + MENU + BACKGROUND IMAGE + VIDEO + COLOR) -->
<?php if( get_post_meta($post->ID, 'VideoBackground', true) ) { ?>
	<!-- BEGIN CUSTOM FIELD FOR HEADER BACKGROUND VIDEO -->
	<?php if ( wp_is_mobile() ) { ?>
		<!-- BEGIN FEATURED IMAGE -->
		<?php if ( has_post_thumbnail() ) { ?>
			<div class="featured-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>" style="background-image: url(<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id,'large', true); echo esc_url($image_url[0]);  ?>);">
			</div>
		<?php } else { ?>
			<!-- BEGIN HEADER IMAGE -->
			<div class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>" style="background-image: url(<?php header_image(); ?>);"></div>
			<!-- END HEADER IMAGE -->
		<?php } ?>
		<!-- END FEATURED IMAGE -->
	<?php } else { ?>
		<div id="videobackground"></div>
	<?php } ?>
	<!-- END CUSTOM FIELD FOR HEADER BACKGROUND VIDEO -->
<?php } else { ?>
	<!-- BEGIN FEATURED IMAGE -->
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="featured-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>" style="background-image: url(<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id,'large', true); echo esc_url($image_url[0]);  ?>);">
		</div>
	<?php } else { ?>
		<!-- BEGIN HEADER IMAGE -->
		<div class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>" style="background-image: url(<?php header_image(); ?>);"></div>
		<!-- END HEADER IMAGE -->
	<?php } ?>
	<!-- END FEATURED IMAGE -->
<?php } ?>

<!-- BEGIN HEADER OVERLAY COLOR -->
<div class="header-overlay-color<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>"></div>
<!-- END HEADER OVERLAY COLOR -->

<!-- BEGIN SITE LOGO -->
<div class="site-logo-wrapper<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
	<?php if ( get_theme_mod( 'bonfire_logo' ) ) : ?>

		<!-- BEGIN LOGO IMAGE -->
		<div class="site-logo-image">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_theme_mod( 'bonfire_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
		</div>
		<!-- END LOGO IMAGE -->

	<?php else : ?>

		<!-- BEGIN LOGO -->
		<div class="site-logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php bloginfo('name'); ?>
			</a>
			
			<!-- BEGIN WORDPRESS TAGLINE -->
			<div class="tagline">
				<?php bloginfo('description'); ?>
			</div>
			<!-- END WORDPRESS TAGLINE -->
		</div>
		<!-- END LOGO -->

	<?php endif; ?>

</div>
<!-- END SITE LOGO -->

<!-- BEGIN SEARCH FORM + MENU -->
<div class="search-menu-wrapper">

	<!-- BEGIN SEARCH -->
		<!-- BEGIN SEARCH BUTTON -->
		<div id="header-search-button">
			<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
			<path id="binoculars-8-icon" d="M339.621,124.015c-4.846-13.11-17.335-22.515-32.121-22.515c-22.279,0-39.715,21.358-32.69,44.317
				C293.888,139.036,315.094,126.898,339.621,124.015z M273.167,273.167c0-9.489-7.679-17.167-17.167-17.167
				s-17.167,7.678-17.167,17.167c0,9.488,7.678,17.167,17.167,17.167S273.167,282.655,273.167,273.167z M364.465,356.82
				c25.113-5.33,42.414-25.582,36.849-55.389C425.32,327.449,398.631,369.227,364.465,356.82z M124.107,356.82
				c25.113-5.33,42.414-25.582,36.848-55.389C184.961,327.449,158.272,369.227,124.107,356.82z M204.5,101.5
				c-14.786,0-27.275,9.405-32.12,22.515c24.526,2.883,45.733,15.021,64.811,21.802C244.215,122.858,226.78,101.5,204.5,101.5z
				 M223.963,195.808c20.746,6.806,43.202,6.831,64.056,0.008c44.896-14.669,60.336-34.099,76.43-5.683
				c6.722,11.869,22.865,40.117,28.582,50.368c-48.984-9.874-95.656,24.317-101.977,73.034c-21.174-10.629-47.963-11.115-70.109,0
				c-6.328-48.734-52.975-82.899-101.985-73.034c7.1-12.674,20.301-35.733,28.583-50.36
				C163.612,161.734,179.027,181.123,223.963,195.808z M347.064,141.315c-23.906,0-49.104,15.122-69.723,21.869
				c-13.932,4.568-28.919,4.51-42.683,0c-20.62-6.748-45.809-21.869-69.723-21.869c-17.133,0-33.613,7.762-47.275,31.919
				C76.354,246.269,50,288.02,50,324.667c0,47.409,38.424,85.833,85.833,85.833c69.329,0,71.349-70.779,120.167-70.779
				c48.818,0,50.83,70.779,120.167,70.779c47.409,0,85.833-38.424,85.833-85.833c0-36.647-26.354-78.398-67.66-151.432
				C380.66,149.077,364.197,141.315,347.064,141.315z M376.167,376.167c-28.399,0-51.5-23.102-51.5-51.5
				c0-28.399,23.101-51.5,51.5-51.5c28.398,0,51.5,23.101,51.5,51.5C427.667,353.065,404.565,376.167,376.167,376.167z
				 M135.833,376.167c-28.399,0-51.5-23.102-51.5-51.5c0-28.399,23.101-51.5,51.5-51.5s51.5,23.101,51.5,51.5
				C187.333,353.065,164.232,376.167,135.833,376.167z"/>
			</svg>
		</div>
		<!-- END SEARCH BUTTON -->
		
		<!-- BEGIN SEARCH FORM -->
		<div class="header-search-background"></div>
		<div id="header-search-wrapper">			
			<?php get_search_form(); ?>
		</div>
		<!-- END SEARCH FORM -->
	<!-- END SEARCH -->
	
	<!-- BEGIN MENU -->
	<div id="main-menu">
		<?php wp_nav_menu( array( 'container_class' => 'bonfire-main-menu', 'theme_location' => 'bonfire-main-menu', 'fallback_cb' => '' ) ); ?>
	</div>
	<!-- END MENU -->
	
	<!-- BEGIN MOBILE MENU BUTTON + MENU -->
	<div class="sparrow-mobile-background">
		<div id="main-menu-mobile">
			<?php wp_nav_menu( array( 'container_class' => 'bonfire-main-menu', 'theme_location' => 'bonfire-main-menu', 'fallback_cb' => '' ) ); ?>
		</div>
	</div>
	
	<div class="sparrow-menu-button-wrapper">
		<div class="sparrow-menu-button">
			<div class="sparrow-menu-button-middle"></div>
		</div>
	</div>
	<!-- END MOBILE MENU BUTTON + MENU -->

</div>
<!-- END SEARCH FORM + MENU -->

<!-- END HEADER (SEARCH + LOGO + MENU + BACKGROUND IMAGE + VIDEO + COLOR) -->

</div>

<!-- BEGIN USER AVATAR -->
<?php if( get_option('bonfire_sparrow_gravatar_emailaddress') || get_option('bonfire_sparrow_custom_profile_image') ) { ?>
<div class="user-avatar-arrow<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>"></div>
	<div class="user-avatar<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">	
		<?php if( get_option('bonfire_sparrow_custom_profile_image') ) { ?>
			<img src="<?php echo get_option('bonfire_sparrow_custom_profile_image'); ?>">
		<?php } else { ?>
			<?php echo get_avatar( get_option('bonfire_sparrow_gravatar_emailaddress'), 200 ); ?>
		<?php } ?>
	</div>
<div class="user-avatar-menu<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
	<?php wp_nav_menu( array( 'container_class' => 'bonfire-author-menu', 'theme_location' => 'bonfire-author-menu', 'fallback_cb' => '' ) ); ?>
</div>
<?php } ?>
<!-- END USER AVATAR -->

<!-- BEGIN INSERT YOUTUBE AS BACKGROUND VIDEO -->
<script>
jQuery().ready(function() {
	jQuery('#videobackground').tubular({videoId: '<?php $videobackground = get_post_meta($post->ID, 'VideoBackground', true); ?><?php echo esc_attr($videobackground); ?>'});
});
</script>
<style>
#tubular-container,
#tubular-shield { height:100% !important; }
</style>
<!-- END INSERT YOUTUBE AS BACKGROUND VIDEO -->

<!-- BEGIN FULL-SCREEN PROFILE -->
<div class="profile-wrapper">
	<div class="closeprofile">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
		<path d="M9.016,40.837c0.195,0.195,0.451,0.292,0.707,0.292c0.256,0,0.512-0.098,0.708-0.293l14.292-14.309
			l14.292,14.309c0.195,0.196,0.451,0.293,0.708,0.293c0.256,0,0.512-0.098,0.707-0.292c0.391-0.39,0.391-1.023,0.001-1.414
			L26.153,25.129L40.43,10.836c0.39-0.391,0.39-1.024-0.001-1.414c-0.392-0.391-1.024-0.391-1.414,0.001L24.722,23.732L10.43,9.423
			c-0.391-0.391-1.024-0.391-1.414-0.001c-0.391,0.39-0.391,1.023-0.001,1.414l14.276,14.293L9.015,39.423
			C8.625,39.813,8.625,40.447,9.016,40.837z"/>
		</svg>
	</div>
	<!-- BEGIN PAGE CONTENT -->
	<div class="profile-inner">
		<?php $my_query = new WP_Query('pagename=sparrow-profile'); ?>
		<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END FULL-SCREEN PROFILE -->

<!-- wp_footer -->
<?php wp_footer(); ?>
</body>
</html>
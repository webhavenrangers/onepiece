<!-- BEGIN MOBILE MENU -->
jQuery('.sparrow-menu-button').on('touchstart click', function(e) {
'use strict';
	e.preventDefault();
		if(jQuery('.sparrow-menu-button-wrapper').hasClass('sparrow-menu-active'))
		{
			/* hide expanded menu button */
			jQuery('.sparrow-menu-button-wrapper').removeClass('sparrow-menu-active');
			/* hide mobile menu */
			jQuery('.sparrow-mobile-background').removeClass('sparrow-mobile-background-active');
		} else {
			/* show expanded menu button */
			jQuery('.sparrow-menu-button-wrapper').addClass('sparrow-menu-active');
			/* show mobile menu */
			jQuery('.sparrow-mobile-background').addClass('sparrow-mobile-background-active');
			/* if search open, hide it */
			jQuery('.user-avatar, .user-avatar-arrow, .user-avatar-menu').removeClass('user-avatar-search-active');
			jQuery('.site-logo, .site-logo-image').removeClass('site-logo-search-active');
			jQuery('#header-search-wrapper').removeClass('header-search-active');
			jQuery('.header-search-background').removeClass('header-search-background-active');
			jQuery('#header-search-wrapper .searchform-wrapper input').blur();
		}
});
<!-- END MOBILE MENU -->

<!-- BEGIN SEARCH FORM OPEN -->
jQuery('#header-search-button').on('touchstart click', function(e) {
	'use strict';
		e.preventDefault();
			jQuery('.user-avatar, .user-avatar-arrow, .user-avatar-menu').addClass('user-avatar-search-active');
			jQuery('.site-logo, .site-logo-image').addClass('site-logo-search-active');
			jQuery('#header-search-wrapper .searchform-wrapper input').focus();
			setTimeout( function() {
				jQuery('#header-search-wrapper').addClass('header-search-active');
				jQuery('.header-search-background').addClass('header-search-background-active');
			}, 150);
			/* hide expanded menu button */
			jQuery('.sparrow-menu-button-wrapper').removeClass('sparrow-menu-active');
			/* hide mobile menu */
			jQuery('.sparrow-mobile-background').removeClass('sparrow-mobile-background-active');
});
<!-- END SEARCH FORM OPEN -->
<!-- BEGIN SEARCH FORM CLOSE -->
jQuery('.search-close-icon').on('touchstart click', function(e) {
	'use strict';
		e.preventDefault();
			jQuery('.user-avatar, .user-avatar-arrow, .user-avatar-menu').removeClass('user-avatar-search-active');
			jQuery('.site-logo, .site-logo-image').removeClass('site-logo-search-active');
			jQuery('#header-search-wrapper').removeClass('header-search-active');
			jQuery('.header-search-background').removeClass('header-search-background-active');
			jQuery('#header-search-wrapper .searchform-wrapper input').blur();
});
<!-- END SEARCH FORM CLOSE -->

<!-- BEGIN USER AVATAR + AVATAR MENU -->
jQuery('.user-avatar').on('touchstart click', function(e) {
'use strict';
	e.preventDefault();
		if(jQuery('.user-avatar').hasClass('user-avatar-active'))
		{
			/* hide expanded avatar */
			jQuery('.user-avatar').removeClass('user-avatar-active');
			/* hide avatar menu */
			jQuery('.user-avatar-menu').removeClass('user-avatar-menu-active');
		} else {
			/* show expanded avatar */
			jQuery('.user-avatar').addClass('user-avatar-active');
			/* show avatar menu */
			jQuery('.user-avatar-menu').addClass('user-avatar-menu-active');
		}
});
<!-- END USER AVATAR + AVATAR MENU -->

<!-- BEGIN FULL-SCREEN PROFILE -->
jQuery('.openprofile').on('touchstart click', function(e) {
	'use strict';
		e.preventDefault();
			/* show full-screen profile */
			jQuery('.profile-wrapper').addClass('profile-wrapper-active');
			/* hide expanded avatar */
			jQuery('.user-avatar').removeClass('user-avatar-active');
			/* hide avatar menu */
			jQuery('.user-avatar-menu').removeClass('user-avatar-menu-active');
});
jQuery('.closeprofile svg').on('touchstart click', function(e) {
	'use strict';
		e.preventDefault();
			/* hide full-screen profile */
			jQuery('.profile-wrapper').removeClass('profile-wrapper-active');
});
<!-- END FULL-SCREEN PROFILE -->

<!-- BEGIN SHARE LINKS -->
jQuery('.share-wrapper svg').on('touchstart click', function(e) {
'use strict';
	e.preventDefault();
		if(jQuery('.share-links-wrapper').hasClass('share-links-wrapper-active'))
		{
			/* hide links */
			jQuery('.share-links-wrapper').removeClass('share-links-wrapper-active');
			/* enable hover tooltip */
			jQuery('.share-tooltip-text').removeClass('share-tooltip-text-inactive');
		} else {
			/* show links */
			jQuery('.share-links-wrapper').addClass('share-links-wrapper-active');
			/* disable hover tooltip */
			jQuery('.share-tooltip-text').addClass('share-tooltip-text-inactive');
		}
});
<!-- END SHARE LINKS -->

<!-- BEGIN TOP OF PAGE LINK -->
jQuery('.top-of-page').on('touchstart click', function(e) {
	'use strict';
		e.preventDefault();
			jQuery(window).scrollTo( '.search-menu-wrapper', 800 );
});
<!-- END TOP OF PAGE LINK -->

<!-- BEGIN AUTO-EXPAND TEXTAREA -->
jQuery(document).ready(function() {
	'use strict';
    if( jQuery("textarea").length!== 0)
		jQuery("textarea").autogrow();
});
<!-- END AUTO-EXPAND TEXTAREA -->
<!-- BEGIN CONVERTING DEFAULT WP MENU TO A SLIDE-DOWN ONE -->
jQuery(document).ready(function ($) {
	'use strict';
		jQuery('.menu ul').slideUp(0);

		jQuery('.user-avatar-menu .menu-item-has-children').click(function () {
			var target = jQuery(this).children('a');
			if(target.hasClass('menu-expanded')){
				target.removeClass('menu-expanded');
			}else{
				jQuery('.menu-item > a').removeClass('menu-expanded');
				target.addClass('menu-expanded');
			}
			jQuery(this).find('ul:first')
						.slideToggle(350)
						.end()
						.siblings('li')
						.find('ul')
						.slideUp(350);
		});
	
});
<!-- END CONVERTING DEFAULT WP MENU TO A SLIDE-DOWN ONE -->
<?php

///////////////////////////////////////
// You may add your custom functions here
///////////////////////////////////////

	//
	// CREATE THE SETTINGS PAGE (for WordPress backend, Settings > Sparrow theme options)
	//

	/* create the "Settings > Sparrow theme options" menu item */
	function bonfire_sparrow_admin_menu() {
		add_submenu_page('options-general.php', 'Sparrow theme options page', 'Sparrow theme options', 'administrator', 'sparrow-theme-options', 'bonfire_sparrow_page');
	}
	
	/* create the actual settings page */
	function bonfire_sparrow_page() {
		if ( isset($_POST['update_bonfire_sparrow']) == 'true' ) { bonfire_sparrow_update(); }
	?>

		<div class="wrap">
			<h2>Sparrow theme options</h2>
			<h4>Psst! Sparrow's color options can be changed under Appearance > Customize</h4>
			<br>
			
			<hr><br>

			<form method="POST" action="">
				<input type="hidden" name="update_bonfire_sparrow" value="true" />

				<!-- BEGIN SHOW EXCERPTS -->
				<h2>Excerpts</h2>
				<table class="form-table">
					<tr valign="top">
					<td>
					<label><input type="checkbox" name="sparrow_show_excerpt" id="sparrow_show_excerpt" <?php echo get_option('bonfire_sparrow_show_excerpt'); ?> /> Show excerpts on blog index (if unchecked, full posts will be shown).</label><br>
					</td>
					</tr>
				</table>
				<!-- END SHOW EXCERPTS -->
				
				<hr><br>

				<!-- BEGIN DROPPED CAPITAL DISABLE -->
				<h2>Dropped capital</h2>
				<table class="form-table">
					<tr valign="top">
					<td>
					<label><input type="checkbox" name="sparrow_disable_dropped_capital" id="sparrow_disable_dropped_capital" <?php echo get_option('bonfire_sparrow_disable_dropped_capital'); ?> /> Disable dropped capital styling on posts (first letter of first paragraph).</label><br>
					</td>
					</tr>
				</table>
				<!-- END DROPPED CAPITAL DISABLE -->

				<hr><br>

				<!-- BEGIN AUTHOR AVATAR DETAILS -->
				<h2>Author Avatar options</h2>
				<table class="form-table">
					<!-- BEGIN GRAVATAR EMAIL -->
					<tr valign="top">
					<th scope="row">Enter your Gravatar email address:</th>
					<td>
					<input type="text" name="bonfire_sparrow_gravatar_email" id="bonfire_sparrow_gravatar_email" value="<?php echo get_option('bonfire_sparrow_gravatar_emailaddress'); ?>"/>
					<p style="margin-left:5px;">To show your gravatar in the top-right corner of the site, enter your gravatar email in the field above. If no email is entered, no icon will be shown.</p>
					</td>
					</tr>
					<!-- END GRAVATAR EMAIL -->
					
					<!-- BEGIN CUSTOM PROFILE IMAGE -->
					<tr valign="top">
					<th scope="row">Enter custom profile image:</th>
					<td>
					<input type="text" name="bonfire_sparrow_custom_profile" id="bonfire_sparrow_custom_profile" value="<?php echo get_option('bonfire_sparrow_custom_profile_image'); ?>"/>
					<br> To use a custom profile image, enter its URL in the field above (overrides Gravatar). To avoid distortion, use a square image.
					</td>
					</tr>
					<!-- END CUSTOM PROFILE IMAGE -->
				</table>
				<!-- END AUTHOR AVATAR DETAILS OPTIONS-->
				
				<hr><br>
				
				<!-- BEGIN HEADER OVERLAY OPACITY -->
				<h2>Header overlay</h2>
				<table class="form-table">
					<tr valign="top">
					<th scope="row">Header overlay color opacity:</th>
					<td>
					<input style="width:45px;height:35px;" type="text" name="sparrow_header_overlay_opacity" id="sparrow_header_overlay_opacity" value="<?php echo stripslashes(get_option('bonfire_sparrow_header_overlay_opacity')); ?>"/> From 0-1. Example: 0.25 or 0.75. If left empty, defaults to 0.65.<br><br>
					</td>
					</tr>
				</table>
				<!-- END HEADER OVERLAY OPACITY -->
				
				<hr><br>
				
				<!-- BEGIN SHOW MOBILE MENU -->
				<h2>Mobile menu</h2>
				<table class="form-table">
					<tr valign="top">
					<th scope="row">Show mobile menu if screen size/resolution is lower than:</th>
					<td>
					<input style="width:45px;height:35px;" type="text" name="sparrow_mobile_menu_resolution" id="sparrow_mobile_menu_resolution" value="<?php echo stripslashes(get_option('bonfire_sparrow_mobile_menu_resolution')); ?>"/> Example: 300. If left empty, defaults to 600.<br><br>
					</td>
					</tr>
				</table>
				<!-- END SHOW MOBILE MENU -->
				
				<hr><br>

				<!-- BEGIN 'SAVE OPTIONS' BUTTON -->	
				<p><input type="submit" name="SparrowName" value="Save Options" class="button button-primary" /></p>
				<!-- END 'SAVE OPTIONS' BUTTON -->	

			</form>

		</div>
	<?php }
	function bonfire_sparrow_update() {

		/* show excerpts */
		if ( isset ($_POST['sparrow_show_excerpt'])=='on') { $display = 'checked'; } else { $display = ''; }
	    update_option('bonfire_sparrow_show_excerpt', $display);
		/* disable dropped capital */
		if ( isset ($_POST['sparrow_disable_dropped_capital'])=='on') { $display = 'checked'; } else { $display = ''; }
	    update_option('bonfire_sparrow_disable_dropped_capital', $display);
		/* enter user gravatar email address */
		update_option('bonfire_sparrow_gravatar_emailaddress',   $_POST['bonfire_sparrow_gravatar_email']);
		/* custom profile image */
		update_option('bonfire_sparrow_custom_profile_image',   $_POST['bonfire_sparrow_custom_profile']);
		/* header overlay color opacity */
		update_option('bonfire_sparrow_header_overlay_opacity',   $_POST['sparrow_header_overlay_opacity']);
		/* show mobile menu */
		update_option('bonfire_sparrow_mobile_menu_resolution',   $_POST['sparrow_mobile_menu_resolution']);
		
	}
	add_action('admin_menu', 'bonfire_sparrow_admin_menu');
	?>
<?php

	///////////////////////////////////////
	// Add color options to Appearance > Customize > Sparrow theme colors
	///////////////////////////////////////
	add_action( 'customize_register', 'sparrow_theme_colors_register' );
	function sparrow_theme_colors_register($wp_customize)
	{
		$colors = array();
		/* body background */
		$colors[] = array( 'slug'=>'sparrow_body_background', 'default' => '', 'label' => __( 'Background', 'bonfire' ) );
		/* header overlay */
		$colors[] = array( 'slug'=>'sparrow_header_overlay', 'default' => '', 'label' => __( 'Header overlay', 'bonfire' ) );
		/* dropped capital (enlarged first letter of post) */
		$colors[] = array( 'slug'=>'sparrow_dropped_capital', 'default' => '', 'label' => __( 'Dropped capital', 'bonfire' ) );
		$colors[] = array( 'slug'=>'sparrow_dropped_capital_background', 'default' => '', 'label' => __( 'Dropped capital background', 'bonfire' ) );

	foreach($colors as $color)
	{

	/* create custom color customization section */
	$wp_customize->add_section( 'sparrow_theme_colors' , array( 'title' => __('Sparrow theme colors', 'bonfire'), 'priority' => 42 ));
	$wp_customize->add_setting( $color['slug'], array( 'default' => $color['default'], 'type' => 'option', 'capability' => 'edit_theme_options', 'sanitize_callback' => 'esc_url_raw' ));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $color['slug'], array( 'label' => $color['label'], 'section' => 'sparrow_theme_colors', 'settings' => $color['slug'] )));
	}
	}

	//
	// Insert color customization options into the header
	//
	
	function smash_theme_customize() {
	?>

		<!-- BEGIN CUSTOM COLORS (WP THEME CUSTOMIZER) -->
		<?php $sparrow_body_background = get_option('sparrow_body_background'); ?>
		
		<?php $sparrow_header_overlay = get_option('sparrow_header_overlay'); ?>

		<?php $sparrow_dropped_capital = get_option('sparrow_dropped_capital'); ?>
		<?php $sparrow_dropped_capital_background = get_option('sparrow_dropped_capital_background'); ?>
		<style>
		<?php if( get_option('bonfire_sparrow_disable_dropped_capital') ) { ?>
		<?php } else { ?>
		/* dropped capital styling */
		.content-wrapper-single .entry-content p:first-child:first-letter {
			font-size:40px;
			font-weight:700;
			color:#F4F4F5;
			padding:19px 10px 12px 10px;
			margin:5px 10px 0 0;
			float:left;
			background-color:#F5B712;
			border-radius:1px;
			
			text-shadow:1px 1px 0px rgba(0,0,0,0.17);
		}
		/* dropped capital Firefox-specific bottom padding */
		@-moz-document url-prefix() { .content-wrapper-single .entry-content p:first-child:first-letter {padding-bottom:16px;} }
		/* disable dropped capital for blockquote */
		.content-wrapper-single .entry-content blockquote p:first-child:first-letter {
			font-family:'Merriweather',arial,tahoma,verdana;
			font-weight:300;
			font-size:30px;
			line-height:40px;
			color:#7E8689;
			padding:0;
			margin:7px 0 0 0;
			text-shadow:none;
			background-color:transparent;
		}
		<?php } ?>
		
		/* body background */
/*		body, #respond #cancel-comment-reply-link { background-color:*/<?php //echo esc_attr($sparrow_body_background); ?>/*; }*/
		/* header overlay */
		.header-overlay-color,
		.header-search-background,
		.sparrow-mobile-background { background-color:<?php echo esc_attr($sparrow_header_overlay); ?>; }
		/* dropped capital */
		.content-wrapper-single .entry-content p:first-child:first-letter { color:<?php echo esc_attr($sparrow_dropped_capital); ?>; background-color:<?php echo esc_attr($sparrow_dropped_capital_background); ?>; }
		/* header overlay opacity */
		.header-overlay-color { opacity:<?php echo get_option('bonfire_sparrow_header_overlay_opacity'); ?>; }
		
		<?php if ( is_admin_bar_showing() ) { ?>
		/* if WP toolbar active, move background video container slightly lower */
			#tubular-container,
			#tubular-shield { margin-top:32px; }
			@media screen and (max-width: 782px) {
				#tubular-container,
				#tubular-shield { margin-top:46px; }
			}
		<?php } ?>
		
		/* show mobile menu if certain resolution set */
		@media screen and (max-width: <?php if( get_option('bonfire_sparrow_mobile_menu_resolution') ) { ?><?php echo get_option('bonfire_sparrow_mobile_menu_resolution'); ?><?php } else { ?>600<?php } ?>px) {
			#main-menu { display:none; }
			.sparrow-mobile-background,
			.sparrow-menu-button-wrapper { display:inline; }
		}
		</style>
		<!-- END CUSTOM COLORS (WP THEME CUSTOMIZER) -->
	
	<?php
	}
	add_action('wp_head','smash_theme_customize');

	///////////////////////////////////////
	// WordPress theme options: Custom logo upload
	///////////////////////////////////////
	function bonfire_theme_customizer( $wp_customize ) {
	
	$wp_customize->add_section( 'bonfire_logo_section' , array(
		'title'       => __( 'Logo', 'bonfire' ),
		'priority'    => 30,
		'description' => 'Upload a logo to replace the default site name and description in the header',
	) );
	
	$wp_customize->add_setting( 'bonfire_logo',
    array ( 'default' => '',
    'sanitize_callback' => 'esc_url_raw'
    ));
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bonfire_logo', array(
		'label'    => __( 'Logo', 'bonfire' ),
		'section'  => 'bonfire_logo_section',
		'settings' => 'bonfire_logo',
	) ) );
	
	}
	add_action('customize_register', 'bonfire_theme_customizer');

	///////////////////////////////////////
	// Add hook after opening body tag
	///////////////////////////////////////
	function wp_after_body() {  
		do_action('wp_after_body');
	}

	///////////////////////////////////////
	// Enable custom header image
	///////////////////////////////////////
	add_theme_support('custom-header');

	///////////////////////////////////////
	// Enable featured image
	///////////////////////////////////////
	add_theme_support('post-thumbnails');

	///////////////////////////////////////
	// Add default posts and comments RSS feed links to head
	///////////////////////////////////////
	add_theme_support('automatic-feed-links');
	
	///////////////////////////////////////
	// Styles the visual editor with editor-style.css to match the theme style
	///////////////////////////////////////
	add_editor_style();

	///////////////////////////////////////
	// Load theme languages
	///////////////////////////////////////
	load_theme_textdomain( 'bonfire', get_template_directory().'/languages' );

	///////////////////////////////////////
	// Custom Excerpt Length
	///////////////////////////////////////
	function custom_excerpt_length( $length ) {
		return 100;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	///////////////////////////////////////
	// Register Custom Menu Function
	///////////////////////////////////////
	if (function_exists('register_nav_menus')) {
		register_nav_menus( array(
			'bonfire-main-menu' => ( 'Bonfire Main Menu' ),
			'bonfire-author-menu' => ( 'Bonfire Author Menu' ),
		) );
	}

	///////////////////////////////////////
	// Default Main Nav Function
	///////////////////////////////////////
	function default_main_nav() {
		echo '<ul id="main-nav" class="main-nav clearfix">';
		wp_list_pages('title_li=');
		echo '</ul>';
	}
	
	///////////////////////////////////////
	// Register Widgets
	///////////////////////////////////////
	function sparrow_widgets_init() {
	
		register_sidebar( array(
		'name' => __( 'Footer Widgets (full width)', 'bonfire' ),
		'id' => 'footer-widgets-full',
		'description' => __( 'Drag widgets here', 'bonfire' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
		));
		
		register_sidebar( array(
		'name' => __( 'Footer Widgets (2 columns)', 'bonfire' ),
		'id' => 'footer-widgets-2-columns',
		'description' => __( 'Drag widgets here', 'bonfire' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
		));
		
		register_sidebar( array(
		'name' => __( 'Footer Widgets (3 columns)', 'bonfire' ),
		'id' => 'footer-widgets-3-columns',
		'description' => __( 'Drag widgets here', 'bonfire' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
		));
		
		register_sidebar( array(
		'name' => __( 'Footer Widgets (4 columns)', 'bonfire' ),
		'id' => 'footer-widgets-4-columns',
		'description' => __( 'Drag widgets here', 'bonfire' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
		));

	}
	add_action( 'widgets_init', 'sparrow_widgets_init' );

	///////////////////////////////////////
	//Custom titles
	///////////////////////////////////////
	add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );
	function baw_hack_wp_title_for_home( $title )
	{
	  if( empty( $title ) && ( is_home() || is_front_page() ) ) {
	  	echo bloginfo('name') . ' | ' . get_bloginfo( 'description' );
	  }
	  return $title;
	}

	///////////////////////////////////////
	// Exclude pages from search results
	///////////////////////////////////////	
	function SearchFilter($query) {
	if ($query->is_search) {
	$query->set('post_type', 'post');
	}
	return $query;
	}

//	add_filter('pre_get_posts','SearchFilter');

	///////////////////////////////////////
	// ENQUEUE font-awesome.min.css
	///////////////////////////////////////
	function bonfire_fontawesome() {
		wp_register_style( 'fontawesome', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.min.css', array(), '1', 'all' );
		wp_enqueue_style( 'fontawesome' );
	}
	add_action( 'wp_enqueue_scripts', 'bonfire_fontawesome' );

	///////////////////////////////////////
	// Enqueue Google WebFonts
	///////////////////////////////////////
	function bonfire_fonts() {
	$protocol = is_ssl() ? 'https' : 'http';
	wp_enqueue_style( 'bonfire-fonts', "$protocol://fonts.googleapis.com/css?family=Quantico:400,700|Titillium+Web:400,700|Merriweather:300,700|Open+Sans:400,600|Dosis:400|Roboto:300' rel='stylesheet' type='text/css" );}
	add_action( 'wp_enqueue_scripts', 'bonfire_fonts' );



	///////////////////////////////////////
	// Enqueue style.css (default WordPress stylesheet)
	///////////////////////////////////////
	function bonfire_style() {  
		wp_register_style( 'style', get_stylesheet_uri() , array(), '1', 'all' );

		wp_enqueue_style( 'style' );  
	}
	add_action( 'wp_enqueue_scripts', 'bonfire_style' );

	///////////////////////////////////////
	// Enqueue accordion.js
	///////////////////////////////////////
    function sparrow_accordion() {  
		wp_register_script( 'sparrow-accordion', get_template_directory_uri() . '/js/accordion.js',  array( 'jquery' ), '1' );  
		wp_enqueue_script( 'sparrow-accordion' );
	}
	add_action( 'wp_enqueue_scripts', 'sparrow_accordion' );
	
	///////////////////////////////////////
	// Enqueue misc-footer.js
	///////////////////////////////////////
    function bonfire_miscfooter() {  
		wp_register_script( 'misc-footer', get_template_directory_uri() . '/js/misc-footer.js',  array( 'jquery' ), '1', true );  
		wp_enqueue_script( 'misc-footer' );
	}
	add_action( 'wp_enqueue_scripts', 'bonfire_miscfooter' );
	
	///////////////////////////////////////
	// Enqueue jquery.tubular.1.0.js
	///////////////////////////////////////
    function bonfire_tubular() {  
		wp_register_script( 'tubular', get_template_directory_uri() . '/js/jquery.tubular.1.0.js',  array( 'jquery' ), '1', true );  
		wp_enqueue_script( 'tubular' );
	}
	add_action( 'wp_enqueue_scripts', 'bonfire_tubular' );

	///////////////////////////////////////
	// Enqueue autogrow/jquery.autogrow-textarea.js
	///////////////////////////////////////
    function bonfire_autogrow() {
		if ( is_singular() ) {
		wp_register_script( 'autogrow', get_template_directory_uri() . '/js/autogrow/jquery.autogrow-textarea.js',  array( 'jquery' ), '1', true );  

		wp_enqueue_script( 'autogrow' );  
	}
	}
	add_action( 'wp_enqueue_scripts', 'bonfire_autogrow' );

	///////////////////////////////////////
	// Enqueue comment-form.js
	///////////////////////////////////////
	function bonfire_commentform() {  
		if ( is_single() ) {
		wp_register_script( 'comment-form', get_template_directory_uri() . '/js/comment-form.js',  array( 'jquery' ), '1', true );  

		wp_enqueue_script( 'comment-form' );  
	}
	}
	add_action( 'wp_enqueue_scripts', 'bonfire_commentform' );

	///////////////////////////////////////
	// Enqueue jquery.scrollTo-min.js (smooth scrolling to anchors)
	///////////////////////////////////////
    function bonfire_scrollto() {  
		wp_register_script( 'scroll-to', get_template_directory_uri() . '/js/jquery.scrollTo-min.js',  array( 'jquery' ), '1', true );  

		wp_enqueue_script( 'scroll-to' );
	}
	add_action( 'wp_enqueue_scripts', 'bonfire_scrollto' ); 

	///////////////////////////////////////
	// Enqueue comment-reply.js (threaded comments)
	///////////////////////////////////////
	function bonfire_comment_reply(){
		if ( is_singular() && get_option( 'thread_comments' ) )	wp_enqueue_script( 'comment-reply' );
	}
	add_action('wp_print_scripts', 'bonfire_comment_reply');
	
	///////////////////////////////////////
	// Define content width
	///////////////////////////////////////
	if ( ! isset( $content_width ) ) $content_width = 1000;

	///////////////////////////////////////
	// Breadcrumbs
	///////////////////////////////////////
	function bonfire_breadcrumbs() {
		echo '<ul>';
		echo '<li><a href="';
		echo esc_url(home_url());
		echo '">';
		echo 'Home';
		echo "</a></li>";
		if (is_category() || is_single()) {
			echo '<li>';
			the_category(' </li><li> ');
			if (is_single()) {
				echo "</li><li>";
				the_title();
				echo '</li>';
			}
		} elseif (is_page()) {
			echo '<li>';
			echo the_title();
			echo '</li>';
		}

	elseif (is_tag()) {echo'<li>'; single_tag_title(); echo'</li>'; }
	elseif (is_day()) {echo"<li>"; esc_attr(the_time('F jS, Y')); echo'</li>';}
	elseif (is_month()) {echo"<li>"; esc_attr(the_time('F, Y')); echo'</li>';}
	elseif (is_year()) {echo"<li>"; esc_attr(the_time('Y')); echo'</li>';}
	elseif (is_author()) {echo"<li>"; the_author(); echo'</li>';}
	elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
	elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
	echo '</ul>';
	}

	///////////////////////////////////////
	// Custom Comment Output
	///////////////////////////////////////
	function custom_theme_comment($comment, $args, $depth) {
	   $GLOBALS['comment'] = $comment; 
	   ?>

		<li id="comment-<?php comment_ID() ?>" <?php comment_class(); ?>>
		
			<div class="comment-avatar">
				<?php echo get_avatar($comment,$size='40'); ?>
				<span class="comment-author"><?php printf(get_comment_author_link()) ?></span>
			</div>
			
			<div class="comment-container">

				<div class="comment-entry">
					<?php if ($comment->comment_approved == '0') : ?>
					<strong><?php _e('(Your comment is awaiting moderation.)', 'bonfire') ?></strong>
					<?php endif; ?>
					<?php echo get_comment_text(); ?>
					<?php edit_comment_link( __('Edit', 'bonfire')) ?>
				</div>

				<div class="comment-reply">
					<?php comment_reply_link(array_merge( $args, array('add_below' => 'comment', 'depth' => $depth, 'reply_text' => __( 'REPLY', 'bonfire' ), 'max_depth' => $args['max_depth']))) ?>
				</div>
			
			</div>
		
	<?php
	}

?>
<!-- BEGIN INCLUDE PAGINATION -->
<?php
global $post;
if ( is_single() || is_page() || is_404() ) { ?>
<?php } else { ?>
	<?php // get_template_part( 'includes/pagination' ); ?>
<?php } ?>
<!-- END INCLUDE PAGINATION -->


</div>
<!-- END body -->

</div>
<!-- END #sitewrap -->

<!-- BEGIN FOOTER -->
<div id="footer">

	<!-- BEGIN WIDGETS -->
	<div class="footer-inner">
		<!-- BEGIN FULL WIDTH WIDGETS -->
		<div class="footer-widgets-1-column"><?php if ( ! function_exists( 'dynamic_sidebar' ) ||
		                                                ! dynamic_sidebar( 'Footer Widgets (full width)' )
			) : ?><?php endif; ?></div><!-- END FULL WIDTH WIDGETS -->
		<!-- BEGIN 2-COLUMN WIDGETS -->
		<div class="footer-widgets-2-columns clear"><?php if ( ! function_exists( 'dynamic_sidebar' ) ||
		                                                       ! dynamic_sidebar( 'Footer Widgets (2 columns)' )
			) : ?><?php endif; ?></div><!-- END 2-COLUMN WIDGETS -->
		<!-- BEGIN 3-COLUMN WIDGETS -->
		<div class="footer-widgets-3-columns clear"><?php if ( ! function_exists( 'dynamic_sidebar' ) ||
		                                                       ! dynamic_sidebar( 'Footer Widgets (3 columns)' )
			) : ?><?php endif; ?></div><!-- END 3-COLUMN WIDGETS -->
		<!-- BEGIN 4-COLUMN WIDGETS -->
		<div class="footer-widgets-4-columns clear"><?php if ( ! function_exists( 'dynamic_sidebar' ) ||
		                                                       ! dynamic_sidebar( 'Footer Widgets (4 columns)' )
			) : ?><?php endif; ?></div><!-- END 4-COLUMN WIDGETS -->



	</div>
	<!-- END WIDGETS -->

</div>
<!-- END FOOTER -->

<?php video_background(); ?>



<?php full_screen_profile();?>
<div id="toTop"></div>
<!-- wp_footer -->
<?php wp_footer(); ?>
</body>
</html>
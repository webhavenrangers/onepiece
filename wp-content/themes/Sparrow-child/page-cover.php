<?php
/*
Template Name: Cover
*/
global $post;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="viewport" content="initial-scale=1.0" />
<meta charset="<?php bloginfo( 'charset' ); ?>">

<title><?php wp_title(); ?></title>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php echo bloginfo('rss2_url'); ?>">

<!-- wp_header -->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- wp_after_body -->
<?php wp_after_body(); ?>

<div id="cover-wrapper">

<!-- BEGIN HEADER (SEARCH + LOGO + MENU + BACKGROUND IMAGE + VIDEO + COLOR) -->
<?php if( get_post_meta($post->ID, 'VideoBackground', true) ) { ?>
	<!-- BEGIN CUSTOM FIELD FOR HEADER BACKGROUND VIDEO -->
	<?php if ( wp_is_mobile() ) { ?>
		<!-- BEGIN FEATURED IMAGE -->
		<?php if ( has_post_thumbnail() ) { ?>
			<div class="featured-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>" style="background-image: url(<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id,'large', true); echo esc_url($image_url[0]);  ?>);">
			</div>
		<?php } else { ?>
			<!-- BEGIN HEADER IMAGE -->
			<div class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>" style="background-image: url(<?php header_image(); ?>);"></div>
			<!-- END HEADER IMAGE -->
		<?php } ?>
		<!-- END FEATURED IMAGE -->
	<?php } else { ?>
		<div id="videobackground"></div>
	<?php } ?>
	<!-- END CUSTOM FIELD FOR HEADER BACKGROUND VIDEO -->
<?php } else { ?>
	<!-- BEGIN FEATURED IMAGE -->
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="featured-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>" style="background-image: url(<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id,'large', true); echo esc_url($image_url[0]);  ?>);">
		</div>
	<?php } else { ?>
		<!-- BEGIN HEADER IMAGE -->
		<div class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>" style="background-image: url(<?php header_image(); ?>);"></div>
		<!-- END HEADER IMAGE -->
	<?php } ?>
	<!-- END FEATURED IMAGE -->
<?php } ?>

<!-- BEGIN HEADER OVERLAY COLOR -->
<div class="header-overlay-color<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>"></div>
<!-- END HEADER OVERLAY COLOR -->

<!-- BEGIN SITE LOGO -->
<div class="site-logo-wrapper<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
	<?php if ( get_theme_mod( 'bonfire_logo' ) ) : ?>

		<!-- BEGIN LOGO IMAGE -->
		<div class="site-logo-image">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_theme_mod( 'bonfire_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
		</div>
		<!-- END LOGO IMAGE -->

	<?php else :

		site_logo();

	endif; ?>

</div>
<!-- END SITE LOGO -->

<?php serch_menu_and_form();?>

<!-- END HEADER (SEARCH + LOGO + MENU + BACKGROUND IMAGE + VIDEO + COLOR) -->

</div>

<?php user_avatar();?>

<?php video_background() ?>

<?php full_screen_profile();?>

<!-- wp_footer -->
<?php wp_footer(); ?>
</body>
</html>
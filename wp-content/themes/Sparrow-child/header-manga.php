<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="viewport" content="initial-scale=1.0"/>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS Feed"
	      href="<?php echo bloginfo( 'rss2_url' ); ?>">

	<!-- wp_header -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- wp_after_body -->
<?php wp_after_body(); ?>

<!-- BEGIN HEADER (SEARCH + LOGO + MENU + BACKGROUND IMAGE + VIDEO + COLOR) -->


	<?php if ( get_post_meta( $post->ID, 'VideoBackground', true ) ) { ?>
		<!-- BEGIN CUSTOM FIELD FOR HEADER BACKGROUND VIDEO -->
		<div id="videobackground"></div>
		<!-- END CUSTOM FIELD FOR HEADER BACKGROUND VIDEO -->
	<?php } else { ?>
		<!-- BEGIN HEADER IMAGE -->
		<div class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>"
		     style="background-image: url(<?php header_image(); ?>);"></div>
		<!-- END HEADER IMAGE -->
	<?php } ?>


<!-- BEGIN HEADER OVERLAY COLOR -->
<div class="header-overlay-color<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>"></div>
<!-- END HEADER OVERLAY COLOR -->

<!-- BEGIN SITE LOGO -->
<div class="site-logo-wrapper<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
	<div class="site-logo single_manga">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-2 col-sm-12 post_image">
					<?php echo get_the_post_thumbnail()?>
				</div>
				<div class="col-lg-9 col-md-10 hidden-sm hidden-xs post_content">
					<?php echo get_manga_extradata($post); ?>
				</div>
			</div>
		</div>

	</div>


</div>
<!-- END SITE LOGO -->

<!-- BEGIN SEARCH FORM + MENU -->
<div class="search-menu-wrapper">

	<!-- BEGIN SEARCH -->
	<?php search_button(); ?>

	<!-- BEGIN SEARCH FORM -->
	<div class="header-search-background"></div>
	<div id="header-search-wrapper">
		<?php get_search_form(); ?>
	</div>
	<!-- END SEARCH FORM -->
	<!-- END SEARCH -->

	<!-- BEGIN MENU -->
	<div id="main-menu">
		<?php wp_nav_menu( array(
			'container_class' => 'bonfire-main-menu',
			'theme_location'  => 'bonfire-main-menu',
			'fallback_cb'     => ''
		) ); ?>
	</div>
	<!-- END MENU -->

	<!-- BEGIN MOBILE MENU BUTTON + MENU -->
	<div class="sparrow-mobile-background">
		<div id="main-menu-mobile">
			<?php wp_nav_menu( array(
				'container_class' => 'bonfire-main-menu',
				'theme_location'  => 'bonfire-main-menu',
				'fallback_cb'     => ''
			) ); ?>
		</div>
	</div>

	<div class="sparrow-menu-button-wrapper">
		<div class="sparrow-menu-button">
			<div class="sparrow-menu-button-middle"></div>
		</div>
	</div>
	<!-- END MOBILE MENU BUTTON + MENU -->

</div>
<!-- END SEARCH FORM + MENU -->

<!-- END HEADER (SEARCH + LOGO + MENU + BACKGROUND IMAGE + VIDEO + COLOR) -->

<?php user_avatar(); ?>

<!-- BEGIN BREADCRUMBS -->

<?php if ( function_exists( 'breadcrumb_trail' ) ) {
	$args = array(
		'labels' => array(
			'browse' => esc_html__( 'You are here: ', 'breadcrumb-trail' ),
		)
	);
	breadcrumb_trail( $args );
} ?>
<!-- END BREADCRUMBS -->

<div id="sitewrap">

	<div id="body" class="pagewidth clearfix">
/**
 * Created by kdv on 18.05.15.
 */
(function ($) {
	$( document ).ready( function () {
		var body = $( 'body' );
		/* Movies and anime lists process */
		if ( $( '#mh-meta-box-mangas-data' ).length !== 0  ) {

			$( '.myal_fetch' ).click( function () {
				var url_to_fetch = $( '#myanimelist_link' ).val();
				fetch_data_from_myal( url_to_fetch );
			} );

			function fetch_data_from_myal(url) {
				$.ajax( {
					url: ajaxurl,
					type: 'POST',
					data: {
						action: 'fetch_data_from_myal',
						url: url
					},
					beforeSend: function () {
						$( '.myal_fetch .spinner' ).show();
					},
					complete: function () {
						$( '.myal_fetch .spinner' ).hide();
					},
					success: function (response) {
						if ( typeof(response) == null || response === 'null' ) {
							$( '.myal_fetch .spinner' ).hide();
							alert( 'MAL fetch failed' );
						}
						var post_id 	= $( 'input#post_ID' ).val();
						var obj 		= $.parseJSON( response );
						var myal_id 	= obj.id;
						var title 		= obj.title;
						var image 		= obj.image;//url to image
						var myal_rating = obj.score;
						var status 		= obj.status;
						var synopsis 	= obj.synopsis;//post content
						var aired_date 	= obj.start_date;//movie or series

						/* SET VALUES TO POST FORM */
						if ( url.indexOf( 'myanimelist.net' ) === -1 )
							var myal_link = 'http://myanimelist.net/manga/'+ myal_id + '/' + url;
						else
							var myal_link = url;
						$( 'input#myanimelist_link' ).val().indexOf( 'myanimelist.net' ) === -1 || $( 'input#myanimelist_link' ).val() === '' ?$( 'input#myanimelist_link' ).val( myal_link ) : '';
						$( 'input#myal_id' ).val( myal_id );
						$( 'input#title' ).val() === '' ? $( 'input#title' ).val( title ) : '';
						$( 'input#aa_aired_date' ).val() === '' ? $( 'input#aa_aired_date' ).val( aired_date ) : '';
						$( 'input#ah_anime_status' ).val() === '' ? $( 'input#ah_anime_status' ).val( status ) : '';
						$( 'input#ah_anime_rating' ).val() === '' ? $( 'input#ah_anime_rating' ).val( myal_rating ) : '';
						//$( 'input#ah_anime_count' ).val() === '' ? $( 'input#ah_anime_count' ).val( episodes ) : '';
						$( 'input#new-tag-post_tag' ).val() === '' ? $( 'input#new-tag-post_tag' ).val( myal_rating.substr( 0, 1 ) + ' stars, ' + aired_date.substr( 0, 4 ) ) : '';
						if ( $( '.attachment-full' ).attr( 'src' ) === '' )
							set_feutered_image_for_post( post_id, image );
						/* Correct paste synopsis */
						if ( $( '#ah_anime_synopsis' ).length === 0 ) {
							if ( $( 'textarea#content' ).attr( 'aria-hidden' ) == 'true' && tinyMCE.activeEditor.getContent() === '' ) {
								tinyMCE.activeEditor.setContent( synopsis );
							} else {
								$( 'textarea#content' ).val() === '' ? $( 'textarea#content' ).val( synopsis ) : '';
							}
						} else {
							$( 'textarea#ah_anime_synopsis' ).val() === '' ? $( 'textarea#ah_anime_synopsis' ).val( synopsis ) : '';
						}

						if ( typeof obj.synonyms == "string" ) {
							var synonyms =  obj.synonyms.replace( obj.title, '' );
							$( 'input#ah_alt_title' ).val() === '' ? $( 'input#ah_alt_title' ).val( synonyms ) : '';
						}
					},
					error: function (response) {
						$( '.myal_fetch .spinner' ).hide();
					}
				} );
			}

			function set_feutered_image_for_post( post_id, image ) {
				$.ajax( {
					url: ajaxurl,
					type: 'POST',
					data: {
						action: 'set_feutered_image_from_myal',
						post_id: post_id,
						image_url: image
					},
					beforeSend: function () {
						$( '[value="Fetch"].spinner' ).show();
					},
					complete: function () {
						$( '[value="Fetch"].spinner' ).hide();
					},
					success: function (response) {
						$( 'a#set-post-thumbnail' ).html( response );
					},
					error: function (response) {
						$( '[value="Fetch"].spinner' ).hide();
					}
				} );
			}
		}



		$('.fetch_urls').click(function(){
			var textarea = $('.generate_batch'),
			text = textarea.val().replace('lstImages.push("', '');
			text = text.replace('"', '');
			$.ajax({
				url: ajaxurl,
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'fetch_images_for_chapter',
					raw_data: text,
					post_id: $( 'input#post_ID' ).val()
				},
				beforeSend: function () {

				},
				complete: function () {

				},
				success: function (response) {

				}
			});
		})




	} );


})( jQuery );
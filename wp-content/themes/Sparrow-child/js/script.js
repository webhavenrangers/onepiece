/**
 * Created by Phantom on 27.06.2016.
 */
(function ($) {
    $(document).ready(function () {
        opm_to_top();



        var cards_wrapper = $('.fixed-wrapper__cards-inner');
        var menus_height = document.getElementsByClassName('sparrow-menu-button-wrapper')[0].offsetHeight + document.getElementById('main-menu').offsetHeight;




        var bottom_offset = window.innerHeight - document.getElementById('main-menu').offsetTop;
        if( bottom_offset ==  window.innerHeight )
            bottom_offset = window.innerHeight - document.getElementsByClassName('sparrow-menu-button-wrapper')[0].offsetTop;
        cards_wrapper.animate({'height':window.innerHeight-menus_height-document.getElementsByClassName('user-avatar')[0].offsetHeight - bottom_offset },function(){
            cards_wrapper.mCustomScrollbar();
            init()});

        /* AJAX HEADER IMAGE */

        var header_image = $('[data-header_image="true"]');
        if(header_image.length !== 0 ) {
            $.ajax({
                url:'/wp-admin/admin-ajax.php',
                type: 'POST',
                //dataType: 'json',
                data:{
                    action:'get_cover_wrapper_image'
                },
                success: function(response) {
                    header_image.find('.fa-spinner').remove();
                    header_image.css('background-image', 'url('+response+')');
                    header_image.css('opacity', 1);
                }
            })
        }


        /* AJAX SEARCH */
        var search_input = $('.sa_search input#s');
        var sa_wrapper = $('.sa_wrapper');
        var sa_start = true;
        var search_query = search_input.val();
        search_input.keyup(function (e) {
            if (sa_start === true && $(this).val().length > 1) {
                get_sa(this.value);
            }
            if (!this.value) {
                sa_wrapper.hide().html('');
            }
            search_query = this.value;
        });
        sa_wrapper.on('click', '.sa_more', function () {
            $('.sa_search form').submit();
        });

        $("body > div:not('.sa_search')").click(function (e) {
            sa_wrapper.hide();
            $('.header-search-active').removeClass('pinned');
        });

        function get_sa(keyword) {
            var sa_wrapper = $('.sa_wrapper');
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'search_ajax',
                    keyword: keyword
                },
                beforeSend: function () {
                    $('.search-close-icon').toggleClass('spin');
                    sa_start = false;
                },
                success: function (response) {
                    $('.search-close-icon').toggleClass('spin');
                    sa_start = true;
                    if (response != '') {
                        sa_wrapper.html(response).slideDown('fast');
                        $('.header-search-active').addClass('pinned');
                    } else {
                        sa_wrapper.html('').slideUp('fast');
                        $('.header-search-active').removeClass('pinned');
                    }

                    if (keyword !== search_query) {
                        get_sa(search_query)
                    }
                },
                error: function (response) {
                    sa_start = true;
                    $('.sa_spin').hide();
                }
            });
        }


            var tiltSettings = [
                {},
                {
                    movement: {
                        imgWrapper : {
                            translation : {x: 10, y: 10, z: 30},
                            rotation : {x: 0, y: -10, z: 0},
                            reverseAnimation : {duration : 200, easing : 'easeOutQuad'}
                        },
                        lines : {
                            translation : {x: 10, y: 10, z: [0,70]},
                            rotation : {x: 0, y: 0, z: -2},
                            reverseAnimation : {duration : 2000, easing : 'easeOutExpo'}
                        },
                        caption : {
                            rotation : {x: 0, y: 0, z: 2},
                            reverseAnimation : {duration : 200, easing : 'easeOutQuad'}
                        },
                        overlay : {
                            translation : {x: 10, y: -10, z: 0},
                            rotation : {x: 0, y: 0, z: 2},
                            reverseAnimation : {duration : 2000, easing : 'easeOutExpo'}
                        },
                        shine : {
                            translation : {x: 100, y: 100, z: 0},
                            reverseAnimation : {duration : 200, easing : 'easeOutQuad'}
                        }
                    }
                },
                {
                    movement: {
                        imgWrapper : {
                            rotation : {x: -5, y: 10, z: 0},
                            reverseAnimation : {duration : 900, easing : 'easeOutCubic'}
                        },
                        caption : {
                            translation : {x: 30, y: 30, z: [0,40]},
                            rotation : {x: [0,15], y: 0, z: 0},
                            reverseAnimation : {duration : 1200, easing : 'easeOutExpo'}
                        },
                        overlay : {
                            translation : {x: 10, y: 10, z: [0,20]},
                            reverseAnimation : {duration : 1000, easing : 'easeOutExpo'}
                        },
                        shine : {
                            translation : {x: 100, y: 100, z: 0},
                            reverseAnimation : {duration : 900, easing : 'easeOutCubic'}
                        }
                    }
                },
                {
                    movement: {
                        imgWrapper : {
                            rotation : {x: -5, y: 10, z: 0},
                            reverseAnimation : {duration : 50, easing : 'easeOutQuad'}
                        },
                        caption : {
                            translation : {x: 20, y: 20, z: 0},
                            reverseAnimation : {duration : 200, easing : 'easeOutQuad'}
                        },
                        overlay : {
                            translation : {x: 5, y: -5, z: 0},
                            rotation : {x: 0, y: 0, z: 6},
                            reverseAnimation : {duration : 1000, easing : 'easeOutQuad'}
                        },
                        shine : {
                            translation : {x: 50, y: 50, z: 0},
                            reverseAnimation : {duration : 50, easing : 'easeOutQuad'}
                        }
                    }
                },
                {
                    movement: {
                        imgWrapper : {
                            translation : {x: 0, y: -8, z: 0},
                            rotation : {x: 3, y: 3, z: 0},
                            reverseAnimation : {duration : 1200, easing : 'easeOutExpo'}
                        },
                        lines : {
                            translation : {x: 15, y: 15, z: [0,15]},
                            reverseAnimation : {duration : 1200, easing : 'easeOutExpo'}
                        },
                        overlay : {
                            translation : {x: 0, y: 8, z: 0},
                            reverseAnimation : {duration : 600, easing : 'easeOutExpo'}
                        },
                        caption : {
                            translation : {x: 10, y: -15, z: 0},
                            reverseAnimation : {duration : 900, easing : 'easeOutExpo'}
                        },
                        shine : {
                            translation : {x: 50, y: 50, z: 0},
                            reverseAnimation : {duration : 1200, easing : 'easeOutExpo'}
                        }
                    }
                },
                {
                    movement: {
                        lines : {
                            translation : {x: -5, y: 5, z: 0},
                            reverseAnimation : {duration : 1000, easing : 'easeOutExpo'}
                        },
                        caption : {
                            translation : {x: 15, y: 15, z: 0},
                            rotation : {x: 0, y: 0, z: 3},
                            reverseAnimation : {duration : 1500, easing : 'easeOutElastic', elasticity : 700}
                        },
                        overlay : {
                            translation : {x: 15, y: -15, z: 0},
                            reverseAnimation : {duration : 500,easing : 'easeOutExpo'}
                        },
                        shine : {
                            translation : {x: 50, y: 50, z: 0},
                            reverseAnimation : {duration : 500, easing : 'easeOutExpo'}
                        }
                    }
                },
                {
                    movement: {
                        imgWrapper : {
                            translation : {x: 5, y: 5, z: 0},
                            reverseAnimation : {duration : 800, easing : 'easeOutQuart'}
                        },
                        caption : {
                            translation : {x: 10, y: 10, z: [0,50]},
                            reverseAnimation : {duration : 1000, easing : 'easeOutQuart'}
                        },
                        shine : {
                            translation : {x: 50, y: 50, z: 0},
                            reverseAnimation : {duration : 800, easing : 'easeOutQuart'}
                        }
                    }
                },
                {
                    movement: {
                        lines : {
                            translation : {x: 40, y: 40, z: 0},
                            reverseAnimation : {duration : 1500, easing : 'easeOutElastic'}
                        },
                        caption : {
                            translation : {x: 20, y: 20, z: 0},
                            rotation : {x: 0, y: 0, z: -5},
                            reverseAnimation : {duration : 1000, easing : 'easeOutExpo'}
                        },
                        overlay : {
                            translation : {x: -30, y: -30, z: 0},
                            rotation : {x: 0, y: 0, z: 3},
                            reverseAnimation : {duration : 750, easing : 'easeOutExpo'}
                        },
                        shine : {
                            translation : {x: 100, y: 100, z: 0},
                            reverseAnimation : {duration : 750, easing : 'easeOutExpo'}
                        }
                    }
                }];

            function init() {
                var idx = 0;
                [].slice.call(document.querySelectorAll('a.tilter')).forEach(function(el, pos) {
                    idx = pos%2 === 0 ? idx+1 : idx;
                    new TiltFx(el, tiltSettings[idx-1]);
                });
            }
    });
})(jQuery);


(function ($) {
    $(window).scroll(function () {
        var x = $(this).scrollTop();

        if (x != 0) {
            $('#toTop').fadeIn(500);


        } else {
            $('#toTop').fadeOut();


        }
    });

    //$(window).on("load",function(){
    //
    //});

})(jQuery);

function opm_to_top() {
    (function ($) {
        $('#toTop').click(function () {
            $('body,html').animate({scrollTop: 0}, 800);
        });
    })(jQuery);
}

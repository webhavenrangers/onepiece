<?php
/**
 * Created by PhpStorm.
 * User: Phantom
 * Date: 11.06.2016
 * Time: 2:43
 */
require_once( STYLESHEETPATH . '/inc/hooks.php' );
require_once( STYLESHEETPATH . '/inc/ajax.php' );
require_once( STYLESHEETPATH . '/inc/utils.php' );
require_once( STYLESHEETPATH . '/inc/htmls.php' );




add_action( 'wp_enqueue_scripts', 'opm_scripts' );
//add_action( 'wp_enqueue_scripts', 'opm_scripts' );


function opm_scripts(){

	wp_register_script( 'opm_components', get_bloginfo('stylesheet_directory') . '/js/components.js', array( 'jquery' ), false, false );
	wp_enqueue_script( 'opm_components' );

//	wp_enqueue_script( 'jquery-masonry' );

	wp_register_script( 'opm_script', get_bloginfo('stylesheet_directory') . '/js/script.js', array( 'jquery' ), false, false );
	wp_enqueue_script( 'opm_script' );


	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

	wp_enqueue_style( 'opm_media', get_stylesheet_directory_uri() . '/css/media.css', array('style'));
	wp_enqueue_style( 'opm_scrollbar', get_stylesheet_directory_uri() . '/css/jquery.mCustomScrollbar.min.css', array('style'));

//	add_action( 'wp_enqueue_scripts', 'child_styles' );

}


add_action( 'admin_enqueue_scripts', 'opm_admin_scripts' );

function opm_admin_scripts() {
	wp_register_script( 'opm_backend', get_bloginfo('stylesheet_directory') . '/js/backend.js', array( 'jquery' ), false, false );
	wp_enqueue_script( 'opm_backend' );
}
<?php
/**
 * Created by PhpStorm.
 * User: hortt
 * Date: 05.09.16
 * Time: 21:53
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="viewport" content="initial-scale=1.0"/>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<title><?php wp_title(); ?></title>
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS Feed"
	      href="<?php echo bloginfo( 'rss2_url' ); ?>">

	<!-- wp_header -->
	<?php wp_head(); ?>
</head>
<?php global $post;?>
<body <?php body_class(); ?>>

<!-- wp_after_body -->
<?php wp_after_body(); ?>

<div id="cover-wrapper">
<?php //var_dump(get_cover_wrapper_image()); die;?>
	<!-- BEGIN HEADER IMAGE -->
	<div  style="background-image: url(<?php echo get_header_image()?>"  class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">

	</div>

	<!-- END HEADER IMAGE -->
	<!-- BEGIN HEADER OVERLAY COLOR -->
	<div class="header-overlay-color<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>"></div>
	<!-- END HEADER OVERLAY COLOR -->
	<div class="fixed-wrapper">
		<div class="fixed-wrapper__cards">
			<div class="fixed-wrapper__cards-inner w3-row-padding w3-center">



			<?php if (have_posts()) : ?>

				<?php while (have_posts()) : the_post(); ?>

					<!-- BEGIN LOOP -->
					<?php get_template_part( 'includes/loop-manga'); ?>
					<!-- END LOOP -->

				<?php endwhile; ?>

				<!-- BEGIN INCLUDE PAGINATION -->
				<!--						<div id="footer">-->
				<!--							--><?php //get_template_part('includes/pagination'); ?>
				<!--						</div>-->
				<!-- END INCLUDE PAGINATION -->

			<?php endif; ?>
			</div>
		</div>
	</div>

	<!-- /#content -->



	<?php serch_menu_and_form(); ?>



	<!-- /.wrapper-outer -->

</div>
<!-- /#content -->




<?php user_avatar() ?>





<!-- wp_footer -->
<?php wp_footer(); ?>
</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: Phantom
 * Date: 25.06.2016
 * Time: 2:11
 */
function get_child_attachments( $parent_post_id, $fields = '' ) {
	$thumbnail = get_post_thumbnail_id($parent_post_id);
	$args = array(
		'posts_per_page' => - 1,
		'post_type'      => 'attachment',
		'post_parent'    => $parent_post_id,
		'post_mime_type' => 'image',
		'post_status'    => 'inherit',
		'orderby'        => 'post_title',
		'order'          => 'ASC',
		'fields'         => $fields,
		'post__not_in'   => [$thumbnail]
	);

	return new WP_Query( $args );
}

class mh_post_helper {
	public static $data = array(
		'manga' => array(
			'children_post_type'     => 'chapter',
			'child_name'             => 'chapter',
			'children_name'          => 'chapters',
			'children_count_metakey' => 'cnt_children',
			'last_child_metakey'     => 'last_child_id',
		),
		'comic' => array(
			'children_post_type'     => 'issue',
			'child_name'             => 'issue',
			'children_name'          => 'issues',
			'children_count_metakey' => 'cnt_children',
			'last_child_metakey'     => 'last_child_id',
		),
	);

	public static function get( $post_type, $key ) {
		if ( isset( self::$data[ $post_type ][ $key ] ) ) {
			return self::$data[ $post_type ][ $key ];
		}

		return false;
	}

}

function mh_get_improved_name( $current_post ) {
	if ( $current_post->post_type == 'chapter' ) {
		return mh_get_improved_chapter_name( $current_post );
	}

	return false;
}

function mh_get_improved_chapter_name( $chapter ) {
	if ( $chapter->post_type != 'chapter' ) {
		return $chapter->post_title;
	}
	$manga        = mh_get_cached_post( $chapter->post_parent );
//	$chapter_info = $chapter->post_title;
//	if ( $chapter_info['recommended_to_use'] ) {
//		$name_parts = array();
//		if ( $chapter_info['volume'] != '' ) {
//			$name_parts[] = 'Volume ' . $chapter_info['volume'];
//		}
//		if ( $chapter_info['chapter_number'] != '' ) {
//			$name_parts[] = 'Chapter ' . $chapter_info['chapter_number'];
//		}
//		if ( $chapter_info['filtered_name'] != '' ) {
//			$name_parts[] = $chapter_info['filtered_name'];
//		}
//		$improved = implode( ' - ', $name_parts );
//
//		return $improved;
//	} else {
		return $chapter->post_title;
//	}
}

function mh_get_cached_post( $id ) {
	static $_cache_posts;
	if ( ! isset( $_cache_posts[ $id ] ) ) {
		$_cache_posts[ $id ] = get_post( $id );
	}

	return $_cache_posts[ $id ];
}

function mh_get_chapter_info( $chapter_name, $manga_name ) {
	$chapter_name = str_replace( '&#8211;', '-', $chapter_name );
	$manga_name   = str_replace( '&#8211;', '-', $manga_name );

	$chapter_name = str_replace( '&amp;', '&', $chapter_name );
	$manga_name   = str_replace( '&amp;', '&', $manga_name );

	$filtered_name = $chapter_name;
	$filtered_name = str_replace( ' Read Online', '', $filtered_name );

	$volume_info   = mh_get_volume_info( $filtered_name );
	$filtered_name = $volume_info['filtered_name'];

	$chapter_info   = mh_get_chapter_number_info( $filtered_name, $manga_name );
	$filtered_name  = $chapter_info['filtered_name'];
	$chapter_number = $chapter_info['chapter_number'];

	$filtered_name = str_replace( $manga_name, '', $filtered_name );
	$filtered_name = trim( $filtered_name );

	$filtered_name = str_replace( '&#8211;', '-', $filtered_name );
	$filtered_name = trim( $filtered_name, '- ' );

	// is this algo recommended to use?
	$recommended_to_use = false;
	$digits             = preg_match_all( "/[0-9]/", $chapter_number );
	$len                = strlen( $chapter_number );
	if ( $digits >= ( $len - $digits ) && ( $len > 0 ) ) {
		$recommended_to_use = true;
	} else {
		$recommended_to_use = false;
	}

	return array(
		'manga_name'            => $manga_name,
		'original_chapter_name' => $chapter_name,
		'volume'                => $volume_info['volume'],
		'chapter_number'        => $chapter_number,
		'filtered_name'         => $filtered_name,
		'recommended_to_use'    => $recommended_to_use, // true if recommended, false if not recommended
	);
}

function mh_get_chapter_number_info( $filtered_name, $manga_name ) {

	$filtered_name = html_entity_decode( $filtered_name );
	if (
		preg_match_all( '/Ch\.\s*(.*?)[\s:]/im', $filtered_name, $matches )
		|| preg_match_all( '/Ch\.(.*?)$/im', $filtered_name, $matches )
		|| preg_match_all( '/Chapter\.*\s(.*?)[\s:]/im', $filtered_name, $matches )
		//		|| preg_match_all( '/Chapter\.(.*?)$/im', $filtered_name, $matches )
		//		|| preg_match_all( '/Chapter(.*?)$/im', $filtered_name, $matches )
		|| preg_match_all( '/Chapter(.*?)$/im', $filtered_name, $matches )

		|| preg_match_all( '/' . preg_quote( $manga_name ) . '\sEp\.\s(.*?)[\s:]/im', $filtered_name, $matches )
		|| preg_match_all( '/' . preg_quote( $manga_name ) . '\sEp\.\s(.*?)$/im', $filtered_name, $matches )
		//		|| preg_match_all( '/' . preg_quote( $manga_name ) . '\s(.*?)[\s:]/im', $filtered_name, $matches )
		|| preg_match_all( '/' . preg_quote( $manga_name ) . '\s(.*?)[\s:]/im', $filtered_name, $matches )
		|| preg_match_all( '/' . preg_quote( $manga_name ) . '\s(.*?)$/im', $filtered_name, $matches )
	) {
		$chapter_number = $matches[1][0];
		$filtered_name  = str_replace( $matches[0][0], '', $filtered_name );
	}

	if ( $chapter_number == '000' ) {
		$chapter_number = '0';
	} else {
		$chapter_number = ltrim( $chapter_number, '0' );
	}

	$filtered_name = trim( $filtered_name );
	$filtered_name = trim( $filtered_name, ':' );

	return array(
		'chapter_number' => $chapter_number,
		'filtered_name'  => $filtered_name,
	);
}

function mh_get_volume_info( $filtered_name ) {
	$volume = '';
	if (
		preg_match_all( '/Vol[\.\s](.*?)[\s:]/im', $filtered_name, $matches )
		|| preg_match_all( '/Vol[\.\s](.*?)$/im', $filtered_name, $matches )
	) {

		$filtered_name = str_replace( $matches[0][0], '', $filtered_name );
		$filtered_name = trim( $filtered_name );

		$volume = $matches[1][0];
		if ( $volume == '000' ) {
			$volume = '0';
		} else {
			$volume = ltrim( $volume, '0' );
		}
	}

	return array(
		'volume'        => $volume,
		'filtered_name' => $filtered_name,
	);
}

function mh_get_last_published_chapter_or_issue_id( $post_id ) {
	$args  = [
		'post_type'      => array( 'chapter', 'issue' ),
		'posts_per_page' => 1,
		'post_parent'    => $post_id,
		'post_status'    => 'publish',
		'orderby'        => 'ID',
		'order'          => 'DESC',
	];
	$query = new WP_Query( $args );
	if ( isset( $query->posts[0] ) ) {
		return $query->posts[0];
	} else {
		return null;
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: Phantom
 * Date: 25.06.2016
 * Time: 2:11
 */
function print_chapters_list( $post ) {
	$args           = array(
		'post_type'      => mh_post_helper::get( $post->post_type, 'child_name' ),
		'posts_per_page' => -1,
		'post_parent'    => $post->ID,
		'orderby'        => 'ID',
		'order'          => 'DESC',
	);
	$chapters_query = new WP_Query( $args );
	if ( $chapters_query->have_posts() ) { ?>
		<ul class="chapters-list">
		<?php foreach ( $chapters_query->posts as $key => $sibling ) {
			$style = $key> 8?' style="display:none;" ':'';
			$sibling_name = mh_get_improved_name( $sibling ); ?>
			<li  <?php echo $style ?>><a data-not-in="<?php echo $sibling->ID ?>" href="<?php the_permalink( $sibling ); ?>"
			       title="<?php echo esc_attr( $sibling_name ); ?>"
			       class="chapter-title">
					<?php echo $sibling_name; ?></a>
			</li>
		<?php } ?>
		</ul>
	<?php
		echo op_show_all_button($chapters_query->found_posts, 8 );
	}
}

function op_show_all_button($total, $offset) {
//	var_dump($total);
	$diff = $total-$offset;
	$button = $total > $offset?'<a href="javascript:void(0)" class="op_show_more_button">+ Show all ('.$diff.')</a>':'';
	return $button;
}


function get_manga_extradata( $post ) {
//	$post = get_post( $post );

	$name                = $post->post_title;
	$altername_names_esc = esc_attr( get_post_meta( $post->ID, 'altr_nms', true ) );
	$release_year        = get_post_meta( $post->ID, 'release_year', true );
	$status              = get_post_meta( $post->ID, 'status', true );
	$author              = get_post_meta( $post->ID, 'author', true );
	$genres              = get_the_tag_list();
	$synopsis            = $post->post_content;
	ob_start();
	if ( ! empty( $name ) ) {
		opm_print_data_with_label( $name, 'name' );
	}
	if ( ! empty( $release_year ) ) {
		opm_print_data_with_label( $release_year, 'year of release' );
	}
	if ( ! empty( $status ) ) {
		opm_print_data_with_label( $status, 'status' );
	}
	if ( ! empty( $author ) ) {
		opm_print_data_with_label( $author, 'author' );
	}
	if ( ! empty( $genres ) ) {
		opm_print_data_with_label( $genres, 'genres' );
	}
	if ( ! empty( $synopsis ) ) {
		opm_print_data_with_label( $synopsis, 'synopsis' );
	}
	$output = ob_get_clean();

	return $output;

}

function opm_print_data_with_label( $data, $label ) { ?>
	<div class="info">
	<span class="type"><?php echo $label ?>: </span>
	<span class="<?php echo str_replace( ' ', '_', $label ) ?>"><?php echo $data ?></span>
	</div><?php
}

function search_button( $echo = true ) {
	ob_start(); ?>
	<!-- BEGIN SEARCH BUTTON -->
	<div id="header-search-button">
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1"
		     x="0px" y="0px" viewBox="0 0 53.627 53.627" style="enable-background:new 0 0 53.627 53.627;"
		     xml:space="preserve" width="512px" height="512px">
<path
	d="M53.627,49.385L37.795,33.553C40.423,30.046,42,25.709,42,21C42,9.42,32.58,0,21,0S0,9.42,0,21s9.42,21,21,21  c4.709,0,9.046-1.577,12.553-4.205l15.832,15.832L53.627,49.385z M2,21C2,10.523,10.523,2,21,2s19,8.523,19,19s-8.523,19-19,19  S2,31.477,2,21z M35.567,36.093c0.178-0.172,0.353-0.347,0.525-0.525c0.146-0.151,0.304-0.29,0.445-0.445l14.262,14.262  l-1.415,1.415L35.123,36.537C35.278,36.396,35.416,36.238,35.567,36.093z"/>
</svg>
	</div>
	<!-- END SEARCH BUTTON -->
	<?php $output = ob_get_clean();
	if ( ! $echo ) {
		return $output;
	} else {
		echo $output;
	}
}

function site_logo( $echo = true, $description = true, $tagline = true ) {
	ob_start(); ?>
	<!-- BEGIN LOGO -->
	<div class="site-logo">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="branding_logo"></a>
		<?php if ( $description ): ?>
			<a class="text-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				READ <?php bloginfo( 'name' ); ?>
			</a>
		<?php endif; ?>
		<?php if ( $tagline ): ?>
			<!-- BEGIN WORDPRESS TAGLINE -->
			<div class="tagline">
				<?php bloginfo( 'description' ); ?>
			</div>
			<!-- END WORDPRESS TAGLINE -->
		<?php endif; ?>
	</div>
	<!-- END LOGO --><?php
	$output = ob_get_clean();
	if ( ! $echo ) {
		return $output;
	} else {
		echo $output;
	}
}

function serch_menu_and_form( $echo = true ) {
	ob_start(); ?>
	<!-- BEGIN SEARCH FORM + MENU -->
	<div class="search-menu-wrapper">

		<!-- BEGIN SEARCH -->
		<?php search_button(); ?>

		<!-- BEGIN SEARCH FORM -->
		<div class="header-search-background"></div>
		<div id="header-search-wrapper">
			<?php get_search_form(); ?>
		</div>
		<!-- END SEARCH FORM -->
		<!-- END SEARCH -->

		<!-- BEGIN MENU -->
		<div id="main-menu">
			<?php wp_nav_menu( array(
				'container_class' => 'bonfire-main-menu',
				'theme_location'  => 'bonfire-main-menu',
				'fallback_cb'     => '',
				'items_wrap' => opm_nav_wrap()
			) ); ?>
		</div>
		<!-- END MENU -->

		<!-- BEGIN MOBILE MENU BUTTON + MENU -->
		<div class="sparrow-mobile-background">
			<div id="main-menu-mobile">
				<?php wp_nav_menu( array(
					'container_class' => 'bonfire-main-menu',
					'theme_location'  => 'bonfire-main-menu',
					'fallback_cb'     => '',
					'items_wrap' => opm_nav_wrap()
				) ); ?>
			</div>
		</div>

		<div class="sparrow-menu-button-wrapper">
			<div class="sparrow-menu-button">
				<div class="sparrow-menu-button-middle"></div>
			</div>
		</div>
		<!-- END MOBILE MENU BUTTON + MENU -->

	</div>
	<!-- END SEARCH FORM + MENU --><?php
	$output = ob_get_clean();
	if ( ! $echo ) {
		return $output;
	} else {
		echo $output;
	}

}

function opm_nav_wrap(){
	// default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'

	// open the <ul>, set 'menu_class' and 'menu_id' values
	$wrap  = '<ul id="%1$s" class="%2$s">';

	if( is_archive('manga')) {
		// the static link
		$wrap .= '<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item"><a href="'.get_site_url().'">Home</a></li>';
	}

	// get nav items as configured in /wp-admin/
	$wrap .= '%3$s';



	// close the <ul>
	$wrap .= '</ul>';
	// return the result
	return $wrap;
}

function user_avatar( $echo = true ) {
	ob_start(); ?>
	<!-- BEGIN USER AVATAR -->
	<?php if ( get_option( 'bonfire_sparrow_gravatar_emailaddress' ) ||
	           get_option( 'bonfire_sparrow_custom_profile_image' )
	) { ?>
		<div class="user-avatar-arrow<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>"></div>
		<div class="user-avatar<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
			<?php if ( get_option( 'bonfire_sparrow_custom_profile_image' ) ) { ?>
				<img src="<?php echo get_option( 'bonfire_sparrow_custom_profile_image' ); ?>">
			<?php } else { ?>
				<?php echo get_avatar( get_option( 'bonfire_sparrow_gravatar_emailaddress' ), 200 ); ?>
			<?php } ?>
		</div>
		<div class="user-avatar-menu<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
			<?php wp_nav_menu( array(
				'container_class' => 'bonfire-author-menu',
				'theme_location'  => 'bonfire-author-menu',
				'fallback_cb'     => ''
			) ); ?>
		</div>
	<?php } ?>
	<!-- END USER AVATAR --><?php
	$output = ob_get_clean();
	if ( ! $echo ) {
		return $output;
	} else {
		echo $output;
	}
}

function video_background() {
	global $post;
	if ( is_object( $post ) ) { ?>
		<!-- BEGIN INSERT YOUTUBE AS BACKGROUND VIDEO -->
		<script>
			jQuery().ready(function () {
				jQuery('#videobackground').tubular({videoId: '<?php $videobackground = get_post_meta($post->ID, 'VideoBackground', true); ?><?php echo esc_attr($videobackground); ?>'});
			});
		</script>
		<style>
			#tubular-container,
			#tubular-shield {
				height: 100% !important;
			}
		</style>
		<!-- END INSERT YOUTUBE AS BACKGROUND VIDEO --><?php
	}
}

function full_screen_profile( $echo = true ) {
	ob_start(); ?>
	<!-- BEGIN FULL-SCREEN PROFILE -->
	<div class="profile-wrapper">
		<div class="closeprofile">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
			     xmlns:xlink="http://www.w3.org/1999/xlink"
			     x="0px" y="0px"
			     viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
		<path d="M9.016,40.837c0.195,0.195,0.451,0.292,0.707,0.292c0.256,0,0.512-0.098,0.708-0.293l14.292-14.309
			l14.292,14.309c0.195,0.196,0.451,0.293,0.708,0.293c0.256,0,0.512-0.098,0.707-0.292c0.391-0.39,0.391-1.023,0.001-1.414
			L26.153,25.129L40.43,10.836c0.39-0.391,0.39-1.024-0.001-1.414c-0.392-0.391-1.024-0.391-1.414,0.001L24.722,23.732L10.43,9.423
			c-0.391-0.391-1.024-0.391-1.414-0.001c-0.391,0.39-0.391,1.023-0.001,1.414l14.276,14.293L9.015,39.423
			C8.625,39.813,8.625,40.447,9.016,40.837z"/>
		</svg>
		</div>
		<!-- BEGIN PAGE CONTENT -->
		<div class="profile-inner">
			<?php $my_query = new WP_Query( 'pagename=sparrow-profile' ); ?>
			<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</div>
		<!-- END PAGE CONTENT -->
	</div>
	<!-- END FULL-SCREEN PROFILE --><?php
	$output = ob_get_clean();
	if ( ! $echo ) {
		return $output;
	} else {
		echo $output;
	}
}
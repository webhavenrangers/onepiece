<?php
/**
 * Created by PhpStorm.
 * User: Phantom
 * Date: 25.06.2016
 * Time: 2:11
 */

add_action( 'init', 'mh_create_taxonomies' );

function mh_create_taxonomies() {

	$labels = array(
		'name'              => 'Crew',
		'singular_name'     => 'Crew',
		'search_items'      => 'Search Crews',
		'all_items'         => 'All Crews',
		'parent_item'       => 'Parent Crews',
		'parent_item_colon' => 'Parent Crew:',
		'edit_item'         => 'Edit Crew',
		'update_item'       => 'Update Crew',
		'add_new_item'      => 'Add New Crew',
		'new_item_name'     => 'New Crew Name',
		'menu_name'         => 'Crew',
	);
	$args   = array(
		'label'                 => '',
		'labels'                => $labels,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_tagcloud'         => true,
		'hierarchical'          => false,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy,
		'capabilities'          => array(),
		'meta_box_cb'           => null,
		'show_admin_column'     => false,
		'_builtin'              => false,
		'show_in_quick_edit'    => null,
		'show_in_rest'          => true,
		'rest_base'             => 'crew',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	);
	register_taxonomy( 'crew', 'pirates', $args );


	$labels = array(
		'name'              => 'Authors',
		'singular_name'     => 'Authors',
		'search_items'      => 'Search Authors',
		'all_items'         => 'All Authors',
		'parent_item'       => 'Parent Author',
		'parent_item_colon' => 'Parent Author:',
		'edit_item'         => 'Edit Author',
		'update_item'       => 'Update Author',
		'add_new_item'      => 'Add New Author',
		'new_item_name'     => 'New Author Name',
		'menu_name'         => 'Author',
	);
	$args   = array(
		'label'                 => '',
		'labels'                => $labels,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_tagcloud'         => true,
		'hierarchical'          => false,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy,
		'capabilities'          => array(),
		'meta_box_cb'           => null,
		'show_admin_column'     => false,
		'_builtin'              => false,
		'show_in_quick_edit'    => null,
		'show_in_rest'          => true,
		'rest_base'             => 'author',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	);
	register_taxonomy( 'author', 'manga', $args );

	$labels = array(
		'name'              => 'Genres',
		'singular_name'     => 'Genre',
		'search_items'      => 'Search Genres',
		'all_items'         => 'All Genres',
		'parent_item'       => 'Parent Genre',
		'parent_item_colon' => 'Parent Genre:',
		'edit_item'         => 'Edit Genre',
		'update_item'       => 'Update Genre',
		'add_new_item'      => 'Add New Genre',
		'new_item_name'     => 'New Genre Name',
		'menu_name'         => 'Genre',
	);
	$args   = array(
		'label'                 => '',
		'labels'                => $labels,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_tagcloud'         => true,
		'hierarchical'          => false,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy,
		'capabilities'          => array(),
		'meta_box_cb'           => null,
		'show_admin_column'     => false,
		'_builtin'              => false,
		'show_in_quick_edit'    => null,
		'show_in_rest'          => true,
		'rest_base'             => 'genre',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	);
	register_taxonomy( 'genre', array( 'manga',  ), $args );

	$labels = array(
		'name'          => 'Artists',
		'singular_name' => 'Artist',
		'search_items'  => 'Search Artists',
		'all_items'     => 'All Artists',
		'edit_item'     => 'Edit Artist',
		'update_item'   => 'Update Artist',
		'add_new_item'  => 'Add New Artist',
		'new_item_name' => 'New Artist Name',
		'menu_name'     => 'Artist',
	);
	$args   = array(
		'label'                 => '',
		'labels'                => $labels,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_tagcloud'         => true,
		'hierarchical'          => false,
		'update_count_callback' => '',
		'rewrite'               => true,
		'capabilities'          => array(),
		'meta_box_cb'           => null,
		'show_admin_column'     => false,
		'_builtin'              => false,
		'show_in_quick_edit'    => null,
		'show_in_rest'          => true,
		'rest_base'             => 'artist',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	);
	register_taxonomy( 'artist', array( 'manga' ), $args );

}

function mh_post_types_init() {
	$labels = array(
		'name'               => 'pirates',
		'singular_name'      => 'Person',
		'menu_name'          => 'People',
		'name_admin_bar'     => 'pirates',
		'add_new'            => 'Add new',
		'add_new_item'       => 'Add New Sailor',
		'new_item'           => 'New Sailor',
		'edit_item'          => 'Edit pirate',
		'view_item'          => 'View pirate',
		'all_items'          => 'All pirates',
		'search_items'       => 'Search pirates',
//        'parent_item_colon' => 'Parent Manga', // .............
		'not_found'          => 'No pirate found',
		'not_found_in_trash' => 'No pirate found in Trash.'
	);

	$args = array(
		'labels'              => $labels,
		'description'         => 'pirates',
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'query_var'           => true,
		'rewrite'             => array( 'slug' => 'pirate' ),
		'capability_type'     => 'post',
		'has_archive'         => true,
		'hierarchical'        => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-universal-access',
		'supports'            => array(
			'page-attributes',
			'title',
			'editor',
			'thumbnail',
			'comments',
		)
	);

	register_post_type( 'pirates', $args );

	$labels = array(
		'name'               => 'Mangas',
		'singular_name'      => 'Manga',
		'menu_name'          => 'Mangas',
		'name_admin_bar'     => 'Manga',
		'add_new'            => 'Add new',
		'add_new_item'       => 'Add New Manga',
		'new_item'           => 'New Manga',
		'edit_item'          => 'Edit Manga',
		'view_item'          => 'View Manga',
		'all_items'          => 'All Mangas',
		'search_items'       => 'Search Mangas',
//        'parent_item_colon' => 'Parent Manga', // .............
		'not_found'          => 'No mangas found',
		'not_found_in_trash' => 'No mangas found in Trash.'
	);

	$args = array(
		'labels'              => $labels,
		'description'         => 'Description',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'query_var'           => true,
		'rewrite'             => array( 'slug' => 'manga' ),
		'capability_type'     => 'post',
		'has_archive'         => true,
		'hierarchical'        => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-book-alt',
		'supports'            => array(
			'page-attributes',
			'title',
			'editor',
			'thumbnail',
			'comments',
		),
		'taxonomies' => array('post_tag')
	);

	register_post_type( 'manga', $args );


	$labels = array(
		'name'               => 'Chapters',
		'singular_name'      => 'Chapter',
		'menu_name'          => 'Chapters',
		'name_admin_bar'     => 'Chapter',
		'add_new'            => 'Add new',
		'add_new_item'       => 'Add New Chapter',
		'new_item'           => 'New Chapter',
		'edit_item'          => 'Edit Chapter',
		'view_item'          => 'View Chapter',
		'all_items'          => 'All Chapters',
		'search_items'       => 'Search Chapters',
//        'parent_item_colon' => 'Parent Manga', // .............
		'not_found'          => 'No chapters found',
		'not_found_in_trash' => 'No chapters found in Trash.'
	);

	$args = array(
		'labels' => $labels,

		'description'         => 'Description',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'query_var'           => true,
		'rewrite'             => array( 'slug' => 'chapter' ),
		'capability_type'     => 'post',
		'has_archive'         => false,
		'hierarchical'        => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-media-text',
		'supports'            => array(
			'page-attributes',
			'title',
			'thumbnail',
			'comments',
			'editor',
		)
	);
	register_post_type( 'chapter', $args );
	add_post_type_support( 'chapter', 'excerpt' );

	add_image_size( 'small-vertical', '70', '110', false );
	add_image_size( 'manga-thumb', '170', '250', false );
}

add_action( 'init', 'mh_post_types_init' );

/**
 * fixes styles inline print from breadcrumbs plugin
 */
remove_action( 'after_setup_theme', 'breadcrumb_trail_theme_setup', 12 );

/**
 * @param $link
 * @param $id
 * @param $size
 * @param $permalink
 * @param $icon
 * @param $text
 * replaces link href="http://onepiece-manga.com/file/2016/06/chp/5/ch_154_One-Piece-149/001.jpg" with remote equivalent
 *
 * @return mixed
 */
//remove_filter( 'wp_get_attachment_link', 'photoswipe_get_attachment_link', 10 );
//add_filter( 'wp_get_attachment_link', 'op_photoswipe_get_attachment_link', 10, 6 );
function op_photoswipe_get_attachment_link( $link, $id, $size, $permalink, $icon, $text ) {
	if ( $permalink === false && ! $text && 'none' != $size ) {
		$_post = get_post( $id );
		remove_filter( 'wp_get_attachment_image_src', 'ca_change_src', 11 );
		$image_attributes = wp_get_attachment_image_src( $_post->ID, 'original' );
		if ( $image_attributes ) {
			$origin_image = get_post_meta( $id, 'origin_image', true );
			$new_src      = ! empty( $origin_image ) ? $origin_image : $image_attributes[0];
			$link         =
				str_replace( '<a ', '<a data-size="' . $image_attributes[1] . 'x' . $image_attributes[2] . '" ',
					$link );
			$link         = str_replace( $image_attributes[0], $new_src, $link );
		}
	}

	return $link;
}

/**
 * @param $image
 * @param $attachment_id
 * @param $size
 * @param $icon
 * replaces image SRC href="http://onepiece-manga.com/file/2016/06/chp/5/ch_154_One-Piece-149/001.jpg" with remote equivalent
 *
 * @return array
 */
//if( !is_admin())
//	add_filter( 'wp_get_attachment_image_src', 'ca_change_src', 11, 4 );
function ca_change_src( $image, $attachment_id, $size, $icon ) {
	$width        = $image[1];
	$height       = $image[2];
	$origin_image = get_post_meta( $attachment_id, 'origin_image', true );
	$src          = empty( $origin_image ) ? $image[0] : $origin_image;

	return array( $src, $width, $height, false );
}

add_action( 'after_setup_theme', 'remove_admin_bar' );

function remove_admin_bar() {
	if ( ! is_admin() ) {
		show_admin_bar( false );
	}
}

function cc_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';

	return $mimes;
}

add_filter( 'upload_mimes', 'cc_mime_types' );

/* WP seo title filter */
add_filter( 'wpseo_metadesc', 'add_alternate_names_to_seotitle' );

function add_alternate_names_to_seotitle( $metadesc ) {
	global $post;
	if ( is_home() || !is_object( $post ) ) {
		return $metadesc;
	}
	$post_id = $post->ID;
	$meta    = get_post_meta( $post_id, 'altr_nms', true );
	if ( $post->post_type == 'chapter' ) {
		$meta = get_post_meta( $post->post_parent, 'altr_nms', true );
	}
	if ( ! empty( $meta ) && strpos( $metadesc, '([alternames])' ) !== false ) {

		$metadesc = preg_replace( '/\(\[alternames\]\)/', ' (' . $meta . ')', $metadesc, 1 ) . ' ' .
		            substr( $post->post_content, 0, 250 ) . '...';
	}


	return $metadesc;
}

remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'feed_links_extra', 3 );

add_action('wp_head', 'theme_mobile_color');
function theme_mobile_color(){
	echo '<meta name="theme-color" content="#636363">';
}
<?php
/**
 * Created by PhpStorm.
 * User: Phantom
 * Date: 28.06.2016
 * Time: 10:59
 */

/**
 * Search AJAX
 */
function search_ajax() {
	global $wpdb;
	$keyword  = "'%" . sanitize_text_field( $_POST['keyword'] ) . "%'";
	$prefix   = $wpdb->prefix;
	$query    =
		"SELECT {$prefix}posts.ID, {$prefix}posts.post_parent parent  FROM {$prefix}posts
				WHERE 1=1  AND {$prefix}posts.post_title LIKE {$keyword}
			  	AND  {$prefix}posts.post_type = 'chapter'
				AND {$prefix}posts.post_status = 'publish'  GROUP BY parent LIMIT 0, 4";
	$post_ids = $wpdb->get_results( $query );
	ob_start();
	if ( count( $post_ids ) > 0 ) :
		foreach ( $post_ids as $post ) {
			$post = get_post($post->ID);
			$post_title = $post->post_title;
			$link = get_permalink( $post );
			remove_filter( 'wp_get_attachment_image_src', 'ca_change_src', 11 );
			$thumb = get_the_post_thumbnail( $post->post_parent, 'small-vertical' );
			?>
			<div class="list manga">
				<a class="clearfix" href="<?php echo $link; ?>">
					<span class="on thumb pull-left">
						<?php echo $thumb; ?>
					</span>
					<?php echo $post_title; ?>
				</a>
			</div><?php
		}
	endif;
	wp_reset_postdata();
	$output = ob_get_contents();
	ob_end_clean();
	echo json_encode( $output );
	die();
}

add_action( 'wp_ajax_search_ajax', 'search_ajax' );
add_action( 'wp_ajax_nopriv_search_ajax', 'search_ajax' );

function fetch_data_from_my_anime_list( $url = '' ) {
	if ( empty( $url ) )
		$url                  = $_POST['url'];
	$anime_name_to_search = basename( $url );
	require_once( "easy_html_dom_parser.php" );
	$ch   = curl_init();
	$url  = 'http://myanimelist.net/api/manga/search.xml?q=' . $anime_name_to_search;
	$page = dom_parser_curl_get_file( $url, $ch );
	curl_close( $ch );
	$content           = simplexml_load_string( $page );
	$xml_data          = $content->entry;
	if ( ! empty( $xml_data->english ) )
		$xml_data->synonyms .= ' '. $xml_data->english . "; ";

	if ( defined('DOING_AJAX') )
		echo json_encode( $xml_data );
	else
		return $xml_data;
	die();
}

add_action( 'wp_ajax_nopriv_fetch_data_from_myal', 'fetch_data_from_my_anime_list' );
add_action( 'wp_ajax_fetch_data_from_myal', 'fetch_data_from_my_anime_list' );

function set_feutered_image_from_myal() {
	global $wpdb;
	$file_path  = $_POST['image_url'];
	$post_id    = $_POST['post_id'];
	if ( has_post_thumbnail( $post_id ) ) {
		echo 'image is already set';
		die();
	}
	if( function_exists('create_image_from_remote_host'))
		$attach_id = create_image_from_remote_host ( $file_path, $post_id );
	if ( $attach_id ) {
		delete_post_meta( $post_id, '_thumbnail_id' );
		add_post_meta( $post_id, '_thumbnail_id', $attach_id, true );
		$wpdb->query( $wpdb->prepare( "UPDATE $wpdb->posts SET post_parent = %d WHERE ID = %d", $post_id, $attach_id ) );
		echo get_the_post_thumbnail( $post_id, 'thumbnail' );
	} else {
		echo 'No image created';
	}
	die();
}

add_action( 'wp_ajax_nopriv_set_feutered_image_from_myal', 'set_feutered_image_from_myal' );
add_action( 'wp_ajax_set_feutered_image_from_myal', 'set_feutered_image_from_myal' );


//function fetch_images_for_chapter() {
//	$raw_data = $_POST['raw_data'];
//	$post_id = $_POST['post_id'];
//	$chapter = get_post($post_id);
//	$array1 = explode('||',$raw_data);
//	$urls = [];
//
//	foreach ($array1 as $url ) {
//		$sanitized_url = str_replace('"','',$url);
//		$sanitized_url = str_replace('\\','',$sanitized_url);
//		$sanitized_url = str_replace(');','',$sanitized_url);
//		$sanitized_url = trim($sanitized_url);
//		if( !empty($sanitized_url)) {
//			$urls[] = sanitize_text_field($sanitized_url);
//		}
//	}
//	foreach ($urls as $key => $value ) {
//		$attach_id = create_image_from_remote_host(
//			$value,
//			$post_id,
//			$chapter->post_title . ' - image: ' . sprintf( '%03d', $key ) // post title
//		);
//		var_dump($attach_id);
//	}
//
//	die();
//}



//add_action( 'wp_ajax_nopriv_fetch_images_for_chapter', 'fetch_images_for_chapter' );
//add_action( 'wp_ajax_fetch_images_for_chapter', 'fetch_images_for_chapter' );

add_action ('wp_ajax_nopriv_get_cover_wrapper_image', 'get_cover_wrapper_image' );
add_action ('wp_ajax_get_cover_wrapper_image', 'get_cover_wrapper_image' );

function get_cover_wrapper_image() {
	global $post;
//	var_dump($post);die;
	if ( has_post_thumbnail() ) {
		$image_id  = get_post_thumbnail_id();
		$image_url = wp_get_attachment_image_src( $image_id, 'large', true );
		echo esc_url( $image_url[0] );
	} else {
		header_image();
	}
	die;
}
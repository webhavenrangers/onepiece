<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS Feed"
	      href="<?php echo bloginfo( 'rss2_url' ); ?>">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script>
		(adsbygoogle = window.adsbygoogle || []).push({
			google_ad_client: "ca-pub-2724159662472385",
			enable_page_level_ads: true
		});
	</script>
	<!-- wp_header -->
	<?php wp_head(); ?>
</head>
<?php global $post;?>
<body <?php body_class(); ?>>

<!-- wp_after_body -->
<?php wp_after_body(); ?>

<!-- BEGIN HEADER (SEARCH + LOGO + MENU + BACKGROUND IMAGE + VIDEO + COLOR) -->
<?php if ( is_singular( 'manga' ) ) { ?>

	<?php if ( get_post_meta( $post->ID, 'VideoBackground', true ) ) { ?>
		<!-- BEGIN CUSTOM FIELD FOR HEADER BACKGROUND VIDEO -->
		<?php if ( wp_is_mobile() ) { ?>
			<div style="background-image: url(<?php get_cover_wrapper_image()?>"  style="background-image: url(<?php echo get_header_image()?>"  class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
				<!--			<i class="fa fa-lg fa-spin fa-spinner"></i>-->
			</div>
			<!-- END FEATURED IMAGE -->
		<?php } else { ?>
			<div id="videobackground"></div>
		<?php } ?>
		<!-- END CUSTOM FIELD FOR HEADER BACKGROUND VIDEO -->
	<?php } else { ?>
		<!-- BEGIN FEATURED IMAGE -->
		<div  style="background-image: url(<?php echo get_header_image()?>"  class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
<!--			<!--			<i class="fa fa-lg fa-spin fa-spinner"></i>-->-->
		</div>
	<?php } ?>

<?php } elseif ( is_singular() ) {
	if ( get_post_meta( $post->ID, 'VideoBackground', true ) ) { ?>
		<!-- BEGIN CUSTOM FIELD FOR HEADER BACKGROUND VIDEO -->
		<?php if ( wp_is_mobile() ) { ?>
			<!-- BEGIN FEATURED IMAGE -->
			<div  style="background-image: url(<?php echo get_header_image()?>"  class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
				<!--			<i class="fa fa-lg fa-spin fa-spinner"></i>-->
			</div>
			<!-- END FEATURED IMAGE -->
		<?php } else { ?>
			<div id="videobackground"></div>
		<?php } ?>
		<!-- END CUSTOM FIELD FOR HEADER BACKGROUND VIDEO -->
	<?php } else { ?>
		<!-- BEGIN FEATURED IMAGE -->
		<div  style="background-image: url(<?php echo get_header_image()?>"  class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
			<!--			<i class="fa fa-lg fa-spin fa-spinner"></i>-->
		</div>
		<!-- END FEATURED IMAGE -->
		<?php
	}
} else { ?>

	<div  style="background-image: url(<?php echo get_header_image()?>"  class="header-image<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
		<!--			<i class="fa fa-lg fa-spin fa-spinner"></i>-->
	</div>

<?php } ?>

<!-- BEGIN HEADER OVERLAY COLOR -->
<div class="header-overlay-color<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>"></div>
<!-- END HEADER OVERLAY COLOR -->

<!-- BEGIN SITE LOGO -->
<div class="site-logo-wrapper<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
	<?php if ( get_theme_mod( 'bonfire_logo' ) ) : ?>

		<!-- BEGIN LOGO IMAGE -->
		<div class="site-logo-image">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>"
			   title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img
					src="<?php echo get_theme_mod( 'bonfire_logo' ); ?>"
					alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
		</div>
		<!-- END LOGO IMAGE -->

	<?php else : ?>

		<!-- BEGIN LOGO -->
		<div class="site-logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php bloginfo( 'name' ); ?>
			</a>
		</div>
		<!-- END LOGO -->

	<?php endif; ?>
</div>
<!-- END SITE LOGO -->

<!-- BEGIN SEARCH FORM + MENU -->
<div class="search-menu-wrapper">

	<!-- BEGIN SEARCH -->
	<?php search_button(); ?>

	<!-- BEGIN SEARCH FORM -->
	<div class="header-search-background"></div>
	<div id="header-search-wrapper">
		<?php get_search_form(); ?>
	</div>
	<!-- END SEARCH FORM -->
	<!-- END SEARCH -->

	<!-- BEGIN MENU -->
	<div id="main-menu">
		<?php wp_nav_menu( array(
			'container_class' => 'bonfire-main-menu',
			'theme_location'  => 'bonfire-main-menu',
			'fallback_cb'     => ''
		) ); ?>
	</div>
	<!-- END MENU -->

	<!-- BEGIN MOBILE MENU BUTTON + MENU -->
	<div class="sparrow-mobile-background">
		<div id="main-menu-mobile">
			<?php wp_nav_menu( array(
				'container_class' => 'bonfire-main-menu',
				'theme_location'  => 'bonfire-main-menu',
				'fallback_cb'     => ''
			) ); ?>
		</div>
	</div>

	<div class="sparrow-menu-button-wrapper">
		<div class="sparrow-menu-button">
			<div class="sparrow-menu-button-middle"></div>
		</div>
	</div>
	<!-- END MOBILE MENU BUTTON + MENU -->

</div>
<!-- END SEARCH FORM + MENU -->

<!-- END HEADER (SEARCH + LOGO + MENU + BACKGROUND IMAGE + VIDEO + COLOR) -->

<!-- BEGIN USER AVATAR -->
<?php if ( get_option( 'bonfire_sparrow_gravatar_emailaddress' ) ||
           get_option( 'bonfire_sparrow_custom_profile_image' )
) { ?>
	<div class="user-avatar-arrow<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>"></div>
	<div class="user-avatar<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
		<?php if ( get_option( 'bonfire_sparrow_custom_profile_image' ) ) { ?>
			<img src="<?php echo get_option( 'bonfire_sparrow_custom_profile_image' ); ?>">
		<?php } else { ?>
			<?php echo get_avatar( get_option( 'bonfire_sparrow_gravatar_emailaddress' ), 200 ); ?>
		<?php } ?>
	</div>
	<div class="user-avatar-menu<?php if ( is_admin_bar_showing() ) { ?> wp-toolbar-active<?php } ?>">
		<?php wp_nav_menu( array(
			'container_class' => 'bonfire-author-menu',
			'theme_location'  => 'bonfire-author-menu',
			'fallback_cb'     => ''
		) ); ?>
	</div>
<?php } ?>
<!-- END USER AVATAR -->

<!-- BEGIN BREADCRUMBS -->

<?php if ( function_exists( 'breadcrumb_trail' ) ) {
	$args = array(
		'labels' => array(
			'browse' => esc_html__( 'You are here: ', 'breadcrumb-trail' ),
		)
	);
	breadcrumb_trail( $args );
} ?>
<!-- END BREADCRUMBS -->

<div id="sitewrap">

	<div id="body" class="pagewidth clearfix">
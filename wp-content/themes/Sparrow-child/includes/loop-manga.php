<?php
/**
 * Created by PhpStorm.
 * User: hortt
 * Date: 05.09.16
 * Time: 21:17
 */
$post = get_post();
$last_chapter = mh_get_last_published_chapter_or_issue_id( $post->ID );
if(!is_single()) : global $more; $more = 0; endif; //enable more link ?>

		<div class="w3-third w3-margin-bottom">
			<div class="w3-card-2 w3-padding-bottom w3-padding-right w3-padding-left" >
				<h4><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'bonfire' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
						<?php the_title(); ?>
					</a></h4>
				<a href="<?php the_permalink(); ?>" class="tilter tilter--5">
					<figure class="tilter__figure">
						<?php echo get_the_post_thumbnail( null, 'manga-thumb', ['class'=>'tilter__image'] )?>
						<div class="tilter__deco tilter__deco--shine"><div></div></div>
						<div class="tilter__deco tilter__deco--overlay"></div>
						<figcaption class="tilter__caption">
							<p class="tilter__description">by Eiichiro Oda</p>
						</figcaption>
						<svg class="tilter__deco tilter__deco--lines" viewBox="0 0 300 415">
							<path d="M20.5,20.5h260v375h-260V20.5z" />
						</svg>
					</figure>
				</a>
<!--				<i class="fa fa-desktop w3-margin-bottom w3-text-theme" style="font-size:120px"></i>-->
				<?php if ( $last_chapter ) { ?>
					<p class="w3-padding-24">Latest: <a type="button" id="view-last-chapter-button"
					                     href="<?php the_permalink( $last_chapter ); ?>"
					                     title="<?php echo esc_attr( get_the_title( $last_chapter ) ); ?>">
						<?php echo substr(mh_get_improved_name( $last_chapter ), 0, 21).'...';?></a></p>

				<?php } ?>
			</div>
		</div>

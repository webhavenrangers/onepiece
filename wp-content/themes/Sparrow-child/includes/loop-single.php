<?php if(!is_single()) : global $more; $more = 0; endif; //enable more link ?>

<div class="content-wrapper-single">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<!-- BEGIN POST DATE + COMMENT COUNT -->
		<div class="post-date-comment">
			<time datetime="<?php echo esc_attr( the_time('o-m-d') ); ?>"><?php the_time('F j, Y') ?></time> /
			<div class="comment-count">
				<?php comments_number( 'Leave comment', 'One comment', '% comments' ); ?>
			</div>
		</div>
		<!-- END POST DATE + COMMENT COUNT -->

		<!-- BEGIN TITLE -->
		<h1 class="post-title">
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'bonfire' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
				<?php the_title(); ?>
			</a>
		</h1>
		<!-- END TITLE -->

		<!-- BEGIN CONTENT -->
		<div class="entry-content">

			<?php the_content( __( 'Continues..', 'bonfire' ) ); ?>

		</div>
		<!-- END CONTENT -->

		<!-- BEGIN PREV/NEXT POST BUTTONS -->
		<div class="next-prev-post-wrapper">
			
			<!-- BEGIN NEXT POST BUTTON -->
			<div class="next-button">
			<a href="<?php echo esc_url(get_permalink(get_adjacent_post(false,'',true))); ?>">
		
				<!-- BEGIN NEXT POST TOOLTIP -->
				<div class="next-tooltip-text">
				<div class="next-tooltip"></div>
					<?php _e( 'NEXT POST', 'bonfire' ); ?>
				</div>
				<!-- END NEXT POST TOOLTIP -->
		
				<!-- BEGIN NEXT POST ICON -->
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48">
					<path d="M14 20l10 10 10-10z">
					<path d="M0 0h48v48h-48z">
				</svg>
				<!-- END NEXT POST ICON -->
			</a>
			</div>
			<!-- END NEXT POST BUTTON -->
			
			<!-- BEGIN PREV POST BUTTON -->
			<div class="prev-button">
			<a href="<?php echo esc_url(get_permalink(get_adjacent_post(false,'',false))); ?>">
			
				<!-- BEGIN PREV POST TOOLTIP -->
				<div class="prev-tooltip-text">
				<div class="prev-tooltip"></div>
					<?php _e( 'PREV POST', 'bonfire' ); ?>
				</div>
				<!-- END PREV POST TOOLTIP -->
		
				<!-- BEGIN PREV POST ICON -->
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48">
					<path d="M14 20l10 10 10-10z">
					<path d="M0 0h48v48h-48z">
				</svg>
				<!-- END PREV POST ICON -->
			</a>
			</div>
			<!-- END PREV POST BUTTON -->
			
		</div>
		<!-- BEGIN PREV/NEXT POST BUTTONS -->
		
		<!-- BEGIN SHARE + PREV/NEXT DIVIDER -->
		<div class="share-prev-next-divider"></div>
		<!-- END SHARE + PREV/NEXT DIVIDER -->
		
		<!-- BEGIN SHARE BUTTON -->
		<div class="share-wrapper">
		
			<!-- BEGIN SHARE BUTTON LINKS -->
			<div class="share-links-wrapper">
				<div class="share-links-tooltip"></div>
					<a target="_blank" href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>">
						<?php _e( 'TWITTER', 'bonfire' ); ?>
					</a>
					<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>">
						<?php _e( 'FACEBOOK', 'bonfire' ); ?>
					</a>
					<a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>">
						<?php _e( 'GOOGLE PLUS', 'bonfire' ); ?>
					</a>
			</div>
			<!-- END FEATURED STORY LINKS -->
		
			<!-- BEGIN SHARE BUTTON TOOLTIP -->
			<?php if ( wp_is_mobile() ) { ?>
			<?php } else { ?>
			<div class="share-tooltip-text">
			<div class="share-tooltip"></div>
				<?php _e( 'SHARE THIS POST', 'bonfire' ); ?>
			</div>
			<?php } ?>
			<!-- END FEATURED STORY TOOLTIP -->
		
			<!-- BEGIN SHARE ICON -->
			<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
				<path d="M0 0h48v48h-48z" fill="none"/>
				<path d="M12 20c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm24 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm-12 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4z"/>
			</svg>
			<!-- END SHARE ICON -->
		</div>
		<!-- END SHARE BUTTON -->

		<!-- BEGIN POST NAVIGATION -->
		<div class="link-pages">
			<?php wp_link_pages(array(''.__('Pages:', 'bonfire').' ', 'next_or_number' => 'number','nextpagelink'     => __( 'Next manga' ),
			                                                          'previouspagelink' => __( 'Previous manga' ),)); ?>
		</div>
		<!-- END POST NAVIGATION -->



		<!-- BEGIN EDIT POST LINK -->
		<?php edit_post_link(__('EDIT', 'bonfire')); ?>
		<!-- END EDIT POST LINK -->
	
	</article>
		
</div>
<!-- /.post -->
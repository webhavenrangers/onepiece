<?php if(!is_single()) : global $more; $more = 0; endif; //enable more link ?>

<div class="content-wrapper">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<!-- BEGIN IF IS STICKY -->
		<?php if (is_sticky()) { ?>
			<div class="sticky-post">
				
				<!-- BEGIN LEFT TITLE -->
				<div class="title-left">
					<?php _e( 'STICKY', 'bonfire' ); ?>
				</div>
				<!-- END LEFT TITLE -->
				
				<!-- BEGIN STICKY ICON -->
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
				<path id="award-23" d="M289.143,225.866l34.686-35.062l-47.678-6.907l-20.989-45.54l-20.989,45.54l-47.678,6.907l34.702,35.062
					l-8.449,49.823l42.414-23.881l42.43,23.881L289.143,225.866z M126.395,303.435c-14.87-23.729-23.587-51.701-23.587-81.768
					c0-85.331,69.169-154.5,154.5-154.5c85.331,0,154.5,69.169,154.5,154.5c0,30.771-9.103,59.362-24.593,83.444
					c-10.428-5.314-21.073-10.051-31.971-14.057c13.949-19.623,22.23-43.537,22.23-69.388c0-66.262-53.914-120.167-120.167-120.167
					c-66.252,0-120.167,53.905-120.167,120.167c0,25.331,7.929,48.826,21.357,68.222C147.568,293.753,136.856,298.263,126.395,303.435z
					 M389.159,345.488l-40.067,57.031c-27.51-15.858-59.194-25.129-93.092-25.28c-33.897,0.151-65.582,9.422-93.092,25.28
					l-40.067-57.031c38.894-23.797,84.325-37.796,133.159-37.955C304.834,307.692,350.266,321.691,389.159,345.488z M462,393.333
					h-40.502l-9.725,51.5c-10.527-17.376-23.821-32.815-39.094-46.01l40.218-57.25C431.59,356.67,448.152,373.3,462,393.333z
					 M99.103,341.573l40.218,57.25c-15.272,13.194-28.566,28.634-39.094,46.01l-9.724-51.5H50
					C63.847,373.3,80.411,356.67,99.103,341.573z"/>
				</svg>
				<!-- END STICKY ICON -->
				
				<!-- BEGIN RIGHT TITLE -->
				<div class="title-right">
					<?php _e( 'POST', 'bonfire' ); ?>
				</div>
				<!-- END RIGHT TITLE -->
				
			</div>
		<?php } ?>
		<!-- END IF IS STICKY -->

		<!-- BEGIN POST DATE + COMMENT COUNT -->
		<div class="post-date-comment">
			<time datetime="<?php echo esc_attr( the_time('o-m-d') ); ?>"><?php the_time('F j, Y') ?></time> /
			<div class="comment-count">
				<a href="<?php the_permalink(); ?>#comments">
					<?php comments_number( 'Leave comment', 'One comment', '% comments' ); ?>
				</a>
			</div>
		</div>
		<!-- END POST DATE + COMMENT COUNT -->

		<!-- BEGIN TITLE -->
		<h1 class="post-title">
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'bonfire' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
				<?php the_title(); ?>
			</a>
		</h1>
		<!-- END TITLE -->

		<!-- BEGIN CONTENT -->
		<div class="entry-content">
			<?php if( get_option('bonfire_sparrow_show_excerpt') ) { ?>
				<?php the_excerpt(); ?>
			<?php } else { ?>
				<?php the_content(''); ?>
			<?php } ?>
		</div>
		<!-- END CONTENT -->

		<!-- BEGIN EDIT POST LINK -->
		<?php edit_post_link(__('EDIT', 'bonfire')); ?>
		<!-- END EDIT POST LINK -->

		<!-- BEGIN 'READ FULL POST' BUTTON -->
		<div class="more-button">
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'bonfire' ), the_title_attribute( 'echo=0' ) ) ); ?>">
				<?php _e( 'FULL POST & COMMENT', 'bonfire' ); ?>
			</a>
		</div>
		<!-- END 'READ FULL POST' BUTTON -->
	
	</article>
		
</div>
<!-- /.post -->
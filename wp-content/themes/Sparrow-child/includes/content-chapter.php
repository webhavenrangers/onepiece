<?php
/**
 * Created by PhpStorm.
 * User: Phantom
 * Date: 25.06.2016
 * Time: 4:10
 */

global $post;
$post_id = $post->ID;

$images_query = get_child_attachments( $post_id );

$ids_list   = '';
$names_list = [ ];
if ( $images_query->have_posts() ) {
	foreach ( $images_query->posts as $key => $image_post ) {
//		this fixes if dublicates are in post attachmants
		if ( array_search( $image_post->post_title, $names_list ) === false ) {
			$ids_list .= $image_post->ID . ',';
			$names_list[] = $image_post->post_title;
		} else {
			continue;
		}
	}
}
if ( ! is_single() ) : global $more;
	$more = 0; endif; //enable more link ?>

<div class="content-wrapper-single">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<!-- BEGIN VIEWS COUNT + COMMENT COUNT -->
		<!--		@todo: add views count (ajaxed) + comments_count(ajaxed)-->
		<!--		<div class="post-date-comment">-->
		<!--			<time datetime="--><?php //echo esc_attr( the_time('o-m-d') ); ?><!--">-->
		<?php //the_time('F j, Y') ?><!--</time> /-->
		<!--			<div class="comment-count">-->
		<!--				--><?php //comments_number( 'Leave comment', 'One comment', '% comments' ); ?>
		<!--			</div>-->
		<!--		</div>-->
		<!-- END VIEWS COUNT + COMMENT COUNT -->

		<!-- BEGIN TITLE -->
		<!--		<h1 class="post-title">-->
		<!--			<a href="--><?php //the_permalink(); ?><!--" title="-->
		<?php //echo esc_attr( sprintf( __( 'Permalink to %s', 'bonfire' ), the_title_attribute( 'echo=0' ) ) ); ?><!--" rel="bookmark">-->
		<!--				--><?php //the_title(); ?>
		<!--			</a>-->
		<!--		</h1>-->
		<!-- END TITLE -->

		<!-- BEGIN CONTENT -->
		<div class="entry-content">

			<?php echo do_shortcode( '[gallery size="full" columns="1" link="file" ids="' . $ids_list . '"]' );
			?>
		</div>
		<!-- END CONTENT -->

		<!-- BEGIN PREV/NEXT POST BUTTONS -->
		<div class="next-prev-post-wrapper">

			<!-- BEGIN NEXT POST BUTTON -->
			<div class="next-button">
				<a href="<?php echo esc_url( get_permalink( get_adjacent_post( false, '', true ) ) ); ?>">

					<!-- BEGIN NEXT POST TOOLTIP -->
					<div class="next-tooltip-text">
						<div class="next-tooltip"></div>
						<?php _e( 'NEXT POST', 'bonfire' ); ?>
					</div>
					<!-- END NEXT POST TOOLTIP -->

					<!-- BEGIN NEXT POST ICON -->
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48">
						<path d="M14 20l10 10 10-10z">
							<path d="M0 0h48v48h-48z">
					</svg>
					<!-- END NEXT POST ICON -->
				</a>
			</div>
			<!-- END NEXT POST BUTTON -->

			<!-- BEGIN PREV POST BUTTON -->
			<div class="prev-button">
				<a href="<?php echo esc_url( get_permalink( get_adjacent_post( false, '', false ) ) ); ?>">

					<!-- BEGIN PREV POST TOOLTIP -->
					<div class="prev-tooltip-text">
						<div class="prev-tooltip"></div>
						<?php _e( 'PREV POST', 'bonfire' ); ?>
					</div>
					<!-- END PREV POST TOOLTIP -->

					<!-- BEGIN PREV POST ICON -->
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48">
						<path d="M14 20l10 10 10-10z">
							<path d="M0 0h48v48h-48z">
					</svg>
					<!-- END PREV POST ICON -->
				</a>
			</div>
			<!-- END PREV POST BUTTON -->

		</div>
		<!-- BEGIN PREV/NEXT POST BUTTONS -->

		<!-- BEGIN SHARE + PREV/NEXT DIVIDER -->
		<div class="share-prev-next-divider"></div>
		<!-- END SHARE + PREV/NEXT DIVIDER -->

		<!-- BEGIN SHARE BUTTON -->
		<div class="share-wrapper">

			<!-- BEGIN SHARE BUTTON LINKS -->
			<div class="share-links-wrapper">
				<div class="share-links-tooltip"></div>
				<a target="_blank"
				   href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>">
					<?php _e( 'TWITTER', 'bonfire' ); ?>
				</a>
				<a target="_blank"
				   href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>">
					<?php _e( 'FACEBOOK', 'bonfire' ); ?>
				</a>
				<a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>">
					<?php _e( 'GOOGLE PLUS', 'bonfire' ); ?>
				</a>
			</div>
			<!-- END FEATURED STORY LINKS -->

			<!-- BEGIN SHARE BUTTON TOOLTIP -->

			<div class="share-tooltip-text">
				<div class="share-tooltip"></div>
				<?php _e( 'SHARE THIS POST', 'bonfire' ); ?>
			</div>

			<!-- END FEATURED STORY TOOLTIP -->

			<!-- BEGIN SHARE ICON -->
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
			     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			     width="64px" height="64px" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve">
				<circle fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" cx="51" cy="13" r="12"/>
				<circle fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" cx="11" cy="42" r="10"/>
				<circle fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" cx="48" cy="55" r="8"/>
				<line fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" x1="40" y1="54" x2="20"
				      y2="46"/>
				<line fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" x1="19" y1="35" x2="41"
				      y2="21"/>
			</svg>
			<!-- END SHARE ICON -->
		</div>
		<!-- END SHARE BUTTON -->

		<!-- BEGIN POST NAVIGATION -->
		<div class="link-pages">
			<?php wp_link_pages( array( '' . __( 'Pages:', 'bonfire' ) . ' ', 'next_or_number' => 'number' ) ); ?>
		</div>
		<!-- END POST NAVIGATION -->

		<!-- BEGIN POST TAGS -->
		<div class="post-tag">
			<?php the_tags( '' . __( 'Tagged with: ', 'bonfire' ) . '', ', ' ); ?>
		</div>
		<!-- END POST TAGS -->


		<!-- BEGIN EDIT POST LINK -->
		<?php edit_post_link( __( 'EDIT', 'bonfire' ) ); ?>
		<!-- END EDIT POST LINK -->

	</article>

</div>
<!-- /.post -->